<?php
    
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    require_once 'H:\School\Spring 2017\[3] Software Engineering\GroupProject\google-api-php-client-2.1.3\vendor\autoload.php';
    putenv('GOOGLE_APPLICATION_CREDENTIALS=H:\School\Spring 2017\[3] Software Engineering\GroupProject\BurningAlpaca-cfeb9ab7edd6.json');
    define('SCOPES', implode(' ', array(
        Google_Service_Calendar::CALENDAR_READONLY)
    ));
    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();
    $client->setScopes(SCOPES);
    // $client->setDeveloperKey('AIzaSyDv-w64cfUxLPSgOW_-EbMLebzYiv17nlc');

    $service = new Google_Service_Calendar($client);

    // Print the next 10 events on the user's calendar.
    $calendarId = 'umslmusic@gmail.com';
    $optParams = array(
    'maxResults' => 10,
    'orderBy' => 'startTime',
    'singleEvents' => TRUE,
    'timeMin' => date('c'),
    );
    $results = $service->events->listEvents($calendarId, $optParams);

    if (count($results->getItems()) == 0) {
        print "No upcoming events found.\n";
    } else {
        print "Upcoming events:\n";
        foreach ($results->getItems() as $event) {
            $start = $event->start->dateTime;
            if (empty($start)) {
            $start = $event->start->date;
            }
            printf("%s (%s)<br />", $event->getSummary(), $start);
        }
    }
?>