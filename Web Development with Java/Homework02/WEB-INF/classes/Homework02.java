// Ahsan Khan
// cs4010 hw2
// 10/05/2017
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Homework02 extends HttpServlet {

	public void init() throws ServletException { ; }

	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		return;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		String[] movies = request.getParameterValues("movie");
		if (movies==null){
			Boolean noneSelected = new Boolean(true);
			request.setAttribute("noneSelected",noneSelected);
			getServletContext()
				.getRequestDispatcher("/index.jsp")
				.forward(request,response);
		} else {
			request.setAttribute("movies",movies);
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			User usr = new User(name,email);
			request.setAttribute("user",usr);
			getServletContext()
				.getRequestDispatcher("/order.jsp")
				.forward(request,response);
		}
	}

	public void destroy(){
		return;
	}

}

