// Ahsan Khan
// cs4010 hw2
// 10/05/2017
import java.io.Serializable;

public class User implements Serializable {
	private String name,email,title;

	public User(){
		name="";
		email="";
	}

	public User(String n,String e){
		name=n;
		email=e;
	}

	public String getName(){
		return name;
	}

	public String getEmail(){
		return email;
	}

	public void setEmail(String e){
		this.email=e;
	}

	public void setName(String n){
		this.name=n;
	}

}

