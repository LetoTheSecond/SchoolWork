<!-- 
	Ahsan Khan
	cs4010 hw2
	10/05/2017
-->
<html>
	<head>
		<link rel="stylesheet" href="./style.css" type="text/css" />
		<style>
			table.tablestyling {
				//border: 1px solid #000000;
				width: 100%;
				text-align: center;
				border-collapse: collapse;
			}
			table.tablestyling td, table.tablestyling th {
				//border: 1px solid #000000;
				padding: 3px 4px;
			}
			table.tablestyling tbody td {
				font-size: 13px;
			}
			table.tablestyling td:nth-child(odd) {
				background: #EBEBEB;
			}
			table.tablestyling tfoot td {
				font-size: 14px;
			}
		</style>
	</head>
	<body>
		<center>
			<h1> DVD Order Form </h1>
			Select the DVDs that you want to order:<br>
			<%
				Boolean test = (Boolean)request.getAttribute("noneSelected");
				if (test!=null){
					out.print("<I>Please select at least on DVD</I><br>");
				}
			%>
			<form action="Homework02" method="post">
				<table class="tablestyling">
					<tr>
						<th>Box Art</th>
						<th>Title</th>
						<th>Price</th>
						<th>Add to Cart</th>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMzg4MDJhMDMtYmJiMS00ZDZmLThmZWUtYTMwZDM1YTc5MWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Hot Fuzz box art"></td>
						<td>Hot Fuzz</td>
						<td>$9.99</td>
						<td><input type="checkbox" name="movie" value="fuzz"></td>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTU2NjA0NDk0NV5BMl5BanBnXkFtZTcwOTA0OTQzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Shawn of the Dead box art"></td>
						<td>Shaun of the Dead</td>
						<td>$9.99</td>
						<td><input type="checkbox" name="movie" value="dead"></td>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMjYyNzgwMl5BMl5BanBnXkFtZTgwMTE3MjYyMTE@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Ghostbusters box art"></td>
						<td>Ghostbusters(1984)</td>
						<td>$9.99</td>
						<td><input type="checkbox" name="movie" value="ghost"></td>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BNzQzMzJhZTEtOWM4NS00MTdhLTg0YjgtMjM4MDRkZjUwZDBlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Blade Runner box art"></td>
						<td>Blade Runner(1982)</td>
						<td>$9.99</td>
						<td><input type="checkbox" name="movie" value="blade"></td>
					</tr>
				</table>
				<br>
				<table>
					<tr>
						<td><label>Name:</label>
						<td><input type="text" name="name" required><br>
					</tr>
					<tr>
						<td><label>Email:</label>
						<td><input type="email" name="email" required><br>
					</tr>
				</table>
				<button type="submit" id="submit">Submit</button>
			</form>
		</center>
	</body>
</body>

