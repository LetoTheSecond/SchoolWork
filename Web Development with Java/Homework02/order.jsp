<!-- 
	Ahsan Khan
	cs4010 hw2
	10/05/2017
-->
<html>
	<head>
		<link rel="stylesheet" href="./style.css" type="text/css" />
		<style>
			table.tablestyling {
				//border: 1px solid #000000;
				//width: 100%;
				text-align: center;
				border-collapse: collapse;
			}
			table.tablestyling td, table.tablestyling th {
				//border: 1px solid #000000;
				padding: 3px 4px;
			}
			table.tablestyling tbody td {
				font-size: 13px;
			}
			table.tablestyling td:nth-child(odd) {
				//background: #EBEBEB;
			}
			table.tablestyling tfoot td {
				font-size: 14px;
			}
		</style>
	</head>
	<body>
		<center>
		<h1>Thanks for your order</h1>
		Here is the information you have entered<br>
		<table class="tablestyling">
			<tr>
				<td>Name:</td>
				<td>${user.name}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>${user.email}</td>
			</tr>
			<%
				String[] movies = (String[])request.getParameterValues("movie");
				for (String i : movies){
					if(i.equals("fuzz")){
						out.print(
						"<tr>\n"+
							"<td><img src=\"https://images-na.ssl-images-amazon.com/images/M/MV5BMzg4MDJhMDMtYmJiMS00ZDZmLThmZWUtYTMwZDM1YTc5MWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg\" alt=\"Image of Hot Fuzz box art\"></td>\n"+
							"<td>Hot Fuzz</td>\n"+
							"<td>$9.99</td>\n"+
						"</tr>\n"
					);
					} else if(i.equals("dead")){
						out.print(
						"<tr>\n"+
							"<td><img src=\"https://images-na.ssl-images-amazon.com/images/M/MV5BMTU2NjA0NDk0NV5BMl5BanBnXkFtZTcwOTA0OTQzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg\" alt=\"Image of Shawn of the Dead box art\"></td>"+
							"<td>Shaun of the Dead</td>\n"+
							"<td>$9.99</td>\n"+
						"</tr>\n"
					);
					} else if(i.equals("ghost")){
					out.print(
						"<tr>\n"+
							"<td><img src=\"https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMjYyNzgwMl5BMl5BanBnXkFtZTgwMTE3MjYyMTE@._V1_UX182_CR0,0,182,268_AL_.jpg\" alt=\"Image of Ghostbusters box art\"></td>"+
							"<td>Ghostbusters(1984)</td>\n"+
							"<td>$9.99</td>\n"+
						"</tr>\n"
					);
					} else if(i.equals("blade")){
					out.println(
						"<tr>\n"+
							"<td><img src=\"https://images-na.ssl-images-amazon.com/images/M/MV5BNzQzMzJhZTEtOWM4NS00MTdhLTg0YjgtMjM4MDRkZjUwZDBlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg\" alt=\"Image of Blade Runner box art\"></td>"+
							"<td>Blade Runner(1982)</td>\n"+
							"<td>$9.99</td>\n"+
						"</tr>\n"
					);
					}
				}
			%>
		</table>
		To order another DVD, click the Back button in your browser or the 
		Return buttton shown below.<br />
		<form action="index.jsp">
			<button type="return" id="return">Return</button>
		</form>
		</center>
	</body>
</html>

