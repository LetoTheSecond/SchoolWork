<!-- 
	Ahsan Khan
	cs4010 hw1
	09/21/2017
-->
<html>
	<head>
		<link rel="stylesheet" href="<%=request.getRealPath("/")%>style.css"  type="text/css" />
	</head>
	<body>
		<center>
			<h1> DVD Order Form </h1>
			Please fill complete this form to order a DVD:<br/>
			<form action="Homework01" method="post">
				<table>
					<tr>
						<td><label>Name:</label>
						<td><input type="text" name="name" required><br>
					</tr>
					<tr>
						<td><label>Email:</label>
						<td><input type="text" name="email" required><br>
					</tr>
					<tr>
						<td><label>DVD Title:</label>
						<td><input type="text" name="title" required><br>
					</tr>
				</table>
				<button type="submit" id="submit">Submit</button>
			</form>
		</center>
	</body>
</body>
