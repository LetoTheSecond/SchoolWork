	// Ahsan Khan
	// cs4010 hw1
	// 09/21/2017
import java.io.Serializable;

public class User implements Serializable {
	private String name,email,title;

	public User(){
		name="";
		email="";
		title="";
	}

	public User(String n,String e,String t){
		name=n;
		email=e;
		title=t;
	}

	public String getName(){
		return name;
	}

	public String getEmail(){
		return email;
	}

	public String getTitle(){
		return title;
	}

	public void setEmail(String e){
		this.email=e;
	}

	public void setName(String n){
		this.name=n;
	}

	public void set(String t){
		this.title=t;
	}

}
