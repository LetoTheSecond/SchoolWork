// Ahsan Khan
// cs4010 hw1
// 09/21/2017
import java.io.*;
import java.util.regex.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Homework01 extends HttpServlet {

	public void init() throws ServletException { ; }

	public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException { ; }

	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
		String url = "/order.jsp";
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String title = req.getParameter("title");
		Pattern pattern = Pattern.compile("^.+@.+(\\.[^\\.]+)+$");
		Matcher matcher = pattern.matcher(email);
		if (!matcher.matches()){
			// res.sendRedirect(req.getHeader("referer"));
			url="/index.jsp";
			// return;
			// getServletContext().getRequestDispatcher("/").forward(req,res);
		}
		User usr = new User(name,email,title);
		req.setAttribute("user",usr);
		getServletContext()
			.getRequestDispatcher(url)
			.forward(req,res);
	}
	public void destroy(){ ; }

}

