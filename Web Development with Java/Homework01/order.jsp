<!-- 
	Ahsan Khan
	cs4010 hw1
	09/21/2017
-->
<html>
	<head>
		<link rel="stylesheet" href="<%=request.getRealPath("/")%>style.css"  type="text/css" />
	</head>
	<body>
		<center>
		<h1>Thanks for your order</h1>
		<table>
			<tr>
				<td>Name:</td>
				<td>${user.name}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>${user.email}</td>
			</tr>
			<tr>
				<td>DVD:</td>
				<td>${user.title}</td>
			</tr>
		</table>
		To order another DVD, click the Back button in your browser or the 
		Return buttton shown below.<br />
		<form action="index.jsp" method="get">
			<button type="return" id="return">Submit</button>
		</form>
		</center>
	</body>
</html>