// Ahsan Khan
// cs4010 hw4
// 11/07/2017
package Beans;

import java.io.Serializable;

public class Movie implements Serializable {
	public String title;
	public String artLink;
	public Boolean inCart;
	public int count;
	public Movie(){
		this.title="";
		this.inCart=false;
		this.count=0;
		this.artLink="";
	}
	public Movie(String t,Boolean i,int c,String l){
		this.title=t;
		this.inCart=i;
		this.count=c;
		this.artLink=l;
	}
	public String getTitle(){
		return this.title;
	}
	public String getArtLink(){
		return this.artLink;
	}
	public Boolean getInCart(){
		return this.inCart;
	}
	public int getCount(){
		return this.count;
	}
	public void setTitle(String t){
		this.title=t;
	}
	public void setArtLink(String l){
		this.artLink=l;
	}
	public void setInCart(Boolean i){
		this.inCart=i;
	}
	public void setCount(int c){
		this.count=c;
	}
}
