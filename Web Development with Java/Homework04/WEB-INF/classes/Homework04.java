// Ahsan Khan
// cs4010 hw4
// 11/07/2017

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.reflect.Array;
import java.util.Vector;
import Beans.*;

public class Homework04 extends HttpServlet {

	public void init() throws ServletException { ; }

	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		return;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {

		String op = request.getParameter("op");
		System.out.printf("[%6s] > %s\n","op",op);

		HttpSession session = request.getSession(true);
		@SuppressWarnings("unchecked")
		Vector<Movie> cart = (Vector<Movie>)session.getAttribute("cart");

		if (cart==null){
			cart = new Vector<Movie>(4);
			cart.add(new Movie());
			cart.get(0).title="Hot Fuzz";
			cart.get(0).count=0;
			cart.get(0).artLink="https://images-na.ssl-images-amazon.com/"+
				"images/M/MV5BMzg4MDJhMDMtYmJiMS00ZDZmLThmZWUtYTMwZDM1YTc5MWE2"+
				"XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg";
			cart.add(new Movie());
			cart.get(1).title="Shaun of the Dead";
			cart.get(1).count=0;
			cart.get(1).artLink="https://images-na.ssl-images-amazon.com/"+
				"images/M/MV5BMTU2NjA0NDk0NV5BMl5BanBnXkFtZTcwOTA0OTQzMw@@._V"+
				"1_UX182_CR0,0,182,268_AL_.jpg";
			cart.add(new Movie());
			cart.get(2).title="Ghostbusters(1984)";
			cart.get(2).count=0;
			cart.get(2).artLink="https://images-na.ssl-images-amazon.com/"+
				"images/M/MV5BMTkxMjYyNzgwMl5BMl5BanBnXkFtZTgwMTE3MjYyMTE@._V"+
				"1_UX182_CR0,0,182,268_AL_.jpg";
			cart.add(new Movie());
			cart.get(3).title="Blade Runner(1982)";
			cart.get(3).count=0;
			cart.get(3).artLink="https://images-na.ssl-images-amazon.com/"+
				"images/M/MV5BNzQzMzJhZTEtOWM4NS00MTdhLTg0YjgtMjM4MDRkZjUwZDB"+
				"lXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg";
			session.setAttribute("cart",cart);
		}

		if (op.equals("add")){

			String addToCart = request.getParameter("addToCart");
			if (addToCart == null || addToCart.equals("")){
				System.out.printf("\n");
				response.sendRedirect("/Homework04/index.jsp");
			}

			System.out.printf("[%6s] > %s\n","movie", addToCart);

			switch (addToCart){
				case "fuzz":
					cart.get(0).inCart=true;
					cart.get(0).count++;
				break;
				case "dead":
					cart.get(1).inCart=true;
					cart.get(1).count++;
				break;
				case "ghost":
					cart.get(2).inCart=true;
					cart.get(2).count++;
				break;
				case "blade":
					cart.get(3).inCart=true;
					cart.get(3).count++;
				break;
				default:
				System.out.printf("\n");
				response.sendRedirect("/Homework04/index.jsp");
			}

			session.setAttribute("cart",cart);
			addToCart = "";
			System.out.printf("\n");
			response.sendRedirect("/Homework04/Cart.jsp");

		} else if (op.equals("cart")){

			String movie 	= request.getParameter("movie");
			String button 	= request.getParameter("button");
			int num 		= Integer.parseInt(request.getParameter("num"));
			int idx=0;
			System.out.printf("[%6s] > %s\n","movie", movie);
			System.out.printf("[%6s] > %s\n","button", button);
			System.out.printf("[%6s] > %d\n","num", num);

			switch (movie){
				case "Hot Fuzz":
					idx=0;
				break;
				case "Shaun of the Dead":
					idx=1;
				break;
				case "Ghostbusters(1984)":
					idx=2;
				break;
				case "Blade Runner(1982)":
					idx=3;
				break;
			}

			if (button.equals("update")){
				if (num <= 0){
					cart.get(idx).count=0;
					cart.get(idx).inCart=false;
				}
				cart.get(idx).count=num;
			} else {
				cart.get(idx).count=0;
				cart.get(idx).inCart=false;
			}
			
			session.setAttribute("cart",cart);
			System.out.printf("\n");
			response.sendRedirect("/Homework04/Cart.jsp");

		} else {
			System.out.printf("[%6s]\n","else");
			System.out.printf("\n");
			response.sendRedirect("/Homework04/index.jsp");
		}
	}

	public void destroy(){
		return;
	}

}

