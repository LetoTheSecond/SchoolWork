<!-- 
	Ahsan Khan
	cs4010 hw4
	11/07/2017
-->
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"  %>

<%@ page import="java.util.Vector" %>
<%@ page import="Beans.Movie" %>
<html>
	<head>
		<link rel="stylesheet" href="./style.css" type="text/css" />
		<style>
			table.tablestyling {
				//border: 1px solid #000000;
				//width: 100%;
				text-align: center;
				border-collapse: collapse;
			}
			table.tablestyling td, table.tablestyling th {
				//border: 1px solid #000000;
				padding: 3px 4px;
			}
			table.tablestyling tbody td {
				font-size: 13px;
			}
			table.tablestyling td:nth-child(odd) {
				background: #EBEBEB;
			}
			table.tablestyling tfoot td {
				font-size: 14px;
			}
			input.num {
				width: 50px;
			}
		</style>
	</head>
	<body>
		<center>
			<h1>Thanks for your order!</h1>
			Here is your final cart:<br>
			<table class="tablestyling">
				<tr>
					<th>Box Art</th>
					<th>Title</th>
					<th>Price</th>
					<th>Amount</th>
					<th>Quantity</th>
				</tr>
				<c:set var="total" value="${0.0}" />
				<c:forEach var="i" items="${cart}">
					<c:if test="${i.inCart == true}">
						<tr>
							<td><img src="${i.artLink}" /></td>
							<td><c:out value="${i.title}" /></td>
							<td>$9.99</td>
							<td><c:out value="$${i.count*9.99}" /></td>
							<c:set var="total" value="${total+(i.count*9.99)}"/>
							<td><c:out value="${i.count}" /></td>
						</tr>
					</c:if>
				</c:forEach>
			</table>
			<table>
				<b>
					<td>Total</td>
					<td><c:out value="$${total}" /></td>
				</b>
			</table>
			To order another DVD, click on the button below.<br>
			<button onclick="window.location.href='index.jsp'">Return</button>
		</center>
	</body>
</html>
