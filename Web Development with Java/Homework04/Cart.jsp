<!-- 
	Ahsan Khan
	cs4010 hw4
	11/07/2017
-->
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"  %>

<%@ page import="java.util.Vector" %>
<%@ page import="Beans.Movie" %>
<html>
	<head>
		<link rel="stylesheet" href="./style.css" type="text/css" />
		<style>
			table.tablestyling {
				//border: 1px solid #000000;
				//width: 100%;
				text-align: center;
				border-collapse: collapse;
			}
			table.tablestyling td, table.tablestyling th {
				//border: 1px solid #000000;
				padding: 3px 4px;
			}
			table.tablestyling tbody td {
				font-size: 13px;
			}
			table.tablestyling td:nth-child(odd) {
				background: #EBEBEB;
			}
			table.tablestyling tfoot td {
				font-size: 14px;
			}
			input.num {
				width: 50px;
			}
		</style>
	</head>
	<body>
		<center>
		<h1>Cart</h1>
		<table class="tablestyling">
			<tr>
				<th>Box Art</th>
				<th>Title</th>
				<th>Price</th>
				<th>Amount</th>
				<th>Quantitiy</th>
				<th>Remove from Cart</th>
			</tr>
			<c:forEach var="i" items="${cart}">
				<c:if test="${i.inCart == true}">
					<form action="Order" method="post">
						<input type="hidden" name="op" value="cart">
						<input type="hidden" name="movie" value="${i.title}">
						<tr>
							<td><img src="${i.artLink}" /></td>
							<td><c:out value="${i.title}" /></td>
							<td>$9.99</td>
							<td><c:out value="$${i.count*9.99}" /></td>
							<td>
								<input class="num" type="number" name="num" value="${i.count}" />
								<input type="submit" name="button" value="update">
							</td>
							<td><input type="submit" name="button" value="remove"></td>
						</tr>
					</form>
				</c:if>
			</c:forEach>
		</table>
		<button onclick="window.location.href='index.jsp'">Continue Shopping</button><br/>
		<button onclick="window.location.href='Checkout.jsp'">Checkout</button>
		</center>
	</body>
</html>

