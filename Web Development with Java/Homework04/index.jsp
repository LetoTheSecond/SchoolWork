<!-- 
	Ahsan Khan
	cs4010 hw4
	11/07/2017
-->

<html>
	<head>
		<link rel="stylesheet" href="./style.css" type="text/css" />
		<style>
			table.tablestyling {
				//border: 1px solid #000000;
				width: 100%;
				text-align: center;
				border-collapse: collapse;
			}
			table.tablestyling td, table.tablestyling th {
				//border: 1px solid #000000;
				padding: 3px 4px;
			}
			table.tablestyling tbody td {
				font-size: 13px;
			}
			table.tablestyling td:nth-child(odd) {
				background: #EBEBEB;
			}
			table.tablestyling tfoot td {
				font-size: 14px;
			}
		</style>
	</head>
	<body>
		<center>
			<h1>DVD list</h1>
			<button onclick="window.location.href='Cart.jsp'">To Cart</button><br/>
			<form action="Order" method="post">
				<input type="hidden" name="op" value="add">
				<table class="tablestyling">
					<tr>
						<th>Box Art</th>
						<th>Title</th>
						<th>Price</th>
						<th></th>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMzg4MDJhMDMtYmJiMS00ZDZmLThmZWUtYTMwZDM1YTc5MWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Hot Fuzz box art"></td>
						<td>Hot Fuzz</td>
						<td>$9.99</td>
						<td><button type="submit" name="addToCart" value="fuzz">Add to Cart</button></td>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTU2NjA0NDk0NV5BMl5BanBnXkFtZTcwOTA0OTQzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Shawn of the Dead box art"></td>
						<td>Shaun of the Dead</td>
						<td>$9.99</td>
						<td><button type="submit" name="addToCart" value="dead">Add to Cart</button></td>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMjYyNzgwMl5BMl5BanBnXkFtZTgwMTE3MjYyMTE@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Ghostbusters box art"></td>
						<td>Ghostbusters(1984)</td>
						<td>$9.99</td>
						<td><button type="submit" name="addToCart" value="ghost">Add to Cart</button></td>
					</tr>
					<tr>
						<td><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BNzQzMzJhZTEtOWM4NS00MTdhLTg0YjgtMjM4MDRkZjUwZDBlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="Image of Blade Runner box art"></td>
						<td>Blade Runner(1982)</td>
						<td>$9.99</td>
						<td><button type="submit" name="addToCart" value="blade">Add to Cart</button></td>
					</tr>
				</table>
			</form>
		</center>
	</body>
</body>
