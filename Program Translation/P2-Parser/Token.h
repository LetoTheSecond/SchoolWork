#ifndef TOKEN_H
#define TOKEN_H

#include <string>

namespace Token{

	enum Id {
		Keyword=0,
		Integer,
		Identifier,
		Operator,
		Delimiter,
		EoF,
		Error
	};

	const std::string IdName[] = {
		"Keyword",
		"Integer",
		"Identifier",
		"Operator",
		"Delimiter",
		"EoF",
		"Error"
	};

	const std::string Keywords[] = {
		"Begin",
		"End",
		"Check",
		"Loop",
		"Void",
		"Var",
		"Return",
		"Input",
		"Output",
		"Program"
	};

	typedef struct Token_T {
		Token::Id id;
		int line,column;
		std::string instance;
	}Token;

}

#endif
