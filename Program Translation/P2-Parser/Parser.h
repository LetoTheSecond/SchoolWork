#ifndef PARSER_H
#define PARSER_H

#include "Scanner.h"

#include <vector>
typedef struct Node{
	std::string Label;
	std::vector<Token::Token> Tokens;
	std::vector<Node*> Children;
	Node(const std::string& l):Label(l){}
	Node():Label(""){}
}Node;

class Parser {
	Scanner scanner;
	Token::Token token;
	Node* root;

	Node* Program();
	Node* RO();
	Node* Assign();
	Node* Loop();
	Node* If();
	Node* Out();
	Node* In();
	Node* MStats();
	Node* Stats();
	Node* Stat();
	Node* R();
	Node* F();
	Node* M();
	Node* Expr();
	Node* MVars();
	Node* Vars();
	Node* Block();
	bool TknEqls(const std::string&);
	void GetToken();
	void LogToken();
	void PrintToken();
	Node* MakeNode(const std::string&);
public:
	Parser():root(nullptr){}
	Parser(std::FILE* f):scanner(f){}
	void SetFile(std::FILE* f){ this->scanner.SetFile(f); }
	bool DoParse();
	// void DestroyTree(Node*);
	// void TraversalInOrder(Node*,const std::string&);
	void TraversalPreOrder(Node*,const std::string&);
	// void TraversalPostOrder(Node*,const std::string&);
	void Print(const std::string&);
};

#endif
