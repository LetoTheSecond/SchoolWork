#ifndef SCANNER_H
#define SCANNER_H

#include <cstdio>
#include "Token.h"

class Scanner {
	int line,
		col;
	std::string str;
	std::FILE* file;
	int filter();
public:
	Scanner(){
		this->file = nullptr;
		this->line = 1;
		this->col = 0;
	}

	Scanner(std::FILE* f){
		this->file = f;
		this->line = 1;
		this->col = 0;
	}
	void SetFile(std::FILE* f){
		this->file = f;
	}
	Token::Token getNextToken();
};

#endif
