#include <cstdint>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <functional>
#include <sstream>

#include "Logger.h"
#include "Parser.h"

#define ErrR (Node*)-1
bool Err(Node* x) { return (std::int64_t)x == -1; }

/*	Grammar
 *	<program>	->	<vars> <block>
 *	<block>		->	Begin <vars> <stats> End
 *	<vars>		->	empty | Var Identifier <mvars>
 *	<mvars>		->	. | , Identifier <mvars>
 *	<expr>		->	<M> + <expr> | <M> - <expr> | <M>
 *	<M>			->	<F> % <M> | <F> * <M> | <F>
 *	<F>			->	( <F> ) | <R>
 *	<R>			->	[ <expr> ] | Identifier | Number
 *	<stats>		->	<stat> <mStat>
 *	<mStat>		->	empty | <stat> <mStat>
 *	<stat>		->	<in> | <out> | <block> | <if> | <loop> | <assign>
 *	<in>		->	Input Identifier ;
 *	<out>		->	Output <expr> ;
 *	<if>		->	Check [ <expr> <RO> <expr> ] <stat>
 *	<loop>		->	Loop [ <expr> <RO> <expr> ] <stat>
 *	<assign>	->	Identifier : <expr> ;
 *	<RO>		->	< | <= | > | >= | == | !=
 */

void PrintNode(Node *node,int level,const std::string& filename) {
	std::stringstream output;
	output << node->Label;
	if (node->Tokens.size()>0){
		output << " Tokens: ";
		for(unsigned int i=0;i<node->Tokens.size()-1;i++){
			output << node->Tokens[i].instance << ", ";
		}
		output << node->Tokens[node->Tokens.size()-1].instance;
	}
	// output << node->Data << ") ";
	// for (unsigned int i=0;i<node->Copies.size();++i){
	// 	output << node->Copies[i] << ", ";
	// }
	// output.rdbuf()->str(std::string(output.rdbuf()->str(),0
	// 			,output.rdbuf()->str().size()-2));
	// for (int i=0;i<level;++i){
	// 	std::printf("    ");
	// }
	for (int i=0;i<level;++i){
		std::printf("  ");
	}
	std::printf("%s\n",output.str().c_str());
	std::fstream file;
	file.open(filename,std::fstream::app);
	// for (int i=0;i<level;++i){
	// 	file << "    ";
	// }
	for (int i=0;i<level;++i){
		file << "\t";
	}
	file << output.str()<<'\n';
	file.close();
}

void Parser::TraversalPreOrder(Node* node,const std::string& file){
	std::string filename = file+".preorder.txt";
	std::function<void(Node*,int)> print = [&](Node* node, int level){
		if (!node){
			return;
		}
		PrintNode(node,level,filename);
		for(unsigned int i=0;i<node->Children.size();i++){
			print(node->Children[i],level+1);
		}
		// print(node->Left,level+1);
		// print(node->Right,level+1);
	};
	print(node,0);
}

void Parser::Print(const std::string& file){
	this->TraversalPreOrder(this->root,file);
}

/*
	void PrintNode(Node *node,int level,const std::string& filename) {
		std::stringstream output;
		output << node->Data << ") ";
		for (unsigned int i=0;i<node->Copies.size();++i){
			output << node->Copies[i] << ", ";
		}
		output.rdbuf()->str(std::string(output.rdbuf()->str(),0
					,output.rdbuf()->str().size()-2));
		for (int i=0;i<level;++i){
			std::printf("    ");
		}
		std::printf("%s\n",output.str().c_str());
		std::fstream file;
		file.open(filename,std::fstream::app);
		for (int i=0;i<level;++i){
			file << "    ";
		}
		file << output.str()<<'\n';
		file.close();
	}

	void Parser::TraversalInOrder(Node* node,const std::string& file){
		std::string filename = file+".inorder";
		std::function<void(Node*,int)> print = [&](Node* node, int level){
			if (!node){
				return;
			}
			print(node->Left,level+1);
			PrintNode(node,level,filename);
			print(node->Right,level+1);
		};
		print(node,0);
	}

	void Parser::TraversalPreOrder(Node* node,const std::string& file){
		std::string filename = file+".preorder";
		std::function<void(Node*,int)> print = [&](Node* node, int level){
			if (!node){
				return;
			}
			PrintNode(node,level,filename);
			print(node->Left,level+1);
			print(node->Right,level+1);
		};
		print(node,0);
	}

	void Parser::TraversalPostOrder(Node* node,const std::string& file){
		std::string filename = file+".postorder";
		std::function<void(Node*,int)> print = [&](Node* node, int level){
			if (!node){
				return;
			}
			print(node->Left,level+1);
			print(node->Right,level+1);
			PrintNode(node,level,filename);
		};
		print(node,0);
	}

	void Parser::DestroyTree(Node* node){
		if (node == nullptr){
			return;
		}
		DestroyTree(node->Left);
		DestroyTree(node->Right);
		delete node;
		node=nullptr;
		return;
	}
*/

void Parser::LogToken() {
	Log("Got Token:[%10s] Instance: %s\n", Token::IdName[token.id].c_str(),
		token.instance.c_str());
}

void Parser::PrintToken() {
	std::printf("Line: [%3d] Column: [%3d] Token: [%10s] Instance: %s\n",
				this->token.line, this->token.column,
				Token::IdName[this->token.id].c_str(),
				this->token.instance.c_str());
}

bool Parser::TknEqls(const std::string& correct) {
	return !std::strcmp(this->token.instance.c_str(), correct.c_str());
}

Node* Parser::MakeNode(const std::string& l) {
	Node* tkn = nullptr;
	tkn = new Node(l);
	if (!tkn) {
		Log("Failed to \"new Node(%s)\"\n", l.c_str());
		return nullptr;
	}
	return tkn;
}

void Parser::GetToken() {
	this->token = this->scanner.getNextToken();
	this->LogToken();
}

Node* Parser::RO() {
	// <RO> -> < | <= | > | >= | == | !=
	Log("Entered RO()\n");
	if (this->token.id == Token::Operator) {
		Node* tkn = this->MakeNode("<RO>");
		tkn->Tokens.push_back(this->token);
		this->GetToken();
		Log("returning tkn from RO()\n");
		return tkn;
	} else {
		this->PrintToken();
		ErrLog("Expected Operator but got %s\n",
			   Token::IdName[this->token.id].c_str());
	}
	return nullptr;
}

Node* Parser::Loop() {
	// <loop> -> Loop [ <expr> <RO> <expr> ] <stat>
	Log("Entered Loop()\n");
	if (this->token.id == Token::Delimiter) {
		if (this->TknEqls("[")) {
			this->GetToken();
			Node* child = this->Expr();
			if (child != nullptr) {
				Node* tkn = this->MakeNode("<Loop>");
				tkn->Children.push_back(child);
				child = this->RO();
				if (child != nullptr) {
					tkn->Children.push_back(child);
					child = this->Expr();
					if (child != nullptr) {
						tkn->Children.push_back(child);
						if (this->token.id == Token::Delimiter) {
							if (this->TknEqls("]")) {
								this->GetToken();
								child = this->Stat();
								if (child != nullptr) {
									tkn->Children.push_back(child);
									Log("returning tkn from Loop()\n");
									return tkn;
								} else {
									// ErrLog("Stat() returned nullptr, which
									// is an error\n");
								}
							} else {
								this->PrintToken();
								ErrLog("Got Delimiter %s but Expected ]\n",
									   this->token.instance.c_str());
							}
						} else {
							this->PrintToken();
							ErrLog("Expected Delimiter but got %s\n",
								   Token::IdName[this->token.id].c_str());
						}
					} else {
						// ErrLog("Expr()[2] returned nullptr, which is an
						// error\n");
					}
				} else {
					// ErrLog("RO() returned nullptr, which is an error\n");
				}
			} else {
				// ErrLog("Expr()[1] returned nullptr, which is an error\n");
			}
		} else {
			this->PrintToken();
			ErrLog("Got Delimiter %s but Expected [\n",
				   this->token.instance.c_str());
		}
	} else {
		this->PrintToken();
		ErrLog("Expected Delimiter but got %s\n",
			   Token::IdName[this->token.id].c_str());
	}
	return nullptr;
}

Node* Parser::If() {
	// <if> -> Check [ <expr> <RO> <expr> ] <stat>
	Log("Entered If()\n");
	if (this->token.id == Token::Delimiter) {
		if (this->TknEqls("[")) {
			this->GetToken();
			Node* child = this->Expr();
			if (child != nullptr) {
				Node* tkn = this->MakeNode("<If>");
				tkn->Children.push_back(child);
				child = this->RO();
				if (child != nullptr) {
					tkn->Children.push_back(child);
					child = this->Expr();
					if (child != nullptr) {
						tkn->Children.push_back(child);
						if (this->token.id == Token::Delimiter) {
							if (this->TknEqls("]")) {
								this->GetToken();
								child = this->Stat();
								if (child != nullptr) {
									tkn->Children.push_back(child);
									Log("returning tkn from If()\n");
									return tkn;
								} else {
									// ErrLog("Stat() returned nullptr, which
									// is an error\n");
								}
							} else {
								this->PrintToken();
								ErrLog("Got Delimiter %s but Expected ]\n",
									   this->token.instance.c_str());
							}
						} else {
							this->PrintToken();
							ErrLog("Expected Delimiter but got %s\n",
								   Token::IdName[this->token.id].c_str());
						}
					} else {
						// ErrLog("Expr()[2] returned nullptr, which is an
						// error\n");
					}
				} else {
					// ErrLog("RO() returned nullptr, which is an error\n");
				}
			} else {
				// ErrLog("Expr()[1] returned nullptr, which is an error\n");
			}
		} else {
			this->PrintToken();
			ErrLog("Got Delimiter %s but Expected [\n",
				   this->token.instance.c_str());
		}
	} else {
		this->PrintToken();
		ErrLog("Expected Delimiter but got %s\n",
			   Token::IdName[this->token.id].c_str());
	}
	return nullptr;
}

Node* Parser::Out() {
	// <out> ->	Output <expr> ;
	Log("Entered Out()\n");
	Node* tkn = MakeNode("<out>");
	Node* child = this->Expr();
	if (child != nullptr) {
		tkn->Children.push_back(child);
		if (this->token.id == Token::Delimiter) {
			if (this->TknEqls(";")) {
				this->GetToken();
				Log("returning token from Out()\n");
				return tkn;
			} else {
				this->PrintToken();
				ErrLog("Got Delimiter %s but Expected ;\n",
					   this->token.instance.c_str());
			}
		} else {
			this->PrintToken();
			ErrLog("Expected Delimiter but got %s\n",
				   Token::IdName[this->token.id].c_str());
		}
	} else {
		// ErrLog("Expr() return nullptr, which is an error\n");
	}
	return nullptr;
}

Node* Parser::In() {
	// <in> -> Input Identifier ;
	Log("Entered In()\n");
	if (this->token.id == Token::Identifier) {
		// <in> -> Identifier
		Node* tkn = this->MakeNode("<in>");
		tkn->Tokens.push_back(this->token);
		this->GetToken();
		if (this->token.id == Token::Delimiter) {
			if (this->TknEqls(";")) {
				// <in> -> ;
				this->GetToken();
				Log("returning tkn from In()\n");
				return tkn;
			} else {
				this->PrintToken();
				ErrLog("Got Delimiter %s but Expected ;\n",
					   this->token.instance.c_str());
				return nullptr;
			}
		} else {
			this->PrintToken();
			ErrLog("Expected Delimiter but got %s\n",
				   Token::IdName[this->token.id].c_str());
			return nullptr;
		}
	}
	this->PrintToken();
	ErrLog("Expected Identifier but got %s\n",
		   Token::IdName[this->token.id].c_str());
	return nullptr;
}

Node* Parser::R() {
	// <R> -> [ <expr> ] | Identifier | Number
	Log("Entered R()\n");
	Node* tkn = this->MakeNode("<R>");
	Node* child;
	if (this->token.id == Token::Delimiter && this->TknEqls("[")) {
		Log("[01] If\n");
		// <R> -> [
		this->GetToken();
		// <R> -> <expr>
		child = this->Expr();
		if (child != nullptr) {
			if (this->token.id == Token::Delimiter) {
				if (this->TknEqls("]")) {
					// <R> -> ]
					this->GetToken();
					tkn->Children.push_back(child);
					Log("returning tkn from R()\n");
					return tkn;
				} else {
					this->PrintToken();
					ErrLog("Got Delimiter %s but Expected ]\n",
						   this->token.instance.c_str());
				}
			} else {
				this->PrintToken();
				ErrLog("Expected Delimiter but got %s\n",
					   Token::IdName[this->token.id].c_str());
			}
		} else {
			// ErrLog("Expr returned nullptr, which is an error\n");
		}
	} else if (this->token.id == Token::Identifier) {
		// <R> -> Identifier
		Log("[02] If\n");
		tkn->Tokens.push_back(this->token);
		this->GetToken();
		Log("returning tkn from R()\n");
		return tkn;
	} else if (this->token.id == Token::Integer) {
		// <R> -> Number
		Log("[03] If\n");
		tkn->Tokens.push_back(this->token);
		this->GetToken();
		Log("returning tkn from R()\n");
		return tkn;
	}
	// <R> -> error
	return nullptr;
}

Node* Parser::F() {
	// <F> -> ( <F> ) | <R>
	Log("Entered F()\n");
	Node* tkn = MakeNode("<F>");
	Node* child;
	if (this->token.id == Token::Delimiter && this->TknEqls("(")) {
		// <F> -> (
		this->GetToken();
		// <F> -> <F>
		child = this->F();
		if (child != nullptr) {
			tkn->Children.push_back(child);
			if (this->token.id == Token::Delimiter) {
				if (this->TknEqls(")")) {
					// <F> -> )
					this->GetToken();
					Log("returning tkn from F()\n");
					return tkn;
				} else {
					this->PrintToken();
					ErrLog("Got Delimiter %s but Expected )\n",
						   this->token.instance.c_str());
				}
			} else {
				this->PrintToken();
				ErrLog("Expected Delimiter but got %s\n",
					   Token::IdName[this->token.id].c_str());
			}
		} else {
			// ErrLog("F() return nullptr, which is an error\n");
		}
	} else {
		child = this->R();
		if (child != nullptr) {
			tkn->Children.push_back(child);
			Log("returning tkn from F()\n");
			return tkn;
		} else {
			// ErrLog("R() returned nullptr, which is an error\n");
		}
	}
	return nullptr;
}

Node* Parser::M() {
	// <M> -> <F> % <M> | <F> * <M> | <F>
	Log("Entered M()\n");
	// <M> -> <F>
	Node* tkn = this->MakeNode("<M>");
	Node* child = this->F();
	if (child == nullptr) {
		// ErrLog("F() returned nullptr, which is an error\n");
		return nullptr;
	} else {
		tkn->Children.push_back(child);
	}
	if (this->token.id == Token::Operator) {
		Log("[01] If\n");
		if (this->TknEqls("%")) {
			// <M> -> %
			Log("[02] If\n");
			tkn->Tokens.push_back(this->token);
			this->GetToken();
			// <M> -> <M>
			child = this->M();
			if (child == nullptr) {
				// ErrLog("M() returned nullptr, which is an error\n");
				return nullptr;
			} else {
				tkn->Children.push_back(child);
			}
		} else if (this->TknEqls("*")) {
			// <M> -> *
			Log("[03] If\n");
			tkn->Tokens.push_back(this->token);
			this->GetToken();
			// <M> -> <M>
			child = this->M();
			if (child == nullptr) {
				// ErrLog("M() returned nullptr, which is an error\n");
				return nullptr;
			} else {
				tkn->Children.push_back(child);
			}
		}
	}
	Log("returning tkn from M()\n");
	return tkn;
}

Node* Parser::Expr() {
	// <expr> -> <M> + <expr> | <M> - <expr> | <M>
	Log("Entered Expr()\n");
	// <expr> -> <M>
	Node* tkn = this->MakeNode("<expr>");
	Node* child = this->M();
	if (child == nullptr) {
		// ErrLog("M() returned nullptr, which is an error\n");
		return nullptr;
	} else {
		tkn->Children.push_back(child);
	}
	if (this->token.id == Token::Operator) {
		Log("[01] If\n");
		if (this->TknEqls("+")) {
			// <expr> -> +
			Log("[02] If\n");
			tkn->Tokens.push_back(this->token);
			this->GetToken();
			// <expr> -> <expr>
			child = this->Expr();
			if (child == nullptr) {
				// ErrLog("Expr() returned nullptr, which is an error\n");
				return nullptr;
			} else {
				tkn->Children.push_back(child);
			}
		} else if (this->TknEqls("-")) {
			// <expr> -> -
			Log("[03] If\n");
			tkn->Tokens.push_back(this->token);
			this->GetToken();
			// <expr> -> <expr>
			child = this->Expr();
			if (child == nullptr) {
				// ErrLog("Expr() returned nullptr, which is an error\n");
				return nullptr;
			} else {
				tkn->Children.push_back(child);
			}
		}
	}
	Log("returning tkn from Expr()\n");
	return tkn;
}

Node* Parser::Assign() {
	// <assign> -> Identifier : <expr> ;
	Log("Entered Assign()\n");
	Node* tkn = this->MakeNode("<assign>");
	tkn->Tokens.push_back(this->token);
	this->GetToken();
	if (this->token.id == Token::Delimiter) {
		// <assign> -> :
		this->GetToken();
		// <assign>	-> <expr>
		Node* child = this->Expr();
		if (child != nullptr) {
			tkn->Children.push_back(child);
			if (this->token.id == Token::Delimiter) {
				if (this->TknEqls(";")) {
					this->GetToken();
					Log("returning tkn from Assign()\n");
					return tkn;
				} else {
					this->PrintToken();
					ErrLog("Got Delimiter %s but Expected ;\n",
						   this->token.instance.c_str());
				}
			} else {
				this->PrintToken();
				ErrLog("Expected Delimiter but got %s\n",
					   Token::IdName[this->token.id].c_str());
			}
		} else {
			this->PrintToken();
			ErrLog("Expr() returned nullptr, which is an error\n");
		}
	}
	// <assign> -> error
	return nullptr;
}

Node* Parser::Stat() {
	// <stat> -> <in> | <out> | <block> | <if> | <loop> | <assign>
	Log("Entered Stat()\n");
	if (this->token.id == Token::Keyword) {
		Log("[01] If\n");
		Node* tkn = this->MakeNode("<stat>");
		if (this->TknEqls("Input")) {
			// <stat> -> <in>
			this->GetToken();
			Node* child = this->In();
			if (child != nullptr) {
				tkn->Children.push_back(child);
				Log("returning tkn from Stat()\n");
				return tkn;
			} else {
				// ErrLog("In() returned nullptr, which is an error\n");
			}
		} else if (this->TknEqls("Output")) {
			// <stat> -> <out>
			this->GetToken();
			Node* child = this->Out();
			if (child != nullptr) {
				tkn->Children.push_back(child);
				Log("returning tkn from Stat()\n");
				return tkn;
			} else {
				// ErrLog("Out() returned nullptr, which is an error\n");
			}
		} else if (this->TknEqls("Begin")) {
			// <stat> -> <block>
			this->GetToken();
			Node* child = this->Block();
			if (child != nullptr) {
				tkn->Children.push_back(child);
				Log("returning tkn from Stat()\n");
				return tkn;
			} else {
				// ErrLog("Block() returned nullptr, which is an error\n");
			}
		} else if (this->TknEqls("Check")) {
			// <stat> -> <if>
			this->GetToken();
			Node* child = this->If();
			if (child != nullptr) {
				tkn->Children.push_back(child);
				Log("returning tkn from Stat()\n");
				return tkn;
			} else {
				// ErrLog("If() returned nullptr, which is an error\n");
			}
		} else if (this->TknEqls("Loop")) {
			// <stat> -> <loop>
			this->GetToken();
			Node* child = this->Loop();
			if (child != nullptr) {
				tkn->Children.push_back(child);
				Log("returning tkn from Stat()\n");
				return tkn;
			} else {
				// ErrLog("Loop() returned nullptr, which is an error\n");
			}
		}
	} else if (this->token.id == Token::Identifier) {
		Log("[02] If\n");
		// <stat> -> <assign>
		Node* child = this->Assign();
		if (child == nullptr) {
			this->PrintToken();
			ErrLog("Assign() returned nullptr, which is an error\n");
		} else {
			Node* tkn = this->MakeNode("<stat>");
			tkn->Children.push_back(child);
			Log("returning tkn from Stat()\n");
			return tkn;
		}
	}
	// <stat> -> error
	return nullptr;
}

Node* Parser::MStats() {
	// <mStat> -> ε | <stat> <mStat>
	Log("Entered MStat()\n");

	if (this->token.id == Token::Keyword ||
		this->token.id == Token::Identifier) {
		// <mStat> -> <stat>
		if (this->TknEqls("End")) {
			return nullptr;
		}
		Node* tkn = this->MakeNode("<mStat>");
		Node* child = this->Stat();
		if (child == nullptr) {
			// Log("Stat() returned nullptr, which is an error\n");
			return ErrR;
		} else {
			tkn->Children.push_back(child);
		}
		// <mStat> -> <mStat>
		child = this->MStats();
		if (Err(child)) {
			// ErrLog("MStats() returned -1, which is an error\n");
			return ErrR;
		} else if (child == nullptr) {
			Log("MStats() returned nullptr, which is valid\n");
			Log("returning tkn from MStats()\n");
			return tkn;
		} else {
			tkn->Children.push_back(child);
			Log("returning tkn from MStats()\n");
			return tkn;
		}
	}

	// <mStat> -> ε
	return nullptr;
}

Node* Parser::Stats() {
	// <stats> -> <stat> <mStat>
	Log("Entered Stats()\n");
	Node* tkn = this->MakeNode("<stats>");
	Node* child;
	// <stats> -> <stat>
	child = this->Stat();
	if (child != nullptr) {
		tkn->Children.push_back(child);
		// <stats> -> <mStat>
		child = this->MStats();
		if (child == nullptr) {
			Log("MStats() return nullptr so its empty, probably\n");
			Log("Returning tkn from Stats()\n");
			return tkn;
		} else if (Err(child)) {
			// ErrLog("MStats() returned -1, which is an error\n");
		} else {
			tkn->Children.push_back(child);
			Log("Returning tkn from Stats()\n");
			return tkn;
		}
	} else {
		// ErrLog("Stat() returned nullptr, which is an error\n");
	}
	// <stats> -> error
	return nullptr;
}

Node* Parser::Block() {
	// <block> -> Begin <vars> <stats> End
	Log("Entered Block()\n");
	Node* tkn = this->MakeNode("<block>");
	Node* child;
	if (this->token.id == Token::Keyword && this->TknEqls("Var")) {
		// <block> -> <vars>
		Log("[01] If\n");
		this->GetToken();
		child = this->Vars();
		if (child == nullptr) {
			Log("Vars() returned nullptr so its ε, probably\n");
		} else if (Err(child)) {
			// ErrLog("Vars() returned -1 so its an error\n");
			return nullptr;
		} else {
			tkn->Children.push_back(child);
		}
		if (this->TknEqls("End")) {
			this->GetToken();
			Log("Returning tkn from Block()\n");
			return tkn;
		}
	}
	// <block> -> <stats>
	child = this->Stats();
	if (child != nullptr) {
		tkn->Children.push_back(child);
		if (this->token.id == Token::Keyword && this->TknEqls("End")) {
			// <block> -> End
			this->GetToken();
			Log("Returning tkn from Block()\n");
			return tkn;
		}
	} else {
		// ErrLog("Stats() returned nullptr, which is an error\n");
	}

	// <block> -> error
	return nullptr;
}

Node* Parser::MVars() {
	// <mvars> -> . | , Identifier <mvars>
	Log("Entered MVars()\n");

	if (this->token.id == Token::Delimiter) {
		if (this->TknEqls(".")) {
			// <mvars> -> .
			Log("[01] If\n");
			this->GetToken();
			return nullptr;
		} else if (this->TknEqls(",")) {
			// <mvars> -> ,
			Log("[02] If\n");
			this->GetToken();
			if (this->token.id == Token::Identifier) {
				// <mvars> -> Identifier
				Node* tkn = this->MakeNode("<mvars>");
				tkn->Tokens.push_back(this->token);
				this->GetToken();
				// <mvars> -> <mvars>
				Node* ret = this->MVars();
				if (Err(ret)) {
					// ErrLog("MVar() returned -1, which is an error\n");
					return ErrR;
				} else if (ret == nullptr) {
					Log("Returning tkn from MVars()[0]\n");
					return tkn;
				} else {
					tkn->Children.push_back(ret);
					Log("Returning tkn from MVars()[1]\n");
					return tkn;
				}
			}
		}
	}

	// <mvars> -> error
	return ErrR;
}

Node* Parser::Vars() {
	// <vars> -> ε | Var Identifier <mvars>
	Log("Entered Vars()\n");
	if (this->token.id == Token::Identifier) {
		// <vars> -> Identifier
		Node* tkn = this->MakeNode("<vars>");
		tkn->Tokens.push_back(this->token);
		this->GetToken();
		// <vars> -> <mvars>
		Node* ret = this->MVars();
		if (Err(ret)) {
			// ErrLog("MVar() returned -1, which is an error\n");
			return ErrR;
		} else if (ret != nullptr) {
			tkn->Children.push_back(ret);
			Log("Returning tkn from Vars()\n");
			return tkn;
		}
	}
	// <vars> -> ε
	return nullptr;
}

Node* Parser::Program() {
	// <program> -> <vars> <block>
	Log("Entered Program()\n");
	Node* tkn = this->MakeNode("<Program>");
	if (this->token.id == Token::Keyword) {
		if (this->TknEqls("Var")) {
			// <program> -> <vars>
			Log("[01] If\n");
			this->GetToken();
			Node* ret = this->Vars();
			if (ret == nullptr) {
				// Var can return ε
				Log("Vars() returned nullptr so its ε, probably\n");
			} else if (Err(ret)) {
				// ErrLog("Vars() returned -1 so its an error\n");
				return nullptr;
			} else {
				tkn->Children.push_back(ret);
			}
			Log("Returning tkn from Program()\n");
		}
		if (this->TknEqls("Begin")) {
			// <program> -> <block>
			Log("[02] If\n");
			this->GetToken();
			Node* ret = nullptr;
			ret = this->Block();
			if (ret == nullptr) {
				// ErrLog("Block() returned nullptr, which is an error\n");
			} else {
				tkn->Children.push_back(ret);
				Log("Returning tkn from Program()\n");
				return tkn;
			}
		} else {
			this->PrintToken();
			ErrLog("Expected Keyword but got %s\n",
				   Token::IdName[this->token.id].c_str());
		}
	}
	// <program> -> error
	return nullptr;
}

bool Parser::DoParse() {
	InitLog();
	Log("Entered DoParse\n");
	this->GetToken();
	this->root = this->Program();
	if (!this->root) {
		// ErrLog("this->root is nullptr\n");
	} else if (this->token.id == Token::EoF) {
		return true;
	}
	return false;
}
