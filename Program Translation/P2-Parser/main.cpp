#include <fstream>
#include "Parser.h"

int main(int argc,char** argv){
	Parser p;
	if (argc > 1){
		// Read from file
		std::FILE* file = std::fopen(argv[1],"r");
		if (!file){
			std::printf("Error: Could not open file: \"%s\". Terminating\n",
					argv[1]);
			return -1;
		}
		p.SetFile(file);
		if (p.DoParse()){
			std::printf("Parsing Succeeded\n");
			p.Print(argv[1]);
		} else {
			std::printf("Parsing Failed\n");
		}
		std::fclose(file);
	} else {
		// Read from stdin
		p.SetFile(stdin);
		if (p.DoParse()){
			std::printf("Parsing Succeeded\n");
			p.Print("out.txt");
		} else {
			std::printf("Parsing Failed\n");
		}
	}
	return 0;
}
