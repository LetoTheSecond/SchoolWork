#pragma once
#ifndef LOGGER_H
#define LOGGER_H
//	Function forwards
bool InitLog();
bool RestartLog();
bool Log( const char* , ... );
bool ErrLog( const char* , ... );
#endif