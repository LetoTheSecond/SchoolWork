#include <fstream>
#include <string>
#include "Compiler.h"
#include "Logger.h"
int main(int argc,char** argv){
	LogInit();
	if (argc > 1){
		std::FILE* file = std::fopen(argv[1],"r");
		if (!file){
			LogError("Error: Could not open file: \"%s\" using \"%s.fs17\" "
					 "instead\n",
					 argv[1], argv[1]);
			std::string test(argv[1]);
			test+=".fs17";
			file = std::fopen(test.c_str(),"r");
			if (!file){
				LogError("Error: Could not open file: \"%s\". Terminating\n",
					argv[1]);
				return -1;
			}
		}
		std::string fileout(argv[1]);
		fileout+=".asm";
		Compile(fileout,file);
		std::fclose(file);
	} else {
		std::string fileout = "out.asm";
		Compile(fileout,stdin);
	}
	return 0;
}
