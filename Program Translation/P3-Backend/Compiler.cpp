#include <cstdarg>
#include <cstdio>
#include <cstring>

#include "Compiler.h"
#include "Logger.h"
#include "Parser.h"

static std::string _asmout;

void CodeOut(const char* code, ...) {
	FILE* file = NULL;
	file = fopen(_asmout.c_str(), "a");
	if (!file) {
		fprintf(stderr, "Could not open asm file : %s : for appending\n",
				_asmout.c_str());
		perror("Perror ");
		return;
	}
	va_list args;
	va_start(args, code);
	vfprintf(file, code, args);
	fprintf(file, "\n");
	va_end(args);
	std::fclose(file);
	va_start(args, code);
	// fprintf(stdout, "");
	vfprintf(stdout, code, args);
	fprintf(stdout, "\n");
	va_end(args);
}

typedef struct Scope_T {
	int varcount;
	std::vector<Token::Token> vars;
	Scope_T() : varcount(0) {}
} Scope;

static std::vector<Scope> _blocks;
static int _block;
static int _temp;
static int _label;

int FindVar(const std::string& var) {
	int stackpos = 0;
	for (int scope = _blocks.size() - 1; 0 <= scope; scope--) {
		for (int v = _blocks[scope].vars.size() - 1; 0 <= v; v--) {
			if (var == _blocks[scope].vars[v].instance) {
				return stackpos;
			} else {
				stackpos++;
			}
		}
	}
	return -1;
}

bool Traverse(Node* node) {
	const auto Label = [&](const std::string& r) { return node->Label == r; };
	Token::Token tkn;
	if (node->Tokens.size()) {
		tkn = node->Tokens[0];
	} else {
		tkn.id = (Token::Id)-1;
	}
	if (Label("<Program>")) {
		_block = 0;
		_temp = -1;
		_label = 0;
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		Log("%*s[Scope %2d]\n", _block * 8, "    ", _block);
		_blocks.emplace_back();
	} else if (Label("<vars>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		for (auto i : _blocks[_block].vars) {
			if (i.instance == tkn.instance) {
				LogError("Line: [%3d] Column: [%3d] %s already declared in "
						 "this scope\n",
						 tkn.line, tkn.column, tkn.instance.c_str());
				return false;
			}
		}
		Log("%*sVar: %s\n", _block * 8, "    ", tkn.instance.c_str());
		_blocks[_block].vars.push_back(tkn);
		_blocks[_block].varcount++;
		Log("%*sPUSH %s\n", _block * 8, "    ",
			_blocks[_block].vars.back().instance.c_str());
		CodeOut("PUSH");
	} else if (Label("<mvars>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		for (auto i : _blocks[_block].vars) {
			if (i.instance == tkn.instance) {
				LogError("Line: [%3d] Column: [%3d] %s already declared in "
						 "this scope\n",
						 tkn.line, tkn.column, tkn.instance.c_str());
				return false;
			}
		}
		Log("%*sVar: %s\n", _block * 8, "    ", tkn.instance.c_str());
		_blocks[_block].vars.push_back(tkn);
		_blocks[_block].varcount++;
		Log("%*sPUSH %s\n", _block * 8, "    ",
			_blocks[_block].vars.back().instance.c_str());
		CodeOut("PUSH");
	} else if (Label("<block>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		_block++;
		_blocks.emplace_back();
		Log("%*s[Scope %2d]\n", _block * 8, "    ", _block);
		int children = node->Children.size();
		if (children) {
			for (int i = 0; i < children; i++) {
				if (!Traverse(node->Children[i])) {
					return false;
				}
			}
		}
		for (int i = _blocks[_block].vars.size() - 1; i >= 0; --i) {
			Log("%*sPOP %s\n", _block * 8, "    ",
				_blocks[_block].vars[i].instance.c_str());
			CodeOut("POP");
			_blocks[_block].vars.pop_back();
		}
		_block--;
		_blocks.pop_back();
		Log("%*s[Scope %2d]\n", _block * 8, "    ", _block);
		return true;
	} else if (Label("<stats>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
	} else if (Label("<mStat>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
	} else if (Label("<stat>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
	} else if (Label("<in>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		int s = FindVar(tkn.instance.c_str());
		if (s < 0) {
			LogError("Line: [%3d] Column: [%3d] %s not found in stack\n",
					 tkn.line, tkn.column, tkn.instance.c_str());
			return false;
		} else {
			_temp++;
			Log("%*s_temp%d\n", _block * 8, "    ", _temp);
			CodeOut("READ _temp%d", _temp);
			CodeOut("LOAD _temp%d", _temp);
			CodeOut("STACKW %d", s);
		}
	} else if (Label("<expr>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		if (node->Children.size() == 2) {
			if (tkn.instance == "+") {
				if (!Traverse(node->Children[0])) {
					return false;
				}
				_temp++;
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("STORE _temp%d", _temp);
				int left = _temp;
				if (!Traverse(node->Children[1])) {
					return false;
				}
				CodeOut("ADD _temp%d", left);
				return true;
			} else if (tkn.instance == "-") {
				if (!Traverse(node->Children[0])) {
					return false;
				}
				_temp++;
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("STORE _temp%d", _temp);
				int left = _temp;
				if (!Traverse(node->Children[1])) {
					return false;
				}
				_temp++;
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("STORE _temp%d", _temp);
				CodeOut("LOAD _temp%d", left);
				CodeOut("SUB _temp%d", _temp);
				return true;
			} else {
				LogError("<expr> encountered incorrect token instance:"
						 "%s\n",
						 tkn.instance.c_str());
				return false;
			}
		}
	} else if (Label("<M>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		if (node->Children.size() == 2) {
			if (tkn.instance == "%") {
				if (!Traverse(node->Children[0])) {
					return false;
				}
				_temp++;
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("STORE _temp%d", _temp);
				int left = _temp;
				if (!Traverse(node->Children[1])) {
					return false;
				}
				_temp++;
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("STORE _temp%d", _temp);
				CodeOut("LOAD _temp%d", left);
				CodeOut("DIV _temp%d", _temp);
				return true;
			} else if (tkn.instance == "*") {
				if (!Traverse(node->Children[0])) {
					return false;
				}
				_temp++;
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("STORE _temp%d", _temp);
				int left = _temp;
				if (!Traverse(node->Children[1])) {
					return false;
				}
				Log("%*s_temp%d\n", _block * 8, "    ", _temp);
				CodeOut("MULT _temp%d", left);
				return true;
			} else {
				LogError("<M> encountered incorrect token instance: %s\n",
						 tkn.instance.c_str());
				return false;
			}
		}
	} else if (Label("<F>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
	} else if (Label("<R>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		if (tkn.id > -1) {
			switch (tkn.id) {
				case Token::Identifier: {
					int s = FindVar(tkn.instance.c_str());
					if (s < 0) {
						LogError("Line: [%3d] Column: [%3d] %s not found in "
								 "stack\n",
								 tkn.line, tkn.column, tkn.instance.c_str());
						return false;
					} else {
						CodeOut("STACKR %d", s);
					}
				} break;
				case Token::Integer:
					CodeOut("LOAD %s", tkn.instance.c_str());
					break;
				default:
					LogError("<R> encountered incorrect token type\n");
					return false;
			}
		}
	} else if (Label("<out>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		int children = node->Children.size();
		if (children) {
			for (int i = 0; i < children; i++) {
				if (!Traverse(node->Children[i])) {
					return false;
				}
			}
		} else {
			LogError("<out> with no children nodes\n");
			return false;
		}
		_temp++;
		Log("%*s_temp%d\n", _block * 8, "    ", _temp);
		CodeOut("STORE _temp%d", _temp);
		CodeOut("WRITE _temp%d", _temp);
		return true;
	} else if (Label("<assign>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		if (tkn.id < 0) {
			LogError("<assign> has no token\n");
			return false;
		} else {
			int s = FindVar(tkn.instance.c_str());
			if (s < 0) {
				LogError("Line: [%3d] Column: [%3d] %s not found in stack\n",
						 tkn.line, tkn.column, tkn.instance.c_str());
				return false;
			} else {
				if (node->Children.size() != 1) {
					LogError("<assign> with no children\n");
					return false;
				}
				if (!Traverse(node->Children[0])) {
					return false;
				}
				CodeOut("STACKW %d", s);
			}
		}
	} else if (Label("<RO>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
	} else if (Label("<If>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		if (!Traverse(node->Children[0])) {
			return false;
		}
		_temp++;
		Log("%*s_temp%d\n", _block * 8, "    ", _temp);
		int expr1 = _temp;
		CodeOut("STORE _temp%d", expr1);
		if (!Traverse(node->Children[2])) {
			return false;
		}
		CodeOut("SUB _temp%d", expr1);
		_label++;
		Log("%*s_label%d\n", _block * 8, "    ", _label);
		int label = _label;
		if (node->Children[1]->Tokens[0].instance == "<") {
			CodeOut("BRZNEG _label%d", label);
		} else if (node->Children[1]->Tokens[0].instance == "<=") {
			CodeOut("BRNEG _label%d", label);
		} else if (node->Children[1]->Tokens[0].instance == ">") {
			CodeOut("BRZPOS _label%d", label);
		} else if (node->Children[1]->Tokens[0].instance == ">=") {
			CodeOut("BRPOS _label%d", label);
		} else if (node->Children[1]->Tokens[0].instance == "==") {
			CodeOut("BRNEG _label%d", label);
			CodeOut("BRPOS _label%d", label);
		} else if (node->Children[1]->Tokens[0].instance == "!=") {
			CodeOut("BRZERO _label%d", label);
		} else {
			return false;
		}
		if (!Traverse(node->Children[3])) {
			return false;
		}
		CodeOut("_label%d: NOOP", label);
		return true;
	} else if (Label("<Loop>")) {
		Log("%*s[%s]\n", _block * 8, "    ", node->Label.c_str());
		_label++;
		Log("%*s_label%d\n", _block * 8, "    ", _label);
		int label1 = _label;
		CodeOut("_label%d: NOOP", label1);
		if (!Traverse(node->Children[0])) {
			return false;
		}
		_temp++;
		Log("%*s_temp%d\n", _block * 8, "    ", _temp);
		int expr1 = _temp;
		CodeOut("STORE _temp%d", expr1);
		if (!Traverse(node->Children[2])) {
			return false;
		}
		CodeOut("SUB _temp%d", expr1);
		_label++;
		Log("%*s_label%d\n", _block * 8, "    ", _label);
		int label2 = _label;
		if (node->Children[1]->Tokens[0].instance == "<") {
			CodeOut("BRZNEG _label%d", label2);
		} else if (node->Children[1]->Tokens[0].instance == "<=") {
			CodeOut("BRNEG _label%d", label2);
		} else if (node->Children[1]->Tokens[0].instance == ">") {
			CodeOut("BRZPOS _label%d", label2);
		} else if (node->Children[1]->Tokens[0].instance == ">=") {
			CodeOut("BRPOS _label%d", label2);
		} else if (node->Children[1]->Tokens[0].instance == "==") {
			CodeOut("BRNEG _label%d", label2);
			CodeOut("BRPOS _label%d", label2);
		} else if (node->Children[1]->Tokens[0].instance == "!=") {
			CodeOut("BRZERO _label%d", label2);
		} else {
			return false;
		}
		if (!Traverse(node->Children[3])) {
			return false;
		}
		CodeOut("BR _label%d", label1);
		CodeOut("_label%d: NOOP", label2);
		return true;
	}
	int children = node->Children.size();
	if (children) {
		for (int i = 0; i < children; i++) {
			if (!Traverse(node->Children[i])) {
				return false;
			}
		}
	}
	return true;
}

void Compile(std::string& filename, std::FILE* f) {
	Parser p(f);
	if (!p.DoParse()) {
		LogError("Parsing failed\n");
		return;
	} else {
		p.Print("tree.txt");
		FILE* file = NULL;
		file = fopen(filename.c_str(), "w");
		std::fclose(file);
		_asmout = filename;
		if (!Traverse(p.root)) {
			LogError("compiling failed\n");
		} else {
			for (int i = _blocks[_block].vars.size() - 1; i >= 0; --i) {
				Log("%*sPOP %s\n", _block * 8, "    ",
					_blocks[_block].vars[i].instance.c_str());
				CodeOut("POP");
			}
			CodeOut("STOP");
			for (int i = _blocks[_block].vars.size() - 1; i >= 0; --i) {
				Log("%*s %s 0\n", _block * 8, "    ",
					_blocks[_block].vars[i].instance.c_str());
				CodeOut("%s 0", _blocks[_block].vars[i].instance.c_str());
				_blocks[_block].vars.pop_back();
			}
			for (; _temp > -1; _temp--) {
				Log("%*s _temp%d 0\n", _block * 8, "    ", _temp);
				CodeOut("_temp%d 0", _temp);
			}
			_block--;
			_blocks.pop_back();
		}
	}
}
