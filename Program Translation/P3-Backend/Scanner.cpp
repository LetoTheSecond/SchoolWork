#include <cstring>
#include <functional>
#include "Scanner.h"
#include "Logger.h"

const char Alphabet[] {
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
	's','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J',
	'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1',
	'2','3','4','5','6','7','8','9','=','<','>','!',':','+','-','*','/','&',
	'%','.','(',')',',','{','}',';','[',']',' ','\n','\t',0
};

enum States {
	Err = -1,	// I/O Error
	S0,			// Start
	S1,			// Process letters	-> id/keyword
	S2,			// Process digits	-> integer
	S3,			// Process = < >	-> operator/S4
	S4,			// Process == <= >=	-> operator
	S5,			// Process !		-> operator/error
	S6,			// Unused
	Integer,	// Return integer token 	07
	Identifier,	// Return identifier token 	08
	Operator,	// Return operator token 	09
	Delimiter,	// Return delimiter token 	10
	EoF,		// Return end of file token 11
	Error		// Return error token 		12
};

const int StateTable[6][87] = {
//																																																																																						  83, 84, 85, 86
// Input:	   a,  b,  c,  d,  e,  f,  g,  h,  i,  j,  k,  l,  m,  n,  o,  p,  q,  r,  s,  t,  u,  v,  w,  x,  y,  z,  A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O,  P,  Q,  R,  S,  T,  U,  V,  W,  X,  Y,  Z,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  =,  <,  >,  !,  :,  +,  -,  *,  /,  &,  %,  .,  (,  ),  ,,  {,  },  ;,  [,  ], WS, \n, \t, EoF Err
/*State 0*/ { S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S2, S2, S2, S2, S2, S2, S2, S2, S2, S2, S3, S3, S3, S5, 10,  9,  9,  9,  9,  9,  9, 10, 10, 10, 10, 10, 10, 10, 10, 10, S0, S0, S0, 11, 12 },
/*State 1*/ { S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8, 11, 12 },
/*State 2*/ {  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, S2, S2, S2, S2, S2, S2, S2, S2, S2, S2,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, 11, 12 },
/*State 3*/ {  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, S4,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, 11, 12 },
/*State 4*/	{  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, 11, 12 },
/*State 5*/ { 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,  9, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 11, 12 }
/*State 6*/// { S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0 },
};

int Scanner::filter(){

	// Check if file pointer is still valid, if not return I/O error state
	if (!this->file){
		this->str ="File pointer no longer valid while fetching next character";
		return -1;
	}

	char c = std::fgetc(this->file);

	// If comment character is encountered, loop untill end of comment 
	if (c =='#'){
		do {
			c = std::fgetc(this->file);
		} while (c != '#');
		c = std::fgetc(this->file);
	}

	// Check of error, if so return I/O error state
	if (std::ferror(this->file)){
		this->str = "I/O error when reading from file";
		return -1;
	}

	// If end if file is reached while there is still a possible token to 
	// process, force it to process then put a flag in the input stream
	// to trigger the EoF token on next getc
	if ((c == EOF && (this->str.size()>0)) || c == -1) {
		c=' ';
		std::ungetc(0,this->file);
	}

	// Check if character is in alphabet
	for (int i=0;i<87;++i){
		if ( c == Alphabet[i]){
			// Keep track of total columns,return StateTable column 
			// of character, and add character to string
			if (c == '\t'){
				this->col+=4;
			} else {
				this->col++;
			}
			if (c != ' ' && c!= '\t'){
				this->str+=c;
			}
			return i;
		}
	}

	// Character was not in alphabet, return error
	this->str = '\'';
	this->str+=c;
	this->str+="\'(";
	this->str+=std::to_string((int)c);
	this->str = this->str+=") Character not in alphabet";
	return 86;
}

Token::Token Scanner::getNextToken(){
	States state = S0;
	int col = 0;
	Token::Token token;

	// Helper function
	const std::function<void()> HandleNewLine = [&](){
		if (!this->str.size()){
			return;
		}
		// If there is a newline character attached to the current token strip
		// it and ajust the line & column numbers accordingly
		if (this->str.back()=='\n'){
			this->str.pop_back();
			token.column=this->col-this->str.size();
			token.line=this->line;
			this->line++;
			this->col=1;
		// If for some reason the token begins with a newline strip it and 
		// ajust the line & column numbers accordingly
		} else if (this->str[0] == '\n'){
			this->str = this->str.substr(1);
			token.column=1;
			this->line++;
			token.line=this->line;
			this->col=1;
		} else {
			token.line=this->line;
			token.column=this->col-this->str.size();
		}
		if (!this->str.size()){
			return;
		}
		if (this->str.back()=='\n' || this->str[0] == '\n'){
			HandleNewLine();
		}
	};

	const auto SetData = [&](){
		token.instance = this->str;
		// Log("SetData: Instance: %s\n",token.instance.c_str());
		this->str.clear();
	};

	// This loop should be pretty self explanitory, a character is processed
	// from the input by calling this->filter. Which returns the column of 
	// the state table the input falls under. The state table is used to 
	// set the correct state then the token is constructed and returned.
	while (true){
		col = this->filter();
		state = static_cast<States>(StateTable[state][col]);
		switch(state){
			case States::Err:{
				HandleNewLine();
				token.id=Token::Id::Error;
				SetData();
				return token;
			}
			case States::Integer:{
				HandleNewLine();
				token.id=Token::Id::Integer;
				SetData();
				return token;
			}
			case States::Identifier:{
				HandleNewLine();
				for (int i=0;i<10;i++){
					if (Token::Keywords[i].compare(this->str) == 0){
						token.id=Token::Id::Keyword;
						SetData();
						return token;
					}
				}
				token.id=Token::Id::Identifier;
				SetData();
				return token;
			}
			case States::Operator:{
				HandleNewLine();
				token.id=Token::Id::Operator;
				SetData();
				return token;
			}
			case States::Delimiter:{
				HandleNewLine();
				token.id=Token::Id::Delimiter;
				SetData();
				return token;
			}
			case States::EoF:{
				HandleNewLine();
				token.id=Token::Id::EoF;
				SetData();
				return token;
			}
			case States::Error:{
				HandleNewLine();
				token.id=Token::Id::Error;
				SetData();
				return token;
			}
			default:
				HandleNewLine();
				break;
		}
	}
}
