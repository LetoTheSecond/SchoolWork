#include <cstring>
#include "Scanner.h"

const char Alphabet[] {
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
	's','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J',
	'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1',
	'2','3','4','5','6','7','8','9','=','<','>','!',':','+','-','*','/','&',
	'%','.','(',')',',','{','}',';','[',']',' ','\n'
};

enum States {
	Err = -1,	// I/O Error
	S0,			// Start
	S1,			// Process letters	-> id/keyword
	S2,			// Process digits	-> integer
	S3,			// Process = < >	-> operator/S4
	S4,			// Process == <= >=	-> operator
	S5,			// Process !		-> operator/error
	S6,			// Unused
	Integer,	// Return integer token 	07
	Identifier,	// Return identifier token 	08
	Operator,	// Return operator token 	09
	Delimiter,	// Return delimiter token 	10
	EoF,		// Return end of file token 11
	Error		// Return error token 		12
};

const int StateTable[6][86] = {
//																																																																																						  83, 84, 85
// Input:	   a,  b,  c,  d,  e,  f,  g,  h,  i,  j,  k,  l,  m,  n,  o,  p,  q,  r,  s,  t,  u,  v,  w,  x,  y,  z,  A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O,  P,  Q,  R,  S,  T,  U,  V,  W,  X,  Y,  Z,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  =,  <,  >,  !,  :,  +,  -,  *,  /,  &,  %,  .,  (,  ),  ,,  {,  },  ;,  [,  ], WS, \n, EoF Err
/*State 0*/ { S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S2, S2, S2, S2, S2, S2, S2, S2, S2, S2, S3, S3, S3, S5, 10,  9,  9,  9,  9,  9,  9, 10, 10, 10, 10, 10, 10, 10, 10, 10, S0, S0, 11, 12 },
/*State 1*/ { S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1, S1,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8, 11, 12 },
/*State 2*/ {  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, S2, S2, S2, S2, S2, S2, S2, S2, S2, S2,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, 11, 12 },
/*State 3*/ {  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, S4,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, 11, 12 },
/*State 4*/	{  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, 11, 12 },
/*State 5*/ { 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,  9, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 11, 12 }
/*State 6*/// { S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0, S0 },
};

int Scanner::filter(){

	// Check if file pointer is still valid, if not return I/O error state
	if (!this->file){
		this->str = "File pointer no longer valid while fetching next token";
		// std::perror("File pointer no longer valid while fetching next token");
		return -1;
	}

	char c = std::fgetc(this->file);
	// Check of error, if so return I/O error state
	if (std::ferror(this->file)){
		this->str = "I/O error when reading from file";
		// std::puts("I/O error when reading from file");
		return -1;
	}

	// If end of file is reached return end of file StateTable column
	if (c == EOF){
		return 84;
	}

	// keep track of line number
	if (c == '\n'){
		this->line++;
		this->col=0;
	}

	// Check if character is in alphabet
	for (int i=0;i<84;++i){
		if ( c == Alphabet[i]){
			// Keep track of total columns,return StateTable column 
			// of character, and add character to string
			this->col++;
			this->str+=c;
			return i;
		}
	}

	// Character was not in alphabet, return error
	this->str =this->str+=" Character not in alphabet";
	return 85;
}

Token::Token Scanner::getNextToken(){
	States state = S0;
	int col = 0;
	Token::Token token;

	const auto SetData = [&](){
		token.line=this->line;
		token.column=this->col-this->str.size();
		token.instance = this->str;
		this->str.clear();
	};

	while (true){
		col = this->filter();
		state = static_cast<States>(StateTable[state][col]);
		switch(state){

			case States::Err:
				token.id=Token::Id::Error;
				SetData();
				return token;

			case States::Integer:
				token.id=Token::Id::Integer;
				SetData();
				return token;

			case States::Identifier:
				for (int i=0;i<10;++i){
					if (Token::Keywords[i].compare(this->str) == 0){
						token.id=Token::Id::Keyword;
						SetData();
						return token;
					}
				}
				token.id=Token::Id::Identifier;
				SetData();
				return token;

			case States::Operator:
				token.id=Token::Id::Operator;
				SetData();
				return token;

			case States::Delimiter:
				token.id=Token::Id::Delimiter;
				SetData();
				return token;

			case States::EoF:
				token.id=Token::Id::EoF;
				SetData();
				return token;

			case States::Error:
				token.id=Token::Id::Error;
				SetData();
				return token;

			default:
				break;
		}
	}
}
