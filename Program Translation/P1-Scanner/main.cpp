#include <fstream>
#include "ScannerTester.h"

int main(int argc,char** argv){
	if (argc > 1){

		// Read from file
		std::FILE* file = std::fopen(argv[1],"r");
		if (!file){
			std::printf("Error: Could not open file: \"%s\". Terminating\n",
					argv[1]);
			return -1;
		}

		TestScanner(file);
		std::fclose(file);
	} else {
		// Read from stdin
		TestScanner(stdin);
	}

	return 0;
}
