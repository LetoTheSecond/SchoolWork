#include "ScannerTester.h"
#include "Scanner.h"

void TestScanner(std::FILE* file){
	Token::Token token;
	Scanner scanner(file);
	while (true){
		token = scanner.getNextToken();
		std::printf("Line: [%3d] Column: [%3d] Token: [%10s] Instance: %s\n",
			token.line,token.column,Token::IdName[token.id].c_str(),
			token.instance.c_str());
		switch(token.id){
			case Token::EoF:
			case Token::Error:
				return;
			default:
				break;
		}
	}
}
