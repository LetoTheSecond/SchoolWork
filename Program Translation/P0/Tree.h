#ifndef TREE_H
#define TREE_H
#include <vector>
#include <new>
#include <memory>
#include <cstdio>

class intTree {
	struct node{
		int data,count;
		node* left,*right;
		node():left(nullptr),right(nullptr){;}
	};
	int data,count,level;
	node* root;
	void indent();
	bool _grow(int,node**);
	void _view(node*,int);
	void _delete(node*);
public:
	intTree()
		{level=count=0;root = nullptr; }
	intTree(int a){
		level=count=0;
		root = new node;root->data = a;root->left = root->right = nullptr;
	}
	~intTree();
	bool grow(int);
	void view(int a = 0);
};

#endif
