#include "Tree.h"
#include <queue>

void intTree::_delete(node* a){
	if (a == nullptr) return;
	_delete(a->left);
	_delete(a->right);
	delete a;
	return;
}

intTree::~intTree(){
	_delete(root);
}

bool intTree::grow(int a){
	if (root == nullptr){
		return _grow(a,&root);
	}
	else if (root->data == a){
		root->count++;
		return true;
	}
	else if (root->data > a){
		return _grow(a,&root->right);
	}
	else if (root->data < a){
		return _grow(a,&root->left);
	}
	else {
		std::cout << "Should not reach, ERROR(0)" << std::endl; 
		return false;
	}
}

bool intTree::_grow(int a, node** b){
	if (*b == nullptr){
		*b = new node;if(*b==nullptr)return false;
		(*b)->data = a;
		(*b)->count = 1;
		(*b)->left = (*b)->right = nullptr;
		return true;
	}
	else if (a == (*b)->data){
		(*b)->count++;
		return true;
	}
	else if (a > (*b)->data)
		return _grow(a,&(*b)->right);
	else if (a < (*b)->data)
		return _grow(a,&(*b)->left);
	else {
		std::cout << "Should not reach, ERROR(1)" << std::endl; 
		return false;
	}
}

void intTree::indent(){
	for (int i=0;i<level;i++)
			std::cout << "  |";
}

void intTree::view(int a){
	if (root == nullptr) return;
	_view(root, a);
	return;
}

void intTree::_view(node* b, int a = 0){
	if (b == nullptr) return;
	switch(a){
		case 0:
			indent (); std::cout << b->data << std::endl;
			level ++;
			_view (b->left, 0);
			_view (b->right, 0);
			level --;
		break;
		case 1:
			_view (b->left, 1);
			std::cout << b->data << " ";
			_view (b->right, 1);
		break;
	}
	return;
}
