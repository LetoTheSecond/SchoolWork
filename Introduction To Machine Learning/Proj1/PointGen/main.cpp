#include <vector>
#include <cstdio>
#include <cstdlib>

typedef struct {
	int x,y;
}Point;

int main(int argc, char** argv){
	std::vector<Point> typeA,typeB;
	const int 	ANum  = 25,
		  		AXlow =  0, AXhigh = 59,
		  		AYlow =  0, AYhigh = 15,
				BNum  = 25,
				BXlow =  0, BXhigh = 59,
				BYlow = 15, BYhigh = 14;
	for (int i=0;i<ANum;i++){
		typeA.push_back({AXlow+rand()%AXhigh,AYlow+rand()%AYhigh});
	}
	for (int i=0;i<BNum;++i){
		typeB.push_back({BXlow+rand()%BXhigh,BYlow+rand()%BYhigh});
	}
	for (int i=0;i<ANum;++i){
		std::printf("{\'%c\',false,%2d,%2d},\n",'0',typeA[i].x,typeA[i].y);
	}
	for (int i=0;i<BNum;++i){
		std::printf("{\'%c\',true,%2d,%2d},\n",'1',typeB[i].x,typeB[i].y);
	}
	return 0;
}

