#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <ctime>

class Point {
	public:
	int type;
	char symbol;
	float x,y;
	Point(int t,char s,float a, float b){
		this->type = t;
		this->symbol = s;
		this->x = a;
		this->y = b;
	}
	Point(float a, float b){
		this->x = a;
		this->y = b;
	}
};

void Draw(std::vector<std::string>& grid){
	int HEIGHT = grid.size(),WIDTH=grid[0].size();
	std::printf("(+y)\n▲\n");
	for (int i=HEIGHT-1;i>=0;--i){
		std::printf("|%s\n",grid[i].c_str());
	}
	std::string temp(WIDTH,'-');
	std::printf("+%s▶ (+x)\n",temp.c_str());
}

void DrawLine(std::vector<std::string> grid,float w0,float w1,float w2,int i=0){
	int x1=0,y1=0,x2=59,y2=29;
	if (w2==0){
		if (w1!=0){
			x1=-(w0/w1);y1=0;
			x2=-(w0/w1);y2=29;
		} else{
			std::printf("p0(~,~) p1(~,~) w0(%f) w1(%f) w2(%f)\n",w0,w1,w2);
			return;
		}
	} else{
		y1 = (int)std::round((-(w1*00)-(w0))/w2),
		y2 = (int)std::round((-(w1*59)-(w0))/w2);
	}
	Point p0(x1,y1);
	Point p1(x2,y2);

	std::printf("[%d] > p0(0,%d) p1(59,%d) w0(%f) w1(%f) w2(%f)\n",
		i,y1,y2,w0,w1,w2);
	
	//Line drawing code/algorithm translated from:
	//http://www.redblobgames.com/grids/line-drawing.html
	auto lerp=[](float s,float e,float t){
		return s+t*(e-s);
	};
	auto lerp_point = [&](Point p0, Point p1, float t) {
		return Point(lerp(p0.x, p1.x, t), lerp(p0.y, p1.y, t));
	};
	auto diagonal_distance=[](Point p0, Point p1) {
		float dx = p1.x - p0.x, dy = p1.y - p0.y;
		return std::max(std::abs(dx), std::abs(dy));
	};
	auto round_point=[](Point p) {
		return Point(std::round(p.x), std::round(p.y));
	};
	float N = diagonal_distance(p0, p1);
	for (int step = 0; step <= N; step++) {
		float t = (N == 0)? (0.0) : ((float)step / N);
		Point z = round_point(lerp_point(p0, p1, t));
		if (z.y>grid.size()-1||z.x>grid[0].size()-1||z.y<0||z.x<0)
			break;
		grid[z.y][z.x]='+';
	}
	Draw(grid);
}

void SetPoints(std::vector<std::string>& grid,std::vector<Point>& points){
	int y = points.size();
	for (int i=0;i<y;++i){
		grid[points[i].y][points[i].x] = points[i].symbol;
	}
}

int main(){
	std::srand(std::time(0));
	std::vector<std::string> grid;
	int WIDTH  = 60,
		HEIGHT = 30;
	std::vector<Point> TrainingData = {
		{-1,'0',0, 0},
		{-1,'0',27,10},
		{-1,'0',23,10},
		{-1,'0',16,12},
		{-1,'0', 9, 1},
		{-1,'0', 2, 7},
		{-1,'0',20, 4},
		{-1,'0',23, 1},
		{-1,'0', 0, 6},
		{-1,'0',22, 1},
		{-1,'0',11, 8},
		{-1,'0',27, 9},
		{-1,'0', 2, 5},
		{-1,'0', 2,13},
		{-1,'0', 7,10},
		{-1,'0',29,12},
		{-1,'0',12, 3},
		{-1,'0',29,12},
		{-1,'0',13, 1},
		{-1,'0', 1, 7},
		{-1,'0', 9, 3},
		{-1,'0',21,14},
		{-1,'0',14, 7},
		{-1,'0', 8,14},
		{-1,'0', 5, 0},
		{+1,'1',59,29},
		{+1,'1',39,23},
		{+1,'1',33,24},
		{+1,'1',52,19},
		{+1,'1',50,18},
		{+1,'1',32,28},
		{+1,'1',48,20},
		{+1,'1',34,20},
		{+1,'1',57,26},
		{+1,'1',41,22},
		{+1,'1',37,18},
		{+1,'1',53,18},
		{+1,'1',39,24},
		{+1,'1',40,15},
		{+1,'1',56,15},
		{+1,'1',31,25},
		{+1,'1',56,15},
		{+1,'1',31,19},
		{+1,'1',51,24},
		{+1,'1',51,16},
		{+1,'1',55,19},
		{+1,'1',52,15},
		{+1,'1',31,23},
		{+1,'1',43,21},
		{+1,'1',49,22}	
	};
	for (int i=0;i<HEIGHT;++i){
		std::string temp(WIDTH,' ');
		grid.push_back(temp);
	}
	SetPoints(grid,TrainingData);
	float w0=std::rand()%50,w1=std::rand()%50,w2=std::rand()%50;
	// float w0=0,w1=-1,w2=2;
	DrawLine(grid,w0,w1,w2);
	int TrainingDataSize = TrainingData.size(),
		iteration = 0;
	bool error = true;
	while (error){
		error = false;
		for (int i=0;i<TrainingDataSize;++i){
			float 	x1=TrainingData[i].x,
					x2=TrainingData[i].y,
					d=TrainingData[i].type,
					s=0.0f;
			static const float 	c=0.25f,
								k=1.0f;
			// printf("[%d]point(%f,%f){%+f}\n",i,x1,x2,d);
			//DrawLine(grid,w0,w1,w2);
			// std::cin.get();
			if (((w1*x1)+(w2*x2)+(w0)) >= 0.0f){
				s=+1.0;
			} else{
				s=-1.0;
			}
			if (s!=d){
				error = true;
				w0+= c * d *  k;
				w1+= c * d * x1;
				w2+= c * d * x2;
				DrawLine(grid,w0,w1,w2,iteration);
			}
		}
		//DrawLine(grid,w0,w1,w2);
		//std::cin.get();
		++iteration;
	}
	std::cin.get();
}
