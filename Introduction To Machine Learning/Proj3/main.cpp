#include <cstdio>
#include <cmath>

const float E = 2.71828182845904523536;

const int DataTable[] = {
	/*1*/ 0,
	/*2*/ 1,
	/*3*/ 0,
	/*4*/ 1,
	/*5*/ 0,
	/*6*/ 1,
	/*7*/ 1,
	/*8*/ 1,
};

int main(){
	double 	w0=0.5,
			w1=0.5,
			LearningRate=0.01;
	for(int loops=0;loops<10000;loops++){
		double 	w0sum = 0.0,
				w1sum = 0.0;
		for(int x=0;x<8;x++){
			double sum = std::pow(E,w0+(w1*(x+1)));
			w0sum+=((DataTable[x]-(sum/1.0+sum)));
			w1sum+=((x+1)*((DataTable[x]-(sum/1.0+sum))));
		}
		w0=w0+LearningRate*(w0sum);
		w1=w1+LearningRate*(w1sum);
		std::printf("[Loop %06d] w0 = %f w1 = %f\n",loops,w0,w1);
	}
	std::printf("w0 = %f w1 = %f\n",w0,w1);
	for(int x=0;x<8;x++){
		std::printf("p(%d=1) = %f\n",x+1,(1.0f/(1.0f+std::pow(E,-1.0*(w0+(w1*(x+1)))))));
	}
}
