#include "Logger.h"
#include <Eigen/Core>
#include <Eigen/LU>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <random>
#include <vector>
#include <fstream>
// ./Proj5  | gnuplot -p -e "plot [-1:13][0:125] '<./Proj5' title \"\",Line1(x)=7.737763*x-15.378799,Line1(x) title \"Linear Regression\" "
// gnuplot -p -e "plot [-1:13][0:125] \"points.txt\" title \"\",Line1(x)=7.737763*x-15.378799,Line1(x) title \"LinearRegression\",Line2(x)=8.271948 + 13.725062*x,Line2(x) title \"Lambda =0.1\",Line3(x)=-13.013725 + 8.336493*x,Line3(x) title \"Lambda =0.01\",Line4(x)=15.142292 + 7.797636*x,Line4(x) title \"Lambda = 0.001\" "

void GenData(Eigen::MatrixXd& x, Eigen::MatrixXd& y, int num) {
	//std::mt19937 gen;
	//gen.seed(12);
	std::random_device rd;
	int seed = rd();
	std::mt19937 gen(seed);
	Log("Seed: %d",seed);
	std::uniform_real_distribution<> dist(-2, 10);

	Log("Data Points:");
	// Log("-----------------------------------------");
	std::vector<float> xs;
	for (int i = 0; i < num; i++) {
		auto x = dist(gen);
		xs.push_back(x);
	}

	std::sort(xs.begin(), xs.end());

	for (int i = 0; i < num; i++) {
		x(i, 0) = 1.0;
		x(i, 1) = xs[i];
		y(i, 0) = (xs[i] * xs[i]) + 10;
		Log("[%02d]  [x] %+9f  [y] %+011f", i + 1, x(i, 1), y(i, 0));
		// Log("-----|-----------------|-----------------");
		// std::printf("%f\t%f\n",x(i,1),y(i,0));
	}
	Log("");
}

int main() {
	LogInit();
	const int numPoints = 12;
	Eigen::MatrixXd x(numPoints, 2), y(numPoints, 1);
	GenData(x, y, numPoints);

	Eigen::MatrixXd xTranspose = x.transpose();
	Eigen::MatrixXd linearReg = (xTranspose * x).inverse() * xTranspose * y;
	double mse = 0.0, w0 = linearReg(0, 0), w1 = linearReg(1, 0);
	for (int i = 0; i < numPoints; i++) {
		int e = (w0 * x(i, 0) + w1 * x(i, 1)) - y(i, 0);
		mse += (e * e);
	}
	mse /= numPoints;
	// Log("MSE: %f", mse);
	Log("Linear Regression:");
	Log("[w0] %f  [w1] %f  [MSE] %011f  y = %f%+fx",w0,w1,mse,w0,w1);
	Log("");

	double lambda[] = {0.1,1.0,10,100};
	double lambdaResults[4];
	for(int l=0;l<4;l++){
		Log("[Lambda] %f",lambda[l]);
		Eigen::MatrixXd xfold1(8,2),yfold1(8,1);
		for(int j=0;j<8;j++){
			xfold1(j, 0) = 1.0;
			xfold1(j, 1) = x(j,1);
			yfold1(j, 0) = (x(j,0) * x(j,1)) + 10;
		}
		Eigen::MatrixXd regf1 = (xfold1.transpose() * xfold1 + lambda[l] * Eigen::MatrixXd::Identity(2, 2)).inverse() * xfold1.transpose() * yfold1;
		double msef1 = 0.0, w0f1 = regf1(0, 0), w1f1 = regf1(1, 0);
		for (int i = 8; i < 12; i++) {
			int e = (w0f1 * x(i, 0) + w1f1 * x(i, 1)) - y(i, 0);
			msef1 += (e * e);
		}
		msef1 /= numPoints;
		Log("[w0] %f  [w1] %f  [MSE] %11f  y = %f%+fx", w0f1, w1f1, msef1, w0f1, w1f1);		
		Eigen::MatrixXd xfold2(8,2),yfold2(8,1);
		for(int j=0;j<4;j++){
			xfold2(j, 0) = 1.0;
			xfold2(j, 1) = x(j,1);
			yfold2(j, 0) = (x(j,0) * x(j,1)) + 10;
		}
		for(int j=8;j<12;j++){
			xfold2((j%4)+3, 0) = 1.0;
			xfold2((j%4)+3, 1) = x(j,1);
			yfold2((j%4)+3, 0) = (x(j,0) * x(j,1)) + 10;
		}
		Eigen::MatrixXd regf2 = (xfold2.transpose() * xfold2 + lambda[l] * Eigen::MatrixXd::Identity(2, 2)).inverse() * xfold2.transpose() * yfold2;
		double msef2 = 0.0, w0f2 = regf2(0, 0), w1f2 = regf2(1, 0);
		for (int i = 4; i < 8; i++) {
			int e = (w0f2 * x(i, 0) + w1f2 * x(i, 1)) - y(i, 0);
			msef2 += (e * e);
		}
		msef2 /= numPoints;
		Log("[w0] %f  [w1] %f  [MSE] %11f  y = %f%+fx", w0f2, w1f2, msef2, w0f2, w1f2);		
		Eigen::MatrixXd xfold3(8,2),yfold3(8,1);
		for(int j=4;j<12;j++){
			xfold3(j-4, 0) = 1.0;
			xfold3(j-4, 1) = x(j,1);
			yfold3(j-4, 0) = (x(j,0) * x(j,1)) + 10;
		}
		Eigen::MatrixXd regf3 = (xfold3.transpose() * xfold3 + lambda[l] * Eigen::MatrixXd::Identity(2, 2)).inverse() * xfold3.transpose() * yfold3;
		double msef3 = 0.0, w0f3 = regf3(0, 0), w1f3 = regf3(1, 0);
		for (int i = 0; i < 4; i++) {
			int e = (w0f3 * x(i, 0) + w1f3 * x(i, 1)) - y(i, 0);
			msef3 += (e * e);
		}
		msef3 /= numPoints;
		Log("[w0] %f  [w1] %f  [MSE] %11f  y = %f%+fx", w0f3, w1f3, msef3, w0f3,w1f3);
		Log("                      Average [MSE] %11f",(1.0/3.0)*(msef1+msef2+msef3));
		lambdaResults[l]=(1.0/3.0)*(msef1+msef2+msef3);
		Log("");
	}

	int lambdaBest = std::distance(std::begin(lambdaResults), std::min_element(std::begin(lambdaResults),std::end(lambdaResults)));
	Log("Best [Lambda] is %f whith average [MSE] of %f",lambda[lambdaBest],lambdaResults[lambdaBest]);
	Eigen::MatrixXd lambdaReg = (xTranspose * x + lambda[lambdaBest] * Eigen::MatrixXd::Identity(2, 2)).inverse() * xTranspose * y;
	double mseLambda = 0.0, w0L = lambdaReg(0, 0), w1L = lambdaReg(1, 0);
	for (int i = 0; i < numPoints; i++) {
		int e = (w0L * x(i, 0) + w1L * x(i, 1)) - y(i, 0);
		mseLambda += (e * e);
	}
	mseLambda /= numPoints;
	Log("[w0] %f  [w1] %f  [MSE] %011f  y = %f%+fx",w0L,w1L,mseLambda,w0L,w1L);

	std::ofstream file;
	file.open("points.txt");
	for(int i=0;i<numPoints;i++){
		file << x(i, 1) << " " << y(i, 0) << '\n';
	}
	file.close();
	file.open("plot.txt");
	file << "set xzeroaxis ls 1 lt -1;\n"
		<< "set yzeroaxis ls 1 lt -1;\n"
		<< "set key left;\n"
		<< "Line1(x)="<<w0<<"+"<<w1<<"*x;\n"
		<< "Line2(x)="<<w0L<<"+"<<w1L<<"*x;\n"
		<< "plot [-3:13][-1:93] \"points.txt\" title \"\", "
		<< "Line1(x) title \"Linear Regression: "<<w0<<"+"<<w1<<"x\" lt 3, "
		<< "Line2(x) title \"Linear Regression λ="<<lambda[lambdaBest] <<": " <<w0L<<"+"<<w1L<<"x\" lt 7;\n";
	file.close();
	return 0;
}
