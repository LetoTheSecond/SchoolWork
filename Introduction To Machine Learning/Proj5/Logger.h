#pragma once
#ifndef LOGGER_H
#define LOGGER_H
//	Function forwards
bool LogInit();
bool LogRestart();
bool Log( const char* , ... );
bool LogError( const char* , ... );
#endif
