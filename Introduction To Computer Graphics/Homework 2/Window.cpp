#include "Window.h"
#include "Logger.h"
using namespace Logger;

bool Window::classRegistered = false;

Window::Window() {
	this->hWin		= nullptr;
	this->wDC		= nullptr;
	this->hRC		= nullptr;
	this->hInstance = nullptr;
}

Window::~Window() {
	if (this->hRC == wglGetCurrentContext())
		wglMakeCurrent(NULL, NULL);
	ReleaseDC(this->hWin, this->wDC);
	wglDeleteContext(this->hRC);
	DestroyWindow(this->hWin);
}

bool Window::MakeWindow(int x, int y, int w, int h,
						LRESULT(CALLBACK* fun)(HWND, UINT, WPARAM, LPARAM)) {
	this->hInstance = GetModuleHandle(0);
	if (!Window::classRegistered) {
		WNDCLASSEX winClass	= {0};
		winClass.cbSize		   = sizeof(WNDCLASSEX);
		winClass.style		   = CS_OWNDC;
		winClass.lpfnWndProc   = (WNDPROC)fun;
		winClass.hInstance	 = this->hInstance;
		winClass.hIcon		   = LoadIcon(NULL, IDI_APPLICATION);
		winClass.hCursor	   = LoadCursor(NULL, IDC_ARROW);
		winClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		winClass.lpszMenuName  = NULL;
		winClass.lpszClassName = "Win32Class";
		winClass.hIconSm	   = LoadIcon(NULL, IDI_APPLICATION);
		if (!RegisterClassEx(&winClass)) {
			// MessageBox(nullptr,"Could not register window
			// class","Error",0);
			LogError("Failed to register window class\n");
			return false;
		}
		Window::classRegistered = true;
	}
	this->hWin =
		CreateWindowExA(WS_EX_CLIENTEDGE, "Win32Class", "",
						WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW,
						x, y, w, h, nullptr, nullptr, this->hInstance, nullptr);
	if (!this->hWin) {
		LogError("Failed to create window\n");
		return false;
	}
	// MessageBox(nullptr, "Could not create window", "Error", 0);
	return true;
}

bool Window::CreateContext() {
	this->wDC				  = GetDC(this->hWin);
	PIXELFORMATDESCRIPTOR pfd = {0};
	pfd.nSize				  = sizeof(pfd);
	pfd.nVersion			  = 1;
	pfd.dwFlags	= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.iLayerType   = PFD_MAIN_PLANE;
	int pf			 = ChoosePixelFormat(this->wDC, &pfd);
	if (!pf) {
		// MessageBox(NULL, "ChoosePixelFormat() failed"
		//				 "Cannot find a suitable pixel format.",
		//		   "Error", MB_OK);
		return false;
	}
	if (!SetPixelFormat(wDC, pf, &pfd)) {
		// MessageBox(NULL, "SetPixelFormat() failed"
		//				 "Cannot set format specified.",
		//		   "Error", MB_OK);
		return false;
	}
	this->hRC = wglCreateContext(wDC);
	wglMakeCurrent(this->wDC, this->hRC);
	return true;
}

bool Window::ProccessEvents() {
	if (PeekMessage(&this->msg, nullptr, 0, 0, PM_REMOVE)) {
		if (this->msg.message == WM_QUIT) {
			return false;
		} else {
			TranslateMessage(&this->msg);
			DispatchMessage(&this->msg);
		}
	}
	return true;
}

void Window::SwapBuffers() { ::SwapBuffers(this->wDC); }

void Window::DisplayWindow() {
	ShowWindow(this->hWin, SW_SHOWNORMAL);
	UpdateWindow(this->hWin);
}
