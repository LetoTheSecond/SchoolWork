#ifndef WINDOW_H
#define WINDOW_H
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <GL/gl.h>

class Window {
	MSG			msg;
	HWND		hWin;
	HDC			wDC;
	HGLRC		hRC;
	HINSTANCE   hInstance;
	static bool classRegistered;

public:
	Window();
	~Window();
	bool MakeWindow(int, int, int, int,
					LRESULT(CALLBACK* fun)(HWND, UINT, WPARAM, LPARAM));
	bool CreateContext();
	void DisplayWindow();
	bool ProccessEvents();
	void SwapBuffers();
	HWND GetWindowHandel() { return this->hWin; }
};
#endif