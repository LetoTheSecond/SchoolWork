#ifndef MESH_H
#define MESH_H
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
// Must include windows.h before gl.h
#include <GL/gl.h>
#include <cmath>
#include <fstream>
#include <iostream>

// 3D Classes for Graphics
//@@@@@@@@@@@@@@@@@@ Point3 class @@@@@@@@@@@@@@@@
class Point3 {
public:
	float x, y, z;

	void Set(float dx, float dy, float dz) {
		x = dx;
		y = dy;
		z = dz;
	}

	void Set(Point3& p) {
		x = p.x;
		y = p.y;
		z = p.z;
	}

	Point3(float xx, float yy, float zz) {
		x = xx;
		y = yy;
		z = zz;
	}

	Point3() { x = y = z = 0; }

	void build4tuple(float v[]) {
		// load 4-tuple with this color: v[3] = 1 for homogeneous
		v[0] = x;
		v[1] = y;
		v[2] = z;
		v[3] = 1.0f;
	}
};

//@@@@@@@@@@@@@@@@@@ Vector3 class @@@@@@@@@@@@@@@@
class Vector3 {
public:
	float x, y, z;

	void set(float dx, float dy, float dz) {
		x = dx;
		y = dy;
		z = dz;
	}

	void set(Vector3& v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}

	void flip() {
		x = -x;
		y = -y;
		z = -z;
	}

	// reverse this vector
	// set to difference a - b
	void setDiff(Point3& a, Point3& b) {
		x = a.x - b.x;
		y = a.y - b.y;
		z = a.z - b.z;
	}

	// adjust this vector to unit length
	void normalize() {
		double sizeSq = x * x + y * y + z * z;
		if (sizeSq < 0.0000001) {
			std::cerr << "\nnormalize() sees vector (0,0,0)!";
			return;
		}
		float scaleFactor = 1.0f / (float)sqrt(sizeSq);
		x *= scaleFactor;
		y *= scaleFactor;
		z *= scaleFactor;
	}

	Vector3(float xx, float yy, float zz) {
		x = xx;
		y = yy;
		z = zz;
	}

	Vector3(Vector3& v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}

	Vector3() { x = y = z = 0; }

	// return this cross b
	Vector3 cross(Vector3 b) {
		Vector3 c(y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
		return c;
	}

	// return this dotted with b
	float dot(Vector3 b) { return x * b.x + y * b.y + z * b.z; }
};

//################# VertexID ###################
class VertexID {
public:
	// index of this vert in the vertex list
	int vertIndex;
	// index of this vertex's normal
	int normIndex;
};

//#################### Face ##################
class Face {
public:
	// number of vertices in this face
	int nVerts;
	// the list of vertex and normal indices
	VertexID* vert;

	Face() {
		nVerts = 0;
		vert   = NULL;
	}

	~Face() {
		delete[] vert;
		nVerts = 0;
	}
};

class Mesh {
private:
	// number of vertices in the mesh
	int numVerts;
	// array of 3D vertices
	Point3* pt;
	// number of normal vectors for the mesh
	int numNorms;
	// array of normals
	Vector3* norm;
	// number of faces in the mesh
	int numFaces;
	// array of face data
	Face* face;

public:
	bool readFile(const char* fileName);
	void Draw(bool wire = false);
};

#endif // ifndef MESH_H
