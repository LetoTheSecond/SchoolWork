@echo off
if /i [%1] == [clean] (
	del /Q  *.pdb *.ilk *.idb *.obj *.exe
) else (
	cl /D OVERWRITE_LOGFILE /Zi /Gm /nologo /Debug /EHsc *.cpp /Fe:main.exe
)
