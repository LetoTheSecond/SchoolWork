#define NOMINMAX
#include "Window.h"
#include "Mesh.h"
#include <GL/glu.h>
//#include <GL\freeglut.h>
#include <vector>
#include "Logger.h"
#include <cmath>
#include <functional>
#ifndef PI
#define PI 3.14159265359
#endif
#include <algorithm>
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#include "OtherObjects.h"
using namespace Logger;

// Struct to hold Object information
struct Object {

	Mesh   mesh;
	Point3 pos;
	float  xRot, yRot, zRot, scale;

	Object() {
		this->xRot  = 0.0f;
		this->yRot  = 0.0f;
		this->zRot  = 0.0f;
		this->scale = 1.0f;
	}

	Object(Mesh& m) {
		this->mesh  = m;
		this->xRot  = 0.0f;
		this->yRot  = 0.0f;
		this->zRot  = 0.0f;
		this->scale = 1.0f;
	}

	void Update() {}

	virtual void Draw(bool wire = false) {
		glPushMatrix();
		glRotatef(this->xRot, 1.0f, 0.0f, 0.0f);
		glRotatef(this->yRot, 0.0f, 1.0f, 0.0f);
		glRotatef(this->zRot, 0.0f, 0.0f, 1.0f);
		glTranslatef(this->pos.x, this->pos.y, this->pos.z);
		glScalef(this->scale, this->scale, this->scale);
		this->mesh.Draw(wire);
		glPopMatrix();
	}

};

struct ObjOther : public Object {

	std::function<void(bool)> usrDraw;
	void Draw(bool wire = false){
		glPushMatrix();
		glRotatef(this->xRot, 1.0f, 0.0f, 0.0f);
		glRotatef(this->yRot, 0.0f, 1.0f, 0.0f);
		glRotatef(this->zRot, 0.0f, 0.0f, 1.0f);
		glTranslatef(this->pos.x, this->pos.y, this->pos.z);
		glScalef(this->scale, this->scale, this->scale);
		// DRAW HERE
		this->usrDraw(wire);
		glPopMatrix();
	}

};

// Struct to hold camera information
struct Camera {

	Point3  pos;
	Vector3 front, up, right;
	float   yaw, pitch, roll, sensitivity, speed;

	Camera() {
		this->yaw		  = -90.0f;
		this->pitch		  = 0.0f;
		this->roll		  = 0.0f;
		this->sensitivity = 0.25f;
		this->speed		  = 3.0f;
	}

	void SetPos(float x, float y, float z) {
		this->pos.x = x;
		this->pos.y = y;
		this->pos.z = z;
	}

	void SetDir(float x, float y, float z) {
		this->front.x = x;
		this->front.y = y;
		this->front.z = z;
	}

	void SetUp(float x, float y, float z) {
		this->up.x = x;
		this->up.y = y;
		this->up.z = z;
	}

	void Update() {
		Vector3 _front;
		Vector3 _WorldUp(0.0f, 1.0f, 0.0f);
		_front.x =
			float(cos(this->yaw * PI / 180) * cos(this->pitch * PI / 180));
		_front.y = sin(this->pitch);
		_front.z =
			float(sin(this->yaw * PI / 180) * cos(this->pitch * PI / 180));
		_front.normalize();
		this->front = _front;
		this->right = this->front.cross(_WorldUp);
		this->right.normalize();
		this->up = this->right.cross(this->front);
		this->up.normalize();
	}

};

// Windows callback function to handel events
static bool GKeys[256] = {false};
static int  GWinH, GWinW;
LRESULT CALLBACK EventHandler(HWND w, UINT m, WPARAM wp, LPARAM lp) {

	switch (m) {
		// Window close event
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;
		// Window resize event
		case WM_SIZE: {
			GWinW = LOWORD(lp);
			GWinH = HIWORD(lp);
			glViewport(0, 0, GWinW, GWinH);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(45.0f, (GLfloat)GWinW / (GLfloat)GWinH, 0.1f,
						   100.0f);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			Log("Resized: %dx%d\n", GWinW, GWinH);
			return 0;
		}
		// Key was pressed
		case WM_KEYDOWN:
			if (wp == VK_ESCAPE) {
				PostQuitMessage(0);
				return 0;
			}
			if (!GKeys[VK_SHIFT]) {
				// Lower Case
				switch (wp) {
					case 'M':
					case 'R':
					case 'T':
					case 'S':
					case 'D':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					// case '6':
						// Need to do this to capture single key press
						// 30th bit tells if key was held down last event
						// cycle or if this event is a keypress for the first
						// time.
						GKeys[wp] = !(lp & (1 << 30));
						return 0;
					default:
						// Records key being held down
						GKeys[wp] = true;
						return 0;
				}
			} else {
				// Upper Case
				switch (wp) {
					case 'W':
						// Need to do this to capture single key press
						GKeys[wp] = !(lp & (1 << 30));
						return 0;
					default:
						// Records key being held down
						GKeys[wp] = true;
						return 0;
				}
			}
		// Key was released
		case WM_KEYUP:
			GKeys[wp] = false;
			return 0;
	}
	// For events not handeled manually, let windows handel them
	return DefWindowProc(w, m, wp, lp);

}

// Switch between ligting model
// 0 : Flat-Shading
// 1 : SmoothShading
void SetLigting(int l) {

	switch (l) {
		case 0:
			glShadeModel(GL_FLAT);
			return;
		case 1:
			glShadeModel(GL_SMOOTH);
			return;
	}

}

// Draw X,Y,Z axes
void DrawAxes(){
	
	const static auto axis=[](double length){
		glPushMatrix();
		glBegin(GL_LINES);
		glVertex3d(0, 0, 0); glVertex3d(0, 0, length); // along the z-axis
		glEnd();
		glTranslated(0, 0,length -0.2); 
		//glutWireCone(0.04, 0.2, 12, 9);
		//glutSolidCone(0.04, 0.2, 12, 9);
		glPopMatrix();
	};
	{
		GLfloat mat_diffuse[] = { 0.0, 0.0, 1.0, 1.0 }; 
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_AMBIENT, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_SHININESS, mat_diffuse);
		glColor3d(0,0,1); // blue lines for wireframe
		axis(2);		 // z-axis
	}
	glPushMatrix(); 
	{
		GLfloat mat_diffuse[] = { 1.0, 0.0, 0.0, 1.0 }; 	
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_AMBIENT, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_SHININESS, mat_diffuse);
		glRotated(90, 0, 1.0, 0);
		glColor3d(1,0,0); // red lines for wireframe
		axis(2);		// x-axis
	}
	glPopMatrix();	
	glPushMatrix();	
	{
		GLfloat mat_diffuse[] = { 0.0, 1.0, 0.0, 1.0 }; 
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_AMBIENT, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_SHININESS, mat_diffuse);	
		glRotated(-90.0, 1, 0, 0);
		glColor3d(0,1,0); // green lines for wireframe
		axis(2);		// y-axis
	}
	glPopMatrix();
	GLfloat mat_ambient[] =		{ 00.7f, 0.7f, 0.7f, 1.0f }; // gray
	GLfloat mat_diffuse[] =		{ 00.6f, 0.0f, 0.0f, 1.0f }; 
	GLfloat mat_specular[] =	{ 01.0f, 1.0f, 1.0f, 1.0f }; 
	GLfloat mat_shininess[] =	{ 50.0f }; 

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	/*
	glColor3d(0,0,1);
	axis(500);					// z-axis
	glPushMatrix(); 
	glRotated(90,0,1.0,0);
	glColor3d(1,0,0);
	axis(500);					// x-axis
	glRotated(-90.0,1,0,0);
	glColor3d(0,1,0);
	axis(500);					// y-axis
	glPopMatrix();	
	*/

}

struct Color{

	float r,g,b,a;

};

int main(int argc,char** argv) {

	LogInit();
	//glutInit(&argc,argv);
	Window				window;
	std::vector<Object*> objects;
	Camera				camera;
	int modelIdx=0,shadeIdx=1,transIdx=0,colIdx=0;
	Color	blue	{(1.0f/255)*211,(1.0f/255)*214,(1.0f/255)*237,1.0},
			green	{(1.0f/255)*112,(1.0f/255)*149,(1.0f/255)*024,1.0},
			red		{(1.0f/255)*204,(1.0f/255)*057,(1.0f/255)*034,1.0},
			yello	{(1.0f/255)*204,(1.0f/255)*204,(1.0f/255)*034,1.0},
			grey	{(1.0f/255)*153,(1.0f/255)*153,(1.0f/255)*102,1.0};
			
	Color col[]={
		blue,
		green,
		red,
		yello,
		grey
	};
	// transIdx: 0 = translate	(t)
	// 			 1 = rotate		(r)
	// 			 2 = scale		(s)

	// Models:
	//=================================
	// Cube
	// Teapot
	// Jack
	// WINEGLASS.3VN
	// Something self made
	{
		// CUBE
		Mesh mesh0;
		if (!mesh0.readFile("CUBE.3VN")){
			LogError("Could not load 3VN\n");
			return -1;
		}
		objects.push_back(&Object(mesh0));
		objects[0]->pos.x=0.0f;
		objects[0]->pos.y=0.0f;
		objects[0]->pos.z=0.0f;
		objects[0]->scale=3.0f;
		// TEAPOT
		objects.push_back(&ObjOther());
		((ObjOther*)objects[1])->usrDraw = [&](bool wire){
			if (!wire){
				//glBegin(GL_POLYGON);
				COPY::glutSolidTeapot(objects[1]->scale);
			} else {
				COPY::glutWireTeapot(objects[1]->scale);
			}
		};
		objects[1]->pos.x=0.0f;
		objects[1]->pos.y=0.0f;
		objects[1]->pos.z=0.0f;
		// JACK
		objects.push_back(&ObjOther());
		((ObjOther*)objects[2])->usrDraw = [&](bool wire){
			static const auto jackPart=[](bool wire){
				if (!wire){
					glPushMatrix();
					glScaled(0.2,0.2,1.0);
					COPY::glutSolidSphere(1,15,15);
					glPopMatrix();
					glPushMatrix();
					glTranslated(0,0,1.2);
					COPY::glutSolidSphere(0.2,15,15);
					glTranslated(0,0,-2.4);
					COPY::glutSolidSphere(0.2,15,15);
					glPopMatrix();
				} else{
					glPushMatrix();
					glScaled(0.2,0.2,1.0);
					COPY::glutWireSphere(1,15,15);
					glPopMatrix();
					glPushMatrix();
					glTranslated(0,0,1.2);
					COPY::glutWireSphere(0.2,15,15);
					glTranslated(0,0,-2.4);
					COPY::glutWireSphere(0.2,15,15);
					glPopMatrix();
				}
			};
				glPushMatrix();
				jackPart(wire);
				glRotated(90.0, 0, 1, 0);
				jackPart(wire);
				glRotated(90.0, 1,0,0);
				jackPart(wire);
				glPopMatrix();
		};
		objects[2]->pos.x=0.0f;
		objects[2]->pos.y=0.0f;
		objects[2]->pos.z=0.0f;

		// WINEGLASS
		Mesh mesh3;
		if (!mesh3.readFile("WINEGLASS.3VN")){
			LogError("Could not load 3VN\n");
			return -1;
		}
		objects.push_back(&Object(mesh3));
		objects[3]->pos.x=0.0f;
		objects[3]->pos.y=0.0f;
		objects[3]->pos.z=0.0f;
		// SELF MADE
		objects.push_back(&ObjOther());
		((ObjOther*)objects[4])->usrDraw = [&](bool wire){
			if (!wire){
				
			} else{

			}

		};
		objects[4]->pos.x=0.0f;
		objects[4]->pos.y=0.0f;
		objects[4]->pos.z=0.0f;
	}
	// Camera
	//=================================
	camera.SetPos(0.0f, 0.0f, 10.0f);
	camera.SetDir(0.0f, 0.0f, 0.0f);
	camera.SetUp(0.0f, 1.0f, 0.0f);
	camera.speed = 1.0f;
	// Window , Create win32 window
	//=================================
	GWinW = 800;
	GWinH = 600;
	window.MakeWindow(GetSystemMetrics(SM_CXSCREEN) / 2 - GWinW / 2,
					  GetSystemMetrics(SM_CYSCREEN) / 2 - GWinH / 2, GWinW,
					  GWinH, EventHandler);
	// Create OpenGL context
	window.CreateContext();
	// Display the window
	window.DisplayWindow();

	// Opengl Settings
	//=================================
	
	// set properties of the surface material
	GLfloat mat_ambient[] =		{ 00.7f, 0.7f, 0.7f, 1.0f }; // gray
	GLfloat mat_diffuse[] =		{ 00.6f, 0.0f, 0.0f, 1.0f }; 
	GLfloat mat_specular[] =	{ 01.0f, 1.0f, 1.0f, 1.0f }; 
	GLfloat mat_shininess[] =	{ 50.0f }; 

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	
	// Set the lights
	GLfloat light_intensity[] = {0.8f, 00.8f, 00.8f, 1.0f};
	GLfloat light_position[] =  {0.0f, 10.0f, 10.0f, 1.0f}; 
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_intensity);
	//float local_view[] = {0.0};
	//glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable(GL_DEPTH_TEST);

	glFrontFace(GL_CW);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	
	glLineWidth(1.0f);
	// Camera Position, using spherical coordinates
	float theta = 44, phi = 70,
	// Sensitivity for operations: transformt - scale - rotation
		  tSens = 0.15f, sSens = 0.15f, rSens = 1.00f,
	// Field of View
		  fov = 45.0f;
	//glClearColor(blue.r, blue.g, blue.b, blue.a);
	
	// Main Loop
	//=================================
	while (window.ProccessEvents()) {
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		//glClearColor(col[colIdx].r,col[colIdx].g,col[colIdx].b,col[colIdx].a);
		// Input Handeling:
		//=============================
		if (!GKeys[VK_SHIFT]){
		// Lower Case		
			// 1: view model 1
			if (GKeys['1']){
				Log("1\n");
				modelIdx=0;
				GKeys['1']=false;
			}
			// 2: view model 2
			if (GKeys['2']){
				Log("2\n");
				modelIdx=1;
				GKeys['2']=false;
			}
			// 3: view model 3
			if (GKeys['3']){
				Log("3\n");
				modelIdx=2;
				GKeys['3']=false;
			}
			// 4: view model 4
			if (GKeys['4']){
				Log("4\n");
				modelIdx=3;
				GKeys['4']=false;
			}
			// 5: view model 5
			if (GKeys['5']){
				Log("5\n");
				modelIdx=4;
				GKeys['5']=false;
			}
			// 5: view model 6
			if (GKeys['6']){
				Log("6\n");
				modelIdx=5;
				GKeys['6']=false;
			}
			// m: Change the material color, 5 different colors
			if (GKeys['M']){
				++colIdx;
				colIdx%=5;
				switch (colIdx){
					case 0:
						Log("m : 0 -> blue: (%f,%f,%f)\n",col[colIdx].r,col[colIdx].g,col[colIdx].b);
						break;
					case 1:
						Log("m : 1 -> green: (%f,%f,%f)\n",col[colIdx].r,col[colIdx].g,col[colIdx].b);
						break;
					case 2:
						Log("m : 2 -> red: (%f,%f,%f)\n",col[colIdx].r,col[colIdx].g,col[colIdx].b);
						break;
					case 3:
						Log("m : 3 -> yello: (%f,%f,%f)\n",col[colIdx].r,col[colIdx].g,col[colIdx].b);
						break;
					case 4:
						Log("m : 4 -> grey: (%f,%f,%f)\n",col[colIdx].r,col[colIdx].g,col[colIdx].b);
						break;
				}
				GKeys['M']=false;
			}
			// r: go into rotation mode
			if (GKeys['R']){
				Log("r\n");
				Log("Rotation Mode\n");
				transIdx=1;
				GKeys['R']=false;
			}
			// t: go into translation mode
			if (GKeys['T']){
				Log("t\n");
				Log("Translation Mode\n");
				transIdx=0;
				GKeys['T']=false;
			}
			// s: go into scaleing mode range: (0,1]
			if (GKeys['S']){
				Log("s\n");
				Log("Scaling Mode\n");
				transIdx=2;
				GKeys['S']=false;
			}
			// x: + rotation/translation/grow along x axis
			if (GKeys['X']) {
				Log("x\n");
				switch (transIdx) {
					case 0: // Translate
						objects[modelIdx]->pos.x += tSens;
						break;
					case 1: // Rotate
						objects[modelIdx]->xRot += rSens;
						break;
					case 2: // Scale
						objects[modelIdx]->scale += sSens;
						break;
				}
			}
			// y: + rotation/translation along y axis
			if (GKeys['Y']) {
				Log("y\n");
				switch (transIdx) {
					case 0: // Translate
						objects[modelIdx]->pos.y += tSens;
						break;
					case 1: // Rotate
						objects[modelIdx]->yRot += rSens;
						break;
					case 2: // Scale
						objects[modelIdx]->scale += sSens;
						break;
				}
			}
			// z: + rotation/translation along z axis
			if (GKeys['Z']) {
				Log("z\n");
				switch (transIdx) {
					case 0: // Translate
						objects[modelIdx]->pos.z += tSens;
						break;
					case 1: // Rotate
						objects[modelIdx]->zRot += rSens;
						break;
					case 2: // Scale
						objects[modelIdx]->scale += sSens;
						break;
				}
			}
			// d: reset object rotations & translations & scaling
			if (GKeys['D']) {
				Log("d\n");
				GKeys['D']= false;
				objects[modelIdx]->pos.x = 0.0f;
				objects[modelIdx]->pos.y = 0.0f;
				objects[modelIdx]->pos.z = 0.0f;
				objects[modelIdx]->scale = 1.0f;
				objects[modelIdx]->xRot  = 0.0f;
				objects[modelIdx]->yRot  = 0.0f;
				objects[modelIdx]->zRot  = 0.0f;
				fov=45.0f;
			}
			if (GKeys[VK_UP]) {
				Log("UP\n");
				phi -= camera.speed;
			}
			if (GKeys[VK_RIGHT]) {
				Log("RIGHT\n");
				theta -= camera.speed;
			}
			if (GKeys[VK_DOWN]) {
				Log("DOWN\n");
				phi += camera.speed;
			}
			if (GKeys[VK_LEFT]) {
				Log("LEFT\n");
				theta += camera.speed;
			}
		} else {
			// Upper Case
			if (GKeys['9']){
				Log(")\n");
				--fov;
				Log("FOV: %f\n",fov);
			} else if (GKeys['0']){
				Log("(\n");
				++fov;
				Log("FOV: %f\n",fov);
			}
			// X: - rotation/translation/shrink along x axis
			if (GKeys['X']) {
				Log("X\n");
				switch (transIdx) {
					case 0: // Translate
						objects[modelIdx]->pos.x -= tSens;
						break;
					case 1: // Rotate
						objects[modelIdx]->xRot -= rSens;
						break;
					case 2: // Scale
						objects[modelIdx]->scale -= sSens;
						break;
				}
			}
			// Y: - rotation/translation along y axis
			if (GKeys['Y']) {
				Log("Y\n");
				switch (transIdx) {
					case 0: // Translate
						objects[modelIdx]->pos.y -= tSens;
						break;
					case 1: // Rotate
						objects[modelIdx]->yRot -= rSens;
						break;
					case 2: // Scale
						objects[modelIdx]->scale -= sSens;
						break;
				}
			}
			// Z: - rotation/translation along z axis
			if (GKeys['Z']) {
				Log("Z\n");
				switch (transIdx) {
					case 0:
						objects[modelIdx]->pos.z -= tSens;
						break;
					case 1:
						objects[modelIdx]->zRot -= rSens;
						break;
					case 2:
						objects[modelIdx]->scale -= sSens;
						break;
				}
			}
			// W: toggle between wirefram/flat-shading/smooth-shading
			if (GKeys['W']) {
				Log("W\n");
				Log("shadeIdx = %d\n", shadeIdx);
				++shadeIdx;
				shadeIdx %= 3;
				if (shadeIdx < 2)
					SetLigting(shadeIdx);
				GKeys['W'] = false;
			}
		}
		const static auto clip = [](float n, float lower, float upper){
			return std::max(lower, std::min(n, upper));
		};
		phi = clip(phi,1.0f,179.0f);
		const static float radius = 10.0f;
		float _theta = float(theta * PI / 180), _phi = float(phi * PI / 180);
		// Log("Theta: %d Phi: %d \n",theta,phi);
		camera.pos.x = radius * cos(_theta) * sin(_phi);
		camera.pos.y = radius * cos(_phi);
		camera.pos.z = radius * sin(_theta) * sin(_phi);
		camera.Update();
		// Rendering
		//===================================
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Projection
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov, (GLfloat)GWinW / (GLfloat)GWinH, 0.1f, 100.0f);
		// View
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(camera.pos.x, camera.pos.y, camera.pos.z, 0, 0, 0,
				  camera.up.x, camera.up.y, camera.up.z);
		// Draw Axies
		DrawAxes();
		// Model
		//glColor3f(green.r,green.g,green.b);
		glColor4d(col[colIdx].r,col[colIdx].g,col[colIdx].b,col[colIdx].a);
		float mat[]={
			col[colIdx].r,
			col[colIdx].g,
			col[colIdx].b,
			col[colIdx].a
		};
		glMaterialfv(GL_FRONT,GL_DIFFUSE,mat);
		if (shadeIdx == 2) {
				objects[modelIdx]->Draw(true);
		} else {
				objects[modelIdx]->Draw();
		}
		glFlush();
		window.SwapBuffers();
	}
	return 0;

}

