#include "OtherObjects.h"
#include <math.h>
namespace COPY {
/*
* fg_teapot.c
*
* Teapot(tm) rendering code.
*
* Copyright (c) 1999-2000 Pawel W. Olszta. All Rights Reserved.
* Written by Pawel W. Olszta, <olszta@sourceforge.net>
* Creation date: Fri Dec 24 1999
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
* PAWEL W. OLSZTA BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
* IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* notes:
* the (very little) required math is found here:
* http://www.gamasutra.com/view/feature/131848/tessellation_of_4x4_bezier_patches_.php?print=1
* a much more optimized version is here, didn't bother to implement that:
* http://www.gamasutra.com/view/feature/131794/an_indepth_look_at_bicubic_bezier_.php?print=1
*/

//#include <GL/freeglut.h>

/* -- STATIC VARS: CACHES ----------------------------------------------------
 */

/* General defs */
#define GLUT_SOLID_N_SUBDIV 8
#define GLUT_WIRE_N_SUBDIV 10

/* Bernstein coefficients only have to be precomputed once (number of patch
* subdivisions is fixed)
* Can thus define arrays for them here, they will be filled upon first use.
* 3rd order Bezier surfaces have 4 Bernstein coeffs.
* Have separate caches for solid and wire as they use a different number of
* subdivisions
* _0 is for Bernstein polynomials, _1 for their first derivative (which we need
* for normals)
*/
static GLfloat bernWire_0[GLUT_WIRE_N_SUBDIV][4];
static GLfloat bernWire_1[GLUT_WIRE_N_SUBDIV][4];
static GLfloat bernSolid_0[GLUT_SOLID_N_SUBDIV][4];
static GLfloat bernSolid_1[GLUT_SOLID_N_SUBDIV][4];

/* Teapot defs */
#define GLUT_TEAPOT_N_PATCHES                                                  \
	(6 * 4 + 4 * 2) /* 6 patches are reproduced (rotated) 4 times, 4 patches   \
					   (flipped) 2 times */
#define GLUT_SOLID_TEAPOT_N_VERT                                               \
	GLUT_SOLID_N_SUBDIV* GLUT_SOLID_N_SUBDIV*                                  \
						 GLUT_TEAPOT_N_PATCHES /* N_SUBDIV^2 vertices per patch */
#define GLUT_SOLID_TEAPOT_N_TRI                                                \
	(GLUT_SOLID_N_SUBDIV - 1) * (GLUT_SOLID_N_SUBDIV - 1) *                    \
		GLUT_TEAPOT_N_PATCHES *                                                \
		2 /* if e.g. 7x7 vertices for each patch, there are 6*6 squares for    \
			 each patch. Each square is decomposed into 2 triangles */

#define GLUT_WIRE_TEAPOT_N_VERT                                                \
	GLUT_WIRE_N_SUBDIV* GLUT_WIRE_N_SUBDIV*                                    \
						GLUT_TEAPOT_N_PATCHES /* N_SUBDIV^2 vertices per patch */

/* Bit of caching:
* vertex indices and normals only need to be generated once for
* a given number of subdivisions as they don't change with scale.
* Vertices can be cached and reused if scale didn't change.
*/
static GLushort  vertIdxsTeapotS[GLUT_SOLID_TEAPOT_N_TRI * 3];
static GLfloat   normsTeapotS[GLUT_SOLID_TEAPOT_N_VERT * 3];
static GLfloat   vertsTeapotS[GLUT_SOLID_TEAPOT_N_VERT * 3];
static GLfloat   texcsTeapotS[GLUT_SOLID_TEAPOT_N_VERT * 2];
static GLfloat   lastScaleTeapotS = 0.f;
static GLboolean initedTeapotS	= GL_FALSE;

static GLushort  vertIdxsTeapotW[GLUT_WIRE_TEAPOT_N_VERT * 2];
static GLfloat   normsTeapotW[GLUT_WIRE_TEAPOT_N_VERT * 3];
static GLfloat   vertsTeapotW[GLUT_WIRE_TEAPOT_N_VERT * 3];
static GLfloat   lastScaleTeapotW = 0.f;
static GLboolean initedTeapotW	= GL_FALSE;

/* Teacup defs */
#define GLUT_TEACUP_N_PATCHES                                                  \
	(6 * 4 + 1 * 2) /* 6 patches are reproduced (rotated) 4 times, 1 patch     \
					   (flipped) 2 times */
#define GLUT_SOLID_TEACUP_N_VERT                                               \
	GLUT_SOLID_N_SUBDIV* GLUT_SOLID_N_SUBDIV*                                  \
						 GLUT_TEACUP_N_PATCHES /* N_SUBDIV^2 vertices per patch */
#define GLUT_SOLID_TEACUP_N_TRI                                                \
	(GLUT_SOLID_N_SUBDIV - 1) * (GLUT_SOLID_N_SUBDIV - 1) *                    \
		GLUT_TEACUP_N_PATCHES *                                                \
		2 /* if e.g. 7x7 vertices for each patch, there are 6*6 squares for    \
			 each patch. Each square is decomposed into 2 triangles */

#define GLUT_WIRE_TEACUP_N_VERT                                                \
	GLUT_WIRE_N_SUBDIV* GLUT_WIRE_N_SUBDIV*                                    \
						GLUT_TEACUP_N_PATCHES /* N_SUBDIV^2 vertices per patch */

/* Bit of caching:
* vertex indices and normals only need to be generated once for
* a given number of subdivisions as they don't change with scale.
* Vertices can be cached and reused if scale didn't change.
*/
static GLushort  vertIdxsTeacupS[GLUT_SOLID_TEACUP_N_TRI * 3];
static GLfloat   normsTeacupS[GLUT_SOLID_TEACUP_N_VERT * 3];
static GLfloat   vertsTeacupS[GLUT_SOLID_TEACUP_N_VERT * 3];
static GLfloat   texcsTeacupS[GLUT_SOLID_TEACUP_N_VERT * 2];
static GLfloat   lastScaleTeacupS = 0.f;
static GLboolean initedTeacupS	= GL_FALSE;

static GLushort  vertIdxsTeacupW[GLUT_WIRE_TEACUP_N_VERT * 2];
static GLfloat   normsTeacupW[GLUT_WIRE_TEACUP_N_VERT * 3];
static GLfloat   vertsTeacupW[GLUT_WIRE_TEACUP_N_VERT * 3];
static GLfloat   lastScaleTeacupW = 0.f;
static GLboolean initedTeacupW	= GL_FALSE;

/* Teaspoon defs */
#define GLUT_TEASPOON_N_PATCHES GLUT_TEASPOON_N_INPUT_PATCHES
#define GLUT_SOLID_TEASPOON_N_VERT                                             \
	GLUT_SOLID_N_SUBDIV* GLUT_SOLID_N_SUBDIV*                                  \
						 GLUT_TEASPOON_N_PATCHES /* N_SUBDIV^2 vertices per patch */
#define GLUT_SOLID_TEASPOON_N_TRI                                              \
	(GLUT_SOLID_N_SUBDIV - 1) * (GLUT_SOLID_N_SUBDIV - 1) *                    \
		GLUT_TEASPOON_N_PATCHES *                                              \
		2 /* if e.g. 7x7 vertices for each patch, there are 6*6 squares for    \
			 each patch. Each square is decomposed into 2 triangles */

#define GLUT_WIRE_TEASPOON_N_VERT                                              \
	GLUT_WIRE_N_SUBDIV* GLUT_WIRE_N_SUBDIV*                                    \
						GLUT_TEASPOON_N_PATCHES /* N_SUBDIV^2 vertices per patch */

/* Bit of caching:
* vertex indices and normals only need to be generated once for
* a given number of subdivisions as they don't change with scale.
* Vertices can be cached and reused if scale didn't change.
*/
static GLushort  vertIdxsTeaspoonS[GLUT_SOLID_TEASPOON_N_TRI * 3];
static GLfloat   normsTeaspoonS[GLUT_SOLID_TEASPOON_N_VERT * 3];
static GLfloat   vertsTeaspoonS[GLUT_SOLID_TEASPOON_N_VERT * 3];
static GLfloat   texcsTeaspoonS[GLUT_SOLID_TEASPOON_N_VERT * 2];
static GLfloat   lastScaleTeaspoonS = 0.f;
static GLboolean initedTeaspoonS	= GL_FALSE;

static GLushort  vertIdxsTeaspoonW[GLUT_WIRE_TEASPOON_N_VERT * 2];
static GLfloat   normsTeaspoonW[GLUT_WIRE_TEASPOON_N_VERT * 3];
static GLfloat   vertsTeaspoonW[GLUT_WIRE_TEASPOON_N_VERT * 3];
static GLfloat   lastScaleTeaspoonW = 0.f;
static GLboolean initedTeaspoonW	= GL_FALSE;

/* -- PRIVATE FUNCTIONS ---------------------------------------------------- */
void fghDrawGeometrySolid11(GLfloat* vertices, GLfloat* normals,
							GLfloat* textcs, GLsizei numVertices,
							GLushort* vertIdxs, GLsizei numParts,
							GLsizei numVertIdxsPerPart) {
	int i;

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, vertices);
	glNormalPointer(GL_FLOAT, 0, normals);

	if (textcs) {
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, 0, textcs);
	}

	if (!vertIdxs)
		glDrawArrays(GL_TRIANGLES, 0, numVertices);
	else if (numParts > 1)
		for (i = 0; i < numParts; i++)
			glDrawElements(GL_TRIANGLE_STRIP, numVertIdxsPerPart,
						   GL_UNSIGNED_SHORT,
						   vertIdxs + i * numVertIdxsPerPart);
	else
		glDrawElements(GL_TRIANGLES, numVertIdxsPerPart, GL_UNSIGNED_SHORT,
					   vertIdxs);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	if (textcs)
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void fghDrawGeometryWire11(GLfloat* vertices, GLfloat* normals,
						   GLushort* vertIdxs, GLsizei numParts,
						   GLsizei numVertPerPart, GLenum vertexMode,
						   GLushort* vertIdxs2, GLsizei numParts2,
						   GLsizei numVertPerPart2) {
	int i;

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, vertices);
	glNormalPointer(GL_FLOAT, 0, normals);

	if (!vertIdxs)
		/* Draw per face (TODO: could use glMultiDrawArrays if available) */
		for (i = 0; i < numParts; i++)
			glDrawArrays(vertexMode, i * numVertPerPart, numVertPerPart);
	else
		for (i = 0; i < numParts; i++)
			glDrawElements(vertexMode, numVertPerPart, GL_UNSIGNED_SHORT,
						   vertIdxs + i * numVertPerPart);

	if (vertIdxs2)
		for (i = 0; i < numParts2; i++)
			glDrawElements(GL_LINE_LOOP, numVertPerPart2, GL_UNSIGNED_SHORT,
						   vertIdxs2 + i * numVertPerPart2);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}
void fghDrawGeometrySolid(GLfloat* vertices, GLfloat* normals, GLfloat* textcs,
						  GLsizei numVertices, GLushort* vertIdxs,
						  GLsizei numParts, GLsizei numVertIdxsPerPart) {

	fghDrawGeometrySolid11(vertices, normals, textcs, numVertices, vertIdxs,
						   numParts, numVertIdxsPerPart);
}

void fghDrawGeometryWire(GLfloat* vertices, GLfloat* normals,
						 GLsizei numVertices, GLushort* vertIdxs,
						 GLsizei numParts, GLsizei numVertPerPart,
						 GLenum vertexMode, GLushort* vertIdxs2,
						 GLsizei numParts2, GLsizei numVertPerPart2) {
	fghDrawGeometryWire11(vertices, normals, vertIdxs, numParts, numVertPerPart,
						  vertexMode, vertIdxs2, numParts2, numVertPerPart2);
}

/* evaluate 3rd order Bernstein polynomial and its 1st deriv */
static void bernstein3(int i, GLfloat x, GLfloat* r0, GLfloat* r1) {
	float invx = 1.f - x;

	/* r0: zero order coeff, r1: first deriv coeff */
	switch (i) {
		GLfloat temp;
		case 0:
			temp = invx * invx;
			*r0  = invx * temp; /* invx * invx * invx */
			*r1  = -3 * temp;   /*   -3 * invx * invx */
			break;
		case 1:
			temp = invx * invx;
			*r0  = 3 * x * temp; /* 3 * x * invx * invx */
			*r1 =
				3 * temp - 6 * x * invx; /* 3 * invx * invx  -  6 * x * invx */
			break;
		case 2:
			temp = x * x;
			*r0  = 3 * temp * invx;			/* 3 * x * x * invx */
			*r1  = 6 * x * invx - 3 * temp; /* 6 * x * invx  -  3 * x * x */
			break;
		case 3:
			temp = x * x;
			*r0  = x * temp; /* x * x * x */
			*r1  = 3 * temp; /* 3 * x * x */
			break;
		default:
			*r0 = *r1 = 0;
	}
}

static void pregenBernstein(int nSubDivs, GLfloat (*bern_0)[4],
							GLfloat (*bern_1)[4]) {
	int s, i;
	for (s = 0; s < nSubDivs; s++) {
		GLfloat x = s / (nSubDivs - 1.f);
		for (i = 0; i < 4; i++) /* 3rd order polynomial */
			bernstein3(i, x, bern_0[s] + i, bern_1[s] + i);
	}
}

/* based on flag either rotate patches around y axis to other 3 quadrants
 * (flag=4) or reflect patch across x-y plane (flag=2) */
static void rotOrReflect(int flag, int nVals, int nSubDivs, GLfloat* vals) {
	int u, i, o;

	if (flag == 4) {
		int i1 = nVals, i2 = nVals * 2, i3 = nVals * 3;
		for (o = 0; o < nVals; o += 3) {
			/* 90� rotation */
			vals[i1 + o + 0] = vals[o + 2];
			vals[i1 + o + 1] = vals[o + 1];
			vals[i1 + o + 2] = -vals[o + 0];
			/* 180� rotation */
			vals[i2 + o + 0] = -vals[o + 0];
			vals[i2 + o + 1] = vals[o + 1];
			vals[i2 + o + 2] = -vals[o + 2];
			/* 270� rotation */
			vals[i3 + o + 0] = -vals[o + 2];
			vals[i3 + o + 1] = vals[o + 1];
			vals[i3 + o + 2] = vals[o + 0];
		}
	} else if (flag == 2) {
		/* copy over values, reversing row order to keep winding correct, and
		 * negating z to perform the flip */
		for (u = 0; u < nSubDivs; u++) /* per row */
		{
			int off =
				(nSubDivs - u - 1) * nSubDivs *
				3; /* read last row first from the already existing rows */
			o = nVals +
				u * nSubDivs * 3; /* write last row as first row to output */
			for (i = 0; i < nSubDivs * 3;
				 i += 3, o += 3) /* each row has nSubDivs points consisting of
									three values */
			{
				vals[o + 0] = vals[off + i + 0];
				vals[o + 1] = vals[off + i + 1];
				vals[o + 2] = -vals[off + i + 2];
			}
		}
	}
}

/* verts array should be initialized to 0! */
static int evalBezierWithNorm(GLfloat cp[4][4][3], int nSubDivs,
							  float (*bern_0)[4], float (*bern_1)[4], int flag,
							  int normalFix, GLfloat* verts, GLfloat* norms) {
	int nVerts	= nSubDivs * nSubDivs;
	int nVertVals = nVerts * 3; /* number of values output for one patch, flag
								   (2 or 4) indicates how many times we will
								   write this to output */
	int u, v, i, j, o;

	/* generate vertices and coordinates for the patch */
	for (u = 0, o = 0; u < nSubDivs; u++) {
		for (v = 0; v < nSubDivs; v++, o += 3) {
			/* for normals, get two tangents at the vertex using partial
			 * derivatives of 2D Bezier grid */
			float tan1[3] = {0}, tan2[3] = {0}, len;
			for (i = 0; i <= 3; i++) {
				float vert_0[3] = {0}, vert_1[3] = {0};
				for (j = 0; j <= 3; j++) {
					vert_0[0] += bern_0[v][j] * cp[i][j][0];
					vert_0[1] += bern_0[v][j] * cp[i][j][1];
					vert_0[2] += bern_0[v][j] * cp[i][j][2];

					vert_1[0] += bern_1[v][j] * cp[i][j][0];
					vert_1[1] += bern_1[v][j] * cp[i][j][1];
					vert_1[2] += bern_1[v][j] * cp[i][j][2];
				}

				verts[o + 0] += bern_0[u][i] * vert_0[0];
				verts[o + 1] += bern_0[u][i] * vert_0[1];
				verts[o + 2] += bern_0[u][i] * vert_0[2];

				tan1[0] += bern_0[u][i] * vert_1[0];
				tan1[1] += bern_0[u][i] * vert_1[1];
				tan1[2] += bern_0[u][i] * vert_1[2];
				tan2[0] += bern_1[u][i] * vert_0[0];
				tan2[1] += bern_1[u][i] * vert_0[1];
				tan2[2] += bern_1[u][i] * vert_0[2];
			}
			/* get normal through cross product of the two tangents of the
			 * vertex */
			norms[o + 0] = tan1[1] * tan2[2] - tan1[2] * tan2[1];
			norms[o + 1] = tan1[2] * tan2[0] - tan1[0] * tan2[2];
			norms[o + 2] = tan1[0] * tan2[1] - tan1[1] * tan2[0];
			len			 = (GLfloat)sqrt(norms[o + 0] * norms[o + 0] +
								norms[o + 1] * norms[o + 1] +
								norms[o + 2] * norms[o + 2]);
			norms[o + 0] /= len;
			norms[o + 1] /= len;
			norms[o + 2] /= len;
		}
	}

	/* Fix normal vector if needed */
	if (normalFix) {
		for (o = 0; o < nSubDivs * 3;
			 o += 3) /* whole first row (first nSubDivs normals) is broken:
						replace normals for the whole row */
		{
			norms[o + 0] = 0.f;
			norms[o + 1] = normalFix == 1 ? 1.f : -1.f;
			norms[o + 2] = 0.f;
		}
	}

	/* now based on flag either rotate patches around y axis to other 3
	 * quadrants (flag=4) or reflect patch across x-y plane (flag=2) */
	rotOrReflect(flag, nVertVals, nSubDivs, verts);
	rotOrReflect(flag, nVertVals, nSubDivs, norms);

	return nVertVals * flag;
}

/* verts array should be initialized to 0! */
static int evalBezier(GLfloat cp[4][4][3], int nSubDivs, float (*bern_0)[4],
					  int flag, GLfloat* verts) {
	int nVerts	= nSubDivs * nSubDivs;
	int nVertVals = nVerts * 3; /* number of values output for one patch, flag
								   (2 or 4) indicates how many times we will
								   write this to output */
	int u, v, i, j, o;

	/* generate vertices and coordinates for the patch */
	for (u = 0, o = 0; u < nSubDivs; u++) {
		for (v = 0; v < nSubDivs; v++, o += 3) {
			for (i = 0; i <= 3; i++) {
				float vert_0[3] = {0};
				for (j = 0; j <= 3; j++) {
					vert_0[0] += bern_0[v][j] * cp[i][j][0];
					vert_0[1] += bern_0[v][j] * cp[i][j][1];
					vert_0[2] += bern_0[v][j] * cp[i][j][2];
				}

				verts[o + 0] += bern_0[u][i] * vert_0[0];
				verts[o + 1] += bern_0[u][i] * vert_0[1];
				verts[o + 2] += bern_0[u][i] * vert_0[2];
			}
		}
	}

	/* now based on flag either rotate patches around y axis to other 3
	 * quadrants (flag=4) or reflect patch across x-y plane (flag=2) */
	rotOrReflect(flag, nVertVals, nSubDivs, verts);

	return nVertVals * flag;
}

static void fghTeaset(GLfloat scale, GLboolean useWireMode,
					  GLfloat (*cpdata)[3], int (*patchdata)[16],
					  GLushort* vertIdxs, GLfloat* verts, GLfloat* norms,
					  GLfloat* texcs, GLfloat* lastScale, GLboolean* inited,
					  GLboolean needNormalFix, GLboolean rotFlip,
					  GLfloat zOffset, int nVerts, int nInputPatches,
					  int nPatches, int nTriangles) {
	/* for internal use */
	int		p, o;
	GLfloat cp[4][4][3];
	/* to hold pointers to static vars/arrays */
	GLfloat(*bern_0)[4], (*bern_1)[4];
	int nSubDivs;

	/* Get relevant static arrays and variables */
	bern_0   = useWireMode ? bernWire_0 : bernSolid_0;
	bern_1   = useWireMode ? bernWire_1 : bernSolid_1;
	nSubDivs = useWireMode ? GLUT_WIRE_N_SUBDIV : GLUT_SOLID_N_SUBDIV;

	/* check if need to generate vertices */
	if (!*inited || scale != *lastScale) {
		/* set vertex array to all 0 (not necessary for normals and vertex
		 * indices) */
		memset(verts, 0, nVerts * 3 * sizeof(GLfloat));

		/* pregen Berstein polynomials and their first derivatives (for normals)
		 */
		if (!*inited)
			pregenBernstein(nSubDivs, bern_0, bern_1);

		/* generate vertices and normals */
		for (p = 0, o = 0; p < nInputPatches; p++) {
			/* set flags for evalBezier function */
			int flag = rotFlip ? p < 6 ? 4 : 2 : 1; /* For teapot and teacup,
													   first six patches get 3
													   copies (rotations),
													   others get 2 copies
													   (flips). No rotating or
													   flipping at all for
													   teaspoon */
			int normalFix = needNormalFix
								? p == 3 ? 1 : p == 5 ? 2 : 0
								: 0; /* For teapot, fix normal vectors for
										vertices on top of lid (patch 4) and
										on middle of bottom (patch 6).
										Different flag value as different
										normal needed */

			/* collect control points */
			int i;
			for (i = 0; i < 16; i++) {
				/* Original code draws with a 270� rot around X axis, a scaling
				* and a translation along the Z-axis.
				* Incorporating these in the control points is much cheaper than
				* transforming all the vertices.
				* Original:
				* glRotated( 270.0, 1.0, 0.0, 0.0 );
				* glScaled( 0.5 * scale, 0.5 * scale, 0.5 * scale );
				* glTranslated( 0.0, 0.0, -zOffset );  -> was 1.5 for teapot,
				* but should be 1.575 to center it on the Z axis. Teacup and
				* teaspoon have different offsets
				*/
				cp[i / 4][i % 4][0] = cpdata[patchdata[p][i]][0] * scale / 2.f;
				cp[i / 4][i % 4][1] =
					(cpdata[patchdata[p][i]][2] - zOffset) * scale / 2.f;
				cp[i / 4][i % 4][2] = -cpdata[patchdata[p][i]][1] * scale / 2.f;
			}

			/* eval bezier patch */
			if (!*inited) /* first time, generate normals as well */
				o += evalBezierWithNorm(cp, nSubDivs, bern_0, bern_1, flag,
										normalFix, verts + o, norms + o);
			else /* only need to regen vertices */
				o += evalBezier(cp, nSubDivs, bern_0, flag, verts + o);
		}
		*lastScale = scale;

		if (!*inited) {
			int r, c;
			/* generate texture coordinates if solid teapot/teacup/teaspoon */
			if (!useWireMode) {
				/* generate for first patch */
				for (r = 0, o = 0; r < nSubDivs; r++) {
					GLfloat u = r / (nSubDivs - 1.f);
					for (c = 0; c < nSubDivs; c++, o += 2) {
						GLfloat v	= c / (nSubDivs - 1.f);
						texcs[o + 0] = u;
						texcs[o + 1] = v;
					}
				}
				/* copy it over for all the other patches */
				for (p = 1; p < nPatches; p++)
					memcpy(texcs + p * nSubDivs * nSubDivs * 2, texcs,
						   nSubDivs * nSubDivs * 2 * sizeof(GLfloat));
			}

			/* build vertex index array */
			if (useWireMode) {
				/* build vertex indices to draw teapot/teacup/teaspoon as line
				 * strips */
				/* first strips along increasing u, constant v */
				for (p = 0, o = 0; p < nPatches; p++) {
					int idx = nSubDivs * nSubDivs * p;
					for (c = 0; c < nSubDivs; c++)
						for (r			= 0; r < nSubDivs; r++, o++)
							vertIdxs[o] = idx + r * nSubDivs + c;
				}

				/* then strips along increasing v, constant u */
				for (p = 0; p < nPatches;
					 p++) /* don't reset o, we continue appending! */
				{
					int idx = nSubDivs * nSubDivs * p;
					for (r = 0; r < nSubDivs; r++) {
						int loc = r * nSubDivs;
						for (c			= 0; c < nSubDivs; c++, o++)
							vertIdxs[o] = idx + loc + c;
					}
				}
			} else {
				/* build vertex indices to draw teapot/teacup/teaspoon as
				 * triangles */
				for (p = 0, o = 0; p < nPatches; p++) {
					int idx = nSubDivs * nSubDivs * p;
					for (r = 0; r < nSubDivs - 1; r++) {
						int loc = r * nSubDivs;
						for (c = 0; c < nSubDivs - 1; c++, o += 6) {
							/* ABC ACD, where B and C are one row lower */
							int row1 = idx + loc + c;
							int row2 = row1 + nSubDivs;

							vertIdxs[o + 0] = row1 + 0;
							vertIdxs[o + 1] = row2 + 0;
							vertIdxs[o + 2] = row2 + 1;

							vertIdxs[o + 3] = row1 + 0;
							vertIdxs[o + 4] = row2 + 1;
							vertIdxs[o + 5] = row1 + 1;
						}
					}
				}
			}

			*inited = GL_TRUE;
		}
	}

	/* draw */
	if (useWireMode)
		fghDrawGeometryWire(verts, norms, nVerts, vertIdxs,
							nPatches * nSubDivs * 2, nSubDivs, GL_LINE_STRIP,
							NULL, 0, 0);
	else
		fghDrawGeometrySolid(verts, norms, texcs, nVerts, vertIdxs, 1,
							 nTriangles * 3);
}

/* -- INTERFACE FUNCTIONS -------------------------------------------------- */

/*
* Renders a wired teapot...
*/
void glutWireTeapot(double size) {
	fghTeaset((GLfloat)size, GL_TRUE, cpdata_teapot, patchdata_teapot,
			  vertIdxsTeapotW, vertsTeapotW, normsTeapotW, NULL,
			  &lastScaleTeapotW, &initedTeapotW, GL_TRUE, GL_TRUE, 1.575f,
			  GLUT_WIRE_TEAPOT_N_VERT, GLUT_TEAPOT_N_INPUT_PATCHES,
			  GLUT_TEAPOT_N_PATCHES, 0);
}

/*
* Renders a filled teapot...
*/
void glutSolidTeapot(double size) {
	fghTeaset((GLfloat)size, GL_FALSE, cpdata_teapot, patchdata_teapot,
			  vertIdxsTeapotS, vertsTeapotS, normsTeapotS, texcsTeapotS,
			  &lastScaleTeapotS, &initedTeapotS, GL_TRUE, GL_TRUE, 1.575f,
			  GLUT_SOLID_TEAPOT_N_VERT, GLUT_TEAPOT_N_INPUT_PATCHES,
			  GLUT_TEAPOT_N_PATCHES, GLUT_SOLID_TEAPOT_N_TRI);
}

/*
* Renders a wired teacup...
*/
void glutWireTeacup(double size) {
	fghTeaset((GLfloat)size / 2.5f, GL_TRUE, cpdata_teacup, patchdata_teacup,
			  vertIdxsTeacupW, vertsTeacupW, normsTeacupW, NULL,
			  &lastScaleTeacupW, &initedTeacupW, GL_FALSE, GL_TRUE, 1.5121f,
			  GLUT_WIRE_TEACUP_N_VERT, GLUT_TEACUP_N_INPUT_PATCHES,
			  GLUT_TEACUP_N_PATCHES, 0);
}

/*
* Renders a filled teacup...
*/
void glutSolidTeacup(double size) {
	fghTeaset((GLfloat)size / 2.5f, GL_FALSE, cpdata_teacup, patchdata_teacup,
			  vertIdxsTeacupS, vertsTeacupS, normsTeacupS, texcsTeacupS,
			  &lastScaleTeacupS, &initedTeacupS, GL_FALSE, GL_TRUE, 1.5121f,
			  GLUT_SOLID_TEACUP_N_VERT, GLUT_TEACUP_N_INPUT_PATCHES,
			  GLUT_TEACUP_N_PATCHES, GLUT_SOLID_TEACUP_N_TRI);
}

/*
* Renders a wired teaspoon...
*/
void glutWireTeaspoon(double size) {
	fghTeaset((GLfloat)size / 2.5f, GL_TRUE, cpdata_teaspoon,
			  patchdata_teaspoon, vertIdxsTeaspoonW, vertsTeaspoonW,
			  normsTeaspoonW, NULL, &lastScaleTeaspoonW, &initedTeaspoonW,
			  GL_FALSE, GL_FALSE, -0.0315f, GLUT_WIRE_TEASPOON_N_VERT,
			  GLUT_TEASPOON_N_INPUT_PATCHES, GLUT_TEASPOON_N_PATCHES, 0);
}

/*
* Renders a filled teaspoon...
*/
void glutSolidTeaspoon(double size) {
	fghTeaset((GLfloat)size / 2.5f, GL_FALSE, cpdata_teaspoon,
			  patchdata_teaspoon, vertIdxsTeaspoonS, vertsTeaspoonS,
			  normsTeaspoonS, texcsTeaspoonS, &lastScaleTeaspoonS,
			  &initedTeaspoonS, GL_FALSE, GL_FALSE, -0.0315f,
			  GLUT_SOLID_TEASPOON_N_VERT, GLUT_TEASPOON_N_INPUT_PATCHES,
			  GLUT_TEASPOON_N_PATCHES, GLUT_SOLID_TEASPOON_N_TRI);
}

void fghCircleTable(GLfloat** sint, GLfloat** cost, const int n,
					const GLboolean halfCircle) {
	int i;

	/* Table size, the sign of n flips the circle direction */
	const int size = abs(n);

	/* Determine the angle between samples */
	const GLfloat angle =
		(halfCircle ? 1 : 2) * (GLfloat)M_PI / (GLfloat)((n == 0) ? 1 : n);

	/* Allocate memory for n samples, plus duplicate of first entry at the end
	 */
	*sint = (GLfloat*)malloc(sizeof(GLfloat) * (size + 1));
	*cost = (GLfloat*)malloc(sizeof(GLfloat) * (size + 1));

	/* Bail out if memory allocation fails, fgError never returns */
	if (!(*sint) || !(*cost)) {
		free(*sint);
		free(*cost);
		// fgError("Failed to allocate memory in fghCircleTable");
	}

	/* Compute cos and sin around the circle */
	(*sint)[0] = 0.0;
	(*cost)[0] = 1.0;

	for (i = 1; i < size; i++) {
		(*sint)[i] = (GLfloat)sin(angle * i);
		(*cost)[i] = (GLfloat)cos(angle * i);
	}

	if (halfCircle) {
		(*sint)[size] = 0.0f;  /* sin PI */
		(*cost)[size] = -1.0f; /* cos PI */
	} else {
		/* Last sample is duplicate of the first (sin or cos of 2 PI) */
		(*sint)[size] = (*sint)[0];
		(*cost)[size] = (*cost)[0];
	}
}

void fghGenerateCone(GLfloat base, GLfloat height, GLint slices,
					 GLint	 stacks, /*  input */
					 GLfloat** vertices, GLfloat** normals,
					 int* nVert /* output */
					 ) {
	int i, j;
	int idx = 0; /* idx into vertex/normal buffer */

	/* Pre-computed circle */
	GLfloat *sint, *cost;

	/* Step in z and radius as stacks are drawn. */
	GLfloat z = 0;
	GLfloat r = (GLfloat)base;

	const GLfloat zStep = (GLfloat)height / ((stacks > 0) ? stacks : 1);
	const GLfloat rStep = (GLfloat)base / ((stacks > 0) ? stacks : 1);

	/* Scaling factors for vertex normals */
	const GLfloat cosn =
		(GLfloat)(height / sqrt(height * height + base * base));
	const GLfloat sinn = (GLfloat)(base / sqrt(height * height + base * base));

	/* number of unique vertices */
	if (slices == 0 || stacks < 1) {
		/* nothing to generate */
		*nVert = 0;
		return;
	}
	*nVert =
		slices * (stacks + 2) +
		1; /* need an extra stack for closing off bottom with correct normals */

	if ((*nVert) > 65535)
		/*
		* limit of glushort, thats 256*256 subdivisions, should be enough in
		* practice. See note above
		*/
		// fgWarning("fghGenerateCone: too many slices or stacks requested,
		// indices will wrap");

		/* Pre-computed circle */
		fghCircleTable(&sint, &cost, -slices, GL_FALSE);

	/* Allocate vertex and normal buffers, bail out if memory allocation fails
	 */
	*vertices = (GLfloat*)malloc((*nVert) * 3 * sizeof(GLfloat));
	*normals  = (GLfloat*)malloc((*nVert) * 3 * sizeof(GLfloat));
	if (!(*vertices) || !(*normals)) {
		free(*vertices);
		free(*normals);
		// fgError("Failed to allocate memory in fghGenerateCone");
	}

	/* bottom */
	(*vertices)[0] = 0.f;
	(*vertices)[1] = 0.f;
	(*vertices)[2] = z;
	(*normals)[0]  = 0.f;
	(*normals)[1]  = 0.f;
	(*normals)[2]  = -1.f;
	idx			   = 3;
	/* other on bottom (get normals right) */
	for (j = 0; j < slices; j++, idx += 3) {
		(*vertices)[idx]	 = cost[j] * r;
		(*vertices)[idx + 1] = sint[j] * r;
		(*vertices)[idx + 2] = z;
		(*normals)[idx]		 = 0.f;
		(*normals)[idx + 1]  = 0.f;
		(*normals)[idx + 2]  = -1.f;
	}

	/* each stack */
	for (i = 0; i < stacks + 1; i++) {
		for (j = 0; j < slices; j++, idx += 3) {
			(*vertices)[idx]	 = cost[j] * r;
			(*vertices)[idx + 1] = sint[j] * r;
			(*vertices)[idx + 2] = z;
			(*normals)[idx]		 = cost[j] * cosn;
			(*normals)[idx + 1]  = sint[j] * cosn;
			(*normals)[idx + 2]  = sinn;
		}

		z += zStep;
		r -= rStep;
	}

	/* Release sin and cos tables */
	free(sint);
	free(cost);
}

void fghCone(GLfloat base, GLfloat height, GLint slices, GLint stacks,
			 GLboolean useWireMode) {
	int		 i, j, idx, nVert;
	GLfloat *vertices, *normals;

	/* Generate vertices and normals */
	/* Note, (stacks+1)*slices vertices for side of object, slices+1 for top and
	 * bottom closures */
	fghGenerateCone(base, height, slices, stacks, &vertices, &normals, &nVert);

	if (nVert == 0)
		/* nothing to draw */
		return;

	if (useWireMode) {
		GLushort *sliceIdx, *stackIdx;
		/* First, generate vertex index arrays for drawing with glDrawElements
		* We have a bunch of line_loops to draw for each stack, and a
		* bunch for each slice.
		*/

		stackIdx = (GLushort*)malloc(slices * stacks * sizeof(GLushort));
		sliceIdx = (GLushort*)malloc(slices * 2 * sizeof(GLushort));
		if (!(stackIdx) || !(sliceIdx)) {
			free(stackIdx);
			free(sliceIdx);
			// fgError("Failed to allocate memory in fghCone");
		}

		/* generate for each stack */
		for (i = 0, idx = 0; i < stacks; i++) {
			GLushort offset = 1 + (i + 1) * slices; /* start at 1 (0 is top
													   vertex), and we advance
													   one stack down as we go
													   along */
			for (j = 0; j < slices; j++, idx++) {
				stackIdx[idx] = offset + j;
			}
		}

		/* generate for each slice */
		for (i = 0, idx = 0; i < slices; i++) {
			GLushort offset = 1 + i; /* start at 1 (0 is top vertex), and we
										advance one slice as we go along */
			sliceIdx[idx++] = offset + slices;
			sliceIdx[idx++] = offset + (stacks + 1) * slices;
		}

		/* draw */
		fghDrawGeometryWire(vertices, normals, nVert, sliceIdx, 1, slices * 2,
							GL_LINES, stackIdx, stacks, slices);

		/* cleanup allocated memory */
		free(sliceIdx);
		free(stackIdx);
	} else {
		/* First, generate vertex index arrays for drawing with glDrawElements
		* All stacks, including top and bottom are covered with a triangle
		* strip.
		*/
		GLushort* stripIdx;
		/* Create index vector */
		GLushort offset;

		/* Allocate buffers for indices, bail out if memory allocation fails */
		stripIdx = (GLushort*)malloc(
			(slices + 1) * 2 * (stacks + 1) *
			sizeof(GLushort)); /*stacks +1 because of closing off bottom */
		if (!(stripIdx)) {
			free(stripIdx);
			// fgError("Failed to allocate memory in fghCone");
		}

		/* top stack */
		for (j = 0, idx = 0; j < slices; j++, idx += 2) {
			stripIdx[idx] = 0;
			stripIdx[idx + 1] =
				j + 1; /* 0 is top vertex, 1 is first for first stack */
		}
		stripIdx[idx] = 0; /* repeat first slice's idx for closing off shape */
		stripIdx[idx + 1] = 1;
		idx += 2;

		/* middle stacks: */
		/* Strip indices are relative to first index belonging to strip, NOT
		 * relative to first vertex/normal pair in array */
		for (i = 0; i < stacks; i++, idx += 2) {
			offset = 1 +
					 (i + 1) * slices; /* triangle_strip indices start at 1 (0
										  is top vertex), and we advance one
										  stack down as we go along */
			for (j = 0; j < slices; j++, idx += 2) {
				stripIdx[idx]	 = offset + j;
				stripIdx[idx + 1] = offset + j + slices;
			}
			stripIdx[idx] =
				offset; /* repeat first slice's idx for closing off shape */
			stripIdx[idx + 1] = offset + slices;
		}

		/* draw */
		fghDrawGeometrySolid(vertices, normals, NULL, nVert, stripIdx,
							 stacks + 1, (slices + 1) * 2);

		/* cleanup allocated memory */
		free(stripIdx);
	}

	/* cleanup allocated memory */
	free(vertices);
	free(normals);
}

/*
* Draws a solid cone
*/
void glutSolidCone(double base, double height, GLint slices, GLint stacks) {

	fghCone((GLfloat)base, (GLfloat)height, slices, stacks, GL_FALSE);
}

/*
* Draws a wire cone
*/
void glutWireCone(double base, double height, GLint slices, GLint stacks) {

	fghCone((GLfloat)base, (GLfloat)height, slices, stacks, GL_TRUE);
}

void fghGenerateSphere(GLfloat radius, GLint slices, GLint stacks,
					   GLfloat** vertices, GLfloat** normals, int* nVert) {
	int		i, j;
	int		idx = 0; /* idx into vertex/normal buffer */
	GLfloat x, y, z;

	/* Pre-computed circle */
	GLfloat *sint1 = nullptr, *cost1 = nullptr;
	GLfloat *sint2 = nullptr, *cost2 = nullptr;

	/* number of unique vertices */
	if (slices == 0 || stacks < 2) {
		/* nothing to generate */
		*nVert = 0;
		return;
	}
	*nVert = slices * (stacks - 1) + 2;
	// if ((*nVert) > 65535)
	/*
	* limit of glushort, thats 256*256 subdivisions, should be enough in
	* practice. See note above
	*/
	// fgWarning("fghGenerateSphere: too many slices or stacks requested,
	// indices will wrap");

	/* precompute values on unit circle */
	fghCircleTable(&sint1, &cost1, -slices, GL_FALSE);
	fghCircleTable(&sint2, &cost2, stacks, GL_TRUE);

	/* Allocate vertex and normal buffers, bail out if memory allocation fails
	 */
	*vertices = (GLfloat*)malloc((*nVert) * 3 * sizeof(GLfloat));
	*normals  = (GLfloat*)malloc((*nVert) * 3 * sizeof(GLfloat));
	if (!(*vertices) || !(*normals)) {
		free(*vertices);
		free(*normals);
		// fgError("Failed to allocate memory in fghGenerateSphere");
	}

	/* top */
	(*vertices)[0] = 0.f;
	(*vertices)[1] = 0.f;
	(*vertices)[2] = radius;
	(*normals)[0]  = 0.f;
	(*normals)[1]  = 0.f;
	(*normals)[2]  = 1.f;
	idx			   = 3;

	/* each stack */
	for (i = 1; i < stacks; i++) {
		for (j = 0; j < slices; j++, idx += 3) {
			x = cost1[j] * sint2[i];
			y = sint1[j] * sint2[i];
			z = cost2[i];

			(*vertices)[idx]	 = x * radius;
			(*vertices)[idx + 1] = y * radius;
			(*vertices)[idx + 2] = z * radius;
			(*normals)[idx]		 = x;
			(*normals)[idx + 1]  = y;
			(*normals)[idx + 2]  = z;
		}
	}

	/* bottom */
	(*vertices)[idx]	 = 0.f;
	(*vertices)[idx + 1] = 0.f;
	(*vertices)[idx + 2] = -radius;
	(*normals)[idx]		 = 0.f;
	(*normals)[idx + 1]  = 0.f;
	(*normals)[idx + 2]  = -1.f;

	/* Done creating vertices, release sin and cos tables */
	free(sint1);
	free(cost1);
	free(sint2);
	free(cost2);
}

void fghSphere(GLfloat radius, GLint slices, GLint stacks,
			   GLboolean useWireMode) {
	int		 i, j, idx, nVert;
	GLfloat *vertices, *normals;

	/* Generate vertices and normals */
	fghGenerateSphere(radius, slices, stacks, &vertices, &normals, &nVert);

	if (nVert == 0)
		/* nothing to draw */
		return;

	if (useWireMode) {
		GLushort *sliceIdx, *stackIdx;
		/* First, generate vertex index arrays for drawing with glDrawElements
		* We have a bunch of line_loops to draw for each stack, and a
		* bunch for each slice.
		*/

		sliceIdx = (GLushort*)malloc(slices * (stacks + 1) * sizeof(GLushort));
		stackIdx = (GLushort*)malloc(slices * (stacks - 1) * sizeof(GLushort));
		if (!(stackIdx) || !(sliceIdx)) {
			free(stackIdx);
			free(sliceIdx);
			// fgError("Failed to allocate memory in fghSphere");
		}

		/* generate for each stack */
		for (i = 0, idx = 0; i < stacks - 1; i++) {
			GLushort offset = 1 + i * slices; /* start at 1 (0 is top vertex),
												 and we advance one stack down
												 as we go along */
			for (j = 0; j < slices; j++, idx++) {
				stackIdx[idx] = offset + j;
			}
		}

		/* generate for each slice */
		for (i = 0, idx = 0; i < slices; i++) {
			GLushort offset = 1 + i; /* start at 1 (0 is top vertex), and we
										advance one slice as we go along */
			sliceIdx[idx++] = 0;	 /* vertex on top */
			for (j = 0; j < stacks - 1; j++, idx++) {
				sliceIdx[idx] = offset + j * slices;
			}
			sliceIdx[idx++] =
				nVert - 1; /* zero based index, last element in array... */
		}

		/* draw */
		fghDrawGeometryWire(vertices, normals, nVert, sliceIdx, slices,
							stacks + 1, GL_LINE_STRIP, stackIdx, stacks - 1,
							slices);

		/* cleanup allocated memory */
		free(sliceIdx);
		free(stackIdx);
	} else {
		/* First, generate vertex index arrays for drawing with glDrawElements
		* All stacks, including top and bottom are covered with a triangle
		* strip.
		*/
		GLushort* stripIdx;
		/* Create index vector */
		GLushort offset;

		/* Allocate buffers for indices, bail out if memory allocation fails */
		stripIdx =
			(GLushort*)malloc((slices + 1) * 2 * (stacks) * sizeof(GLushort));
		if (!(stripIdx)) {
			free(stripIdx);
			// fgError("Failed to allocate memory in fghSphere");
		}

		/* top stack */
		for (j = 0, idx = 0; j < slices; j++, idx += 2) {
			stripIdx[idx] =
				j + 1; /* 0 is top vertex, 1 is first for first stack */
			stripIdx[idx + 1] = 0;
		}
		stripIdx[idx] = 1; /* repeat first slice's idx for closing off shape */
		stripIdx[idx + 1] = 0;
		idx += 2;

		/* middle stacks: */
		/* Strip indices are relative to first index belonging to strip, NOT
		 * relative to first vertex/normal pair in array */
		for (i = 0; i < stacks - 2; i++, idx += 2) {
			offset =
				1 +
				i * slices; /* triangle_strip indices start at 1 (0 is top
							   vertex), and we advance one stack down as we go
							   along */
			for (j = 0; j < slices; j++, idx += 2) {
				stripIdx[idx]	 = offset + j + slices;
				stripIdx[idx + 1] = offset + j;
			}
			stripIdx[idx] =
				offset +
				slices; /* repeat first slice's idx for closing off shape */
			stripIdx[idx + 1] = offset;
		}

		/* bottom stack */
		offset =
			1 + (stacks - 2) * slices; /* triangle_strip indices start at 1 (0
										  is top vertex), and we advance one
										  stack down as we go along */
		for (j = 0; j < slices; j++, idx += 2) {
			stripIdx[idx] = nVert - 1; /* zero based index, last element in
										  array (bottom vertex)... */
			stripIdx[idx + 1] = offset + j;
		}
		stripIdx[idx] =
			nVert - 1; /* repeat first slice's idx for closing off shape */
		stripIdx[idx + 1] = offset;

		/* draw */
		fghDrawGeometrySolid(vertices, normals, NULL, nVert, stripIdx, stacks,
							 (slices + 1) * 2);

		/* cleanup allocated memory */
		free(stripIdx);
	}

	/* cleanup allocated memory */
	free(vertices);
	free(normals);
}

/*
* Draws a solid sphere
*/
void glutSolidSphere(double radius, GLint slices, GLint stacks) {
	fghSphere((GLfloat)radius, slices, stacks, GL_FALSE);
}

/*
* Draws a wire sphere
*/
void glutWireSphere(double radius, GLint slices, GLint stacks) {
	fghSphere((GLfloat)radius, slices, stacks, GL_TRUE);
}

/**
* Generate all combinations of vertices and normals needed to draw object.
* Optional shape decomposition to triangles:
* We'll use glDrawElements to draw all shapes that are not naturally
* composed of triangles, so generate an index vector here, using the
* below sampling scheme.
* Be careful to keep winding of all triangles counter-clockwise,
* assuming that input has correct winding...
*/
static GLubyte vert4Decomp[6] = {
	0, 1, 2, 0, 2, 3}; /* quad    : 4 input vertices, 6 output (2 triangles) */
static GLubyte vert5Decomp[9] = {
	0, 1, 2, 0, 2,
	4, 4, 2, 3}; /* pentagon: 5 input vertices, 9 output (3 triangles) */

void fghGenerateGeometryWithIndexArray(int numFaces, int numEdgePerFace,
									   GLfloat* vertices, GLubyte* vertIndices,
									   GLfloat* normals, GLfloat* vertOut,
									   GLfloat* normOut, GLushort* vertIdxOut) {
	int		 i, j, numEdgeIdxPerFace;
	GLubyte* vertSamps = NULL;
	switch (numEdgePerFace) {
		case 3:
			/* nothing to do here, we'll draw with glDrawArrays */
			break;
		case 4:
			vertSamps		  = vert4Decomp;
			numEdgeIdxPerFace = 6; /* 6 output vertices for each face */
			break;
		case 5:
			vertSamps		  = vert5Decomp;
			numEdgeIdxPerFace = 9; /* 9 output vertices for each face */
			break;
	}
	/*
	* Build array with vertices using vertex coordinates and vertex indices
	* Do same for normals.
	* Need to do this because of different normals at shared vertices.
	*/
	for (i = 0; i < numFaces; i++) {
		int normIdx		   = i * 3;
		int faceIdxVertIdx = i * numEdgePerFace; /* index to first element of
													"row" in vertex indices */
		for (j = 0; j < numEdgePerFace; j++) {
			int outIdx  = i * numEdgePerFace * 3 + j * 3;
			int vertIdx = vertIndices[faceIdxVertIdx + j] * 3;

			vertOut[outIdx]		= vertices[vertIdx];
			vertOut[outIdx + 1] = vertices[vertIdx + 1];
			vertOut[outIdx + 2] = vertices[vertIdx + 2];

			normOut[outIdx]		= normals[normIdx];
			normOut[outIdx + 1] = normals[normIdx + 1];
			normOut[outIdx + 2] = normals[normIdx + 2];
		}

		/* generate vertex indices for each face */
		if (vertSamps)
			for (j = 0; j < numEdgeIdxPerFace; j++)
				vertIdxOut[i * numEdgeIdxPerFace + j] =
					faceIdxVertIdx + vertSamps[j];
	}
}

/* -- stuff that can be cached -- */
/* Cache of input to glDrawArrays or glDrawElements
* In general, we build arrays with all vertices or normals.
* We cant compress this and use glDrawElements as all combinations of
* vertices and normals are unique.
*/
#define DECLARE_SHAPE_CACHE(name, nameICaps, nameCaps)                         \
	static GLboolean name##Cached = GL_FALSE;                                  \
	static GLfloat   name##_verts[nameCaps##_VERT_ELEM_PER_OBJ];               \
	static GLfloat   name##_norms[nameCaps##_VERT_ELEM_PER_OBJ];               \
	static void		 fgh##nameICaps##Generate() {                              \
		fghGenerateGeometry(nameCaps##_NUM_FACES,                              \
							nameCaps##_NUM_EDGE_PER_FACE, name##_v, name##_vi, \
							name##_n, name##_verts, name##_norms);             \
	}
#define DECLARE_SHAPE_CACHE_DECOMPOSE_TO_TRIANGLE(name, nameICaps, nameCaps)   \
	static GLboolean name##Cached = GL_FALSE;                                  \
	static GLfloat   name##_verts[nameCaps##_VERT_ELEM_PER_OBJ];               \
	static GLfloat   name##_norms[nameCaps##_VERT_ELEM_PER_OBJ];               \
	static GLushort  name##_vertIdxs[nameCaps##_VERT_PER_OBJ_TRI];             \
	static void		 fgh##nameICaps##Generate() {                              \
		fghGenerateGeometryWithIndexArray(                                     \
			nameCaps##_NUM_FACES, nameCaps##_NUM_EDGE_PER_FACE, name##_v,      \
			name##_vi, name##_n, name##_verts, name##_norms, name##_vertIdxs); \
	}

/* -- Cube -- */
#define CUBE_NUM_VERT 8
#define CUBE_NUM_FACES 6
#define CUBE_NUM_EDGE_PER_FACE 4
#define CUBE_VERT_PER_OBJ (CUBE_NUM_FACES * CUBE_NUM_EDGE_PER_FACE)
#define CUBE_VERT_ELEM_PER_OBJ (CUBE_VERT_PER_OBJ * 3)
#define CUBE_VERT_PER_OBJ_TRI                                                  \
	(CUBE_VERT_PER_OBJ +                                                       \
	 CUBE_NUM_FACES *                                                          \
		 2) /* 2 extra edges per face when drawing quads as triangles */
/* Vertex Coordinates */
static GLfloat cube_v[CUBE_NUM_VERT * 3] = {
	.5f, .5f,  .5f,  -.5f, .5f, .5f,  -.5f, -.5f, .5f,  .5f,  -.5f, .5f,
	.5f, -.5f, -.5f, .5f,  .5f, -.5f, -.5f, .5f,  -.5f, -.5f, -.5f, -.5f};
/* Normal Vectors */
static GLfloat cube_n[CUBE_NUM_FACES * 3] = {
	0.0f,  0.0f, 1.0f, 1.0f, 0.0f,  0.0f, 0.0f, 1.0f, 0.0f,
	-1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, -1.0f};

/* Vertex indices, as quads, before triangulation */
static GLubyte cube_vi[CUBE_VERT_PER_OBJ] = {
	0, 1, 2, 3, 0, 3, 4, 5, 0, 5, 6, 1, 1, 6, 7, 2, 7, 4, 3, 2, 4, 7, 6, 5};
DECLARE_SHAPE_CACHE_DECOMPOSE_TO_TRIANGLE(cube, Cube, CUBE)

/* -- INTERNAL DRAWING functions --------------------------------------- */
#define _DECLARE_INTERNAL_DRAW_DO_DECLARE(name, nameICaps, nameCaps, vertIdxs) \
	static void fgh##nameICaps(GLboolean useWireMode) {                        \
		if (!name##Cached) {                                                   \
			fgh##nameICaps##Generate();                                        \
			name##Cached = GL_TRUE;                                            \
		}                                                                      \
                                                                               \
		if (useWireMode) {                                                     \
			fghDrawGeometryWire(                                               \
				name##_verts, name##_norms, nameCaps##_VERT_PER_OBJ, NULL,     \
				nameCaps##_NUM_FACES, nameCaps##_NUM_EDGE_PER_FACE,            \
				GL_LINE_LOOP, NULL, 0, 0);                                     \
		} else {                                                               \
			fghDrawGeometrySolid(name##_verts, name##_norms, NULL,             \
								 nameCaps##_VERT_PER_OBJ, vertIdxs, 1,         \
								 nameCaps##_VERT_PER_OBJ_TRI);                 \
		}                                                                      \
	}
#define DECLARE_INTERNAL_DRAW(name, nameICaps, nameCaps)                       \
	_DECLARE_INTERNAL_DRAW_DO_DECLARE(name, nameICaps, nameCaps, NULL)
#define DECLARE_INTERNAL_DRAW_DECOMPOSED_TO_TRIANGLE(name, nameICaps,          \
													 nameCaps)                 \
	_DECLARE_INTERNAL_DRAW_DO_DECLARE(name, nameICaps, nameCaps,               \
									  name##_vertIdxs)

void fghCube(GLfloat dSize, GLboolean useWireMode) {
	GLfloat* vertices;

	if (!cubeCached) {
		fghCubeGenerate();
		cubeCached = GL_TRUE;
	}

	if (dSize != 1.f) {
		/* Need to build new vertex list containing vertices for cube of
		 * different size */
		int i;

		vertices = (GLfloat*)malloc(CUBE_VERT_ELEM_PER_OBJ * sizeof(GLfloat));

		/* Bail out if memory allocation fails, fgError never returns */
		if (!vertices) {
			free(vertices);
			// fgError("Failed to allocate memory in fghCube");
		}

		for (i			= 0; i < CUBE_VERT_ELEM_PER_OBJ; i++)
			vertices[i] = dSize * cube_verts[i];
	} else
		vertices = cube_verts;

	if (useWireMode)
		fghDrawGeometryWire(vertices, cube_norms, CUBE_VERT_PER_OBJ, NULL,
							CUBE_NUM_FACES, CUBE_NUM_EDGE_PER_FACE,
							GL_LINE_LOOP, NULL, 0, 0);
	else
		fghDrawGeometrySolid(vertices, cube_norms, NULL, CUBE_VERT_PER_OBJ,
							 cube_vertIdxs, 1, CUBE_VERT_PER_OBJ_TRI);

	if (dSize != 1.f)
		/* cleanup allocated memory */
		free(vertices);
}

void glutWireCube(double dSize) { fghCube((GLfloat)dSize, GL_TRUE); }
void glutSolidCube(double dSize) { fghCube((GLfloat)dSize, GL_FALSE); }
}

/*** END OF FILE ***/
