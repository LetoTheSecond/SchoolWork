#ifndef LOGGER_H
#define LOGGER_H
#if defined _WIN32 && !defined _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <cstdarg>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
namespace Logger {
std::string GetTime();
std::string GetDate();
std::string GetLogTimestamp();
#ifndef LOG_FILENAME
#define LOG_FILENAME "log.txt"
#endif

bool LogInit();
bool LogRestart();
bool Log(const char*, ...);
bool LogError(const char*, ...);
};
#endif
