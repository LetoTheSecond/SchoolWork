#include "Logger.h"

namespace Logger {
std::string GetTime() {
	time_t			  now = time(NULL);
	std::stringstream timestamp;
	timestamp << std::put_time(std::gmtime(&now), "%T");
	return timestamp.str();
}
std::string GetDate() {
	time_t			  now = time(NULL);
	std::stringstream timestamp;
	timestamp << std::put_time(std::gmtime(&now), "%Y-%m-%d");
	return timestamp.str();
}
std::string GetLogTimestamp() {
	time_t			  now = time(NULL);
	std::stringstream timestamp;
	timestamp << std::put_time(std::localtime(&now), "%Y-%m-%d ")
			  << std::put_time(std::localtime(&now), "%T");
	return timestamp.str();
}

//=========================================================================
//									STDOUT ONLY
//=========================================================================
#ifdef LOG_STDOUT_ONLY
bool LogInit() {
	std::string timestamp = GetLogTimestamp();
	std::cout << "[" << timestamp << "] Log > "
			  << "Log Init\n";
	return true;
}
bool LogRestart() { return true; }
bool Log(const char* inMsg, ...) {
	va_list		args;
	std::string timestamp = GetLogTimestamp(), outStr;
	va_start(args, inMsg);
	size_t size = vsnprintf(nullptr, 0, inMsg, args) + 1;
	va_end(args);
	char* buff = new char[size];
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	std::cout << "[" << timestamp << "] Log > " << buff;
	va_end(args);
	delete[] buff;
	return true;
}
bool LogError(const char* inMsg, ...) {
	va_list		args;
	std::string timestamp = GetLogTimestamp(), outStr;
	va_start(args, inMsg);
	size_t size = vsnprintf(nullptr, 0, inMsg, args) + 1;
	va_end(args);
	char* buff = new char[size];
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	std::cerr << "[" << timestamp << "] ERR > " << buff;
	va_end(args);
	delete[] buff;
	return true;
}
//=========================================================================
//									FILEOUT ONLY
//=========================================================================
#elif LOG_FILE_ONLY
bool LogInit() {
	std::string   timestamp = GetLogTimestamp();
	std::ofstream log;
#ifdef OVERWRITE_LOGFILE
	log.open(LOG_FILENAME, std::fstream::trunc);
#else
	log.open(LOG_FILENAME, std::fstream::app);
#endif
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	log << "[" << timestamp << "] Log > "
		<< "Log Init\n";
	log.close();
	return true;
}
bool LogRestart() {
	std::ofstream log;
	log.open(LOG_FILENAME, std::fstream::app);
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	log.close();
	return LogInit();
}
bool Log(const char* inMsg, ...) {
	va_list		args;
	std::string timestamp = GetLogTimestamp(), outStr;
	va_start(args, inMsg);
	size_t size = vsnprintf(nullptr, 0, inMsg, args) + 1;
	va_end(args);
	char*		  buff = new char[size];
	std::ofstream log;
	log.open(LOG_FILENAME, std::fstream::app);
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	log << "[" << timestamp << "] Log > " << buff;
	log.close();
	va_end(args);
	delete[] buff;
	return true;
}
bool LogError(const char* inMsg, ...) {
	va_list		args;
	std::string timestamp = GetLogTimestamp(), outStr;
	va_start(args, inMsg);
	size_t size = vsnprintf(nullptr, 0, inMsg, args) + 1;
	va_end(args);
	char*		  buff = new char[size];
	std::ofstream log;
	log.open(LOG_FILENAME, std::fstream::app);
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	log << "[" << timestamp << "] ERR > " << buff;
	log.close();
	va_end(args);
	delete[] buff;
	return true;
}
#else
//=========================================================================
//							STDOUT && FILEOUT ( DEFAULT )
//=========================================================================
bool LogInit() {
	std::string   timestamp = GetLogTimestamp();
	std::ofstream log;
#ifdef OVERWRITE_LOGFILE
	log.open(LOG_FILENAME, std::fstream::trunc);
#else
	log.open(LOG_FILENAME, std::fstream::app);
#endif
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	log << "[" << timestamp << "] Log > "
		<< "Log Init\n";
	log.close();
	std::cout << "[" << timestamp << "] Log > "
			  << "Log Init\n";
	return true;
}
bool LogRestart() {
	std::ofstream log;
	log.open(LOG_FILENAME, std::fstream::app);
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	log.close();
	return LogInit();
}
bool Log(const char* inMsg, ...) {
	va_list		args;
	std::string timestamp = GetLogTimestamp(), outStr;
	va_start(args, inMsg);
	size_t size = vsnprintf(nullptr, 0, inMsg, args) + 1;
	va_end(args);
	char*		  buff = new char[size];
	std::ofstream log;
	log.open(LOG_FILENAME, std::fstream::app);
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	log << "[" << timestamp << "] Log > " << buff;
	log.close();
	va_end(args);
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	std::cout << "[" << timestamp << "] Log > " << buff;
	va_end(args);
	delete[] buff;
	return true;
}
bool LogError(const char* inMsg, ...) {
	va_list		args;
	std::string timestamp = GetLogTimestamp(), outStr;
	va_start(args, inMsg);
	size_t size = vsnprintf(nullptr, 0, inMsg, args) + 1;
	va_end(args);
	char*		  buff = new char[size];
	std::ofstream log;
	log.open(LOG_FILENAME, std::fstream::app);
	if (!log.is_open()) {
		std::cerr << "Could not open log file : " << LOG_FILENAME
				  << " : for writing\n";
		return false;
	}
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	log << "[" << timestamp << "] ERR > " << buff;
	log.close();
	va_end(args);
	va_start(args, inMsg);
	vsnprintf(buff, size, inMsg, args);
	buff[size - 1] = '\0';
	std::cerr << "[" << timestamp << "] ERR > " << buff;
	va_end(args);
	delete[] buff;
	return true;
}
};
#endif