#include "Mesh.h"
#include "Logger.h"
using namespace Logger;

bool Mesh::readFile(const char* fileName) {
	std::fstream infile;
	infile.open(fileName, std::ios::in);
	if (infile.fail()) {
		LogError("infile.fail()\n");
		return false;
	}
	if (infile.eof()) {
		LogError("infile.eof()\n");
		return false;
	}
	infile >> numVerts >> numNorms >> numFaces;
	pt   = new Point3[numVerts];
	norm = new Vector3[numNorms];
	face = new Face[numFaces];
	// check that enough memory was found:
	if (!pt || !norm || !face) {
		LogError("!pt||!norm||!face\n");
		return false;
	}
	// read the vertices
	for (int p = 0; p < numVerts; ++p)
		infile >> pt[p].x >> pt[p].y >> pt[p].z;
	// read the normals
	for (int n = 0; n < numNorms; ++n)
		infile >> norm[n].x >> norm[n].y >> norm[n].z;
	// read the faces
	for (int f = 0; f < numFaces; ++f) {
		infile >> face[f].nVerts;
		face[f].vert = new VertexID[face[f].nVerts];
		// vertex list
		for (int i = 0; i < face[f].nVerts; ++i) {
			infile >> face[f].vert[i].vertIndex;
		}
		// normal list
		for (int i = 0; i < face[f].nVerts; ++i) {
			infile >> face[f].vert[i].normIndex;
		}
	}
	return true;
}

void Mesh::Draw(bool wire) {
	// draw each face
	for (int f = 0; f < numFaces; ++f) {
		if (!wire) {
			glBegin(GL_POLYGON);
		} else {
			glBegin(GL_LINE_LOOP);
		}
		// for each one..
		for (int v = 0; v < face[f].nVerts; ++v) {
			// index of this normal
			int in = face[f].vert[v].normIndex;
			// index of this vertex
			int iv = face[f].vert[v].vertIndex;
			glNormal3f(norm[in].x, norm[in].y, norm[in].z);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}
