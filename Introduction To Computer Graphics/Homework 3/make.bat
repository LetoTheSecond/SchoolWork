@echo off

if /i [%1]==[clean]   (goto :clean)
if /i [%1]==[release] (goto :Release)
if /i [%1]==[run]	  (goto :run)
if /i [%1]==[brun]    (goto :brun)
					   goto :Default

:clean
rmdir /s /Q bin
goto :eof

:Release
msbuild /m /p:Configuration=Release
goto :eof

:run
bin\Homework3\Debug\x86\Homework3.exe
goto :eof

:brun
msbuild /m /p:Configuration=Debug;PlatformTarget=x86
bin\Homework3\Debug\x86\Homework3.exe
goto eof

:Default
msbuild /m /p:Configuration=Debug;PlatformTarget=x86
goto :eof

:eof
::
