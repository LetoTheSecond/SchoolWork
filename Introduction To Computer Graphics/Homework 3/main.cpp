// Ahsan Khan
// 12/01/2016
// Homework 3
#include "Window.h"
#include "Camera.h"
#include "Maze.h"
#include "Logger.h"

#include <cstdio>
#include <vector>
#include <functional>
#include <algorithm>
#include <chrono>
#include <thread>
#include <cmath>

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")

using namespace Logger;

void LogOpenGLParams(){
	int i;
	int v[2];
	unsigned char s = 0;
	GLenum params[] = {
		GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
		GL_MAX_CUBE_MAP_TEXTURE_SIZE,
		GL_MAX_DRAW_BUFFERS,
		GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,
		GL_MAX_TEXTURE_IMAGE_UNITS,
		GL_MAX_TEXTURE_SIZE,
		GL_MAX_VARYING_FLOATS,
		GL_MAX_VERTEX_ATTRIBS,
		GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,
		GL_MAX_VERTEX_UNIFORM_COMPONENTS,
		GL_MAX_VIEWPORT_DIMS,
		GL_STEREO,
	};
	const char* names[] = {
		"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS",
		"GL_MAX_CUBE_MAP_TEXTURE_SIZE",
		"GL_MAX_DRAW_BUFFERS",
		"GL_MAX_FRAGMENT_UNIFORM_COMPONENTS",
		"GL_MAX_TEXTURE_IMAGE_UNITS",
		"GL_MAX_TEXTURE_SIZE",
		"GL_MAX_VARYING_FLOATS",
		"GL_MAX_VERTEX_ATTRIBS",
		"GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS",
		"GL_MAX_VERTEX_UNIFORM_COMPONENTS",
		"GL_MAX_VIEWPORT_DIMS",
		"GL_STEREO",
	};
	// Get version info
	// Get renderer string
	const GLubyte* renderer = glGetString(GL_RENDERER);
	// Version as a string
	const GLubyte* version = glGetString(GL_VERSION);
	Logger::Log("Renderer: %s\n", renderer);
	Logger::Log("OpenGL version supported %s\n", version);
	Log("-----------------------------\n");
	Logger::Log("GL Context Params:\n");
	// Integers - only works if the order is 0-10 integer return types
	for (i = 0; i < 10; i++) {
		int v = 0;
		glGetIntegerv (params[i], &v);
		Logger::Log("%-35s %6i\n", names[i], v);
	}
	// Others
	v[0] = v[1] = 0;
	glGetIntegerv (params[10], v);
	Logger::Log("%-35s %6i %6i\n", names[10], v[0], v[1]);
	glGetBooleanv (params[11], &s);
	Logger::Log("%-35s %6i\n", names[11], (unsigned int)s);
	Log("-----------------------------\n");
}

// Windows callback function to handel events
static bool GKeys[256] = {false},GResized;
static float GWinH, GWinW;
LRESULT CALLBACK EventHandler(HWND w, UINT m, WPARAM wp, LPARAM lp) {
	switch (m) {
		// Window close event
		case WM_CLOSE: {
			PostQuitMessage(0);
			return 0;
		}
		// Window resize event
		case WM_SIZE: {
			GWinW = (float)LOWORD(lp);
			GWinH = (float)HIWORD(lp);
			glViewport(0, 0, (GLsizei)GWinW, (GLsizei)GWinH);
			Log("Resized: %dx%d\n", GWinW, GWinH);
			GResized=true;
			return 0;
		}
		// Key was pressed
		case WM_KEYDOWN: {
			if (wp == VK_ESCAPE) {
				PostQuitMessage(0);
				return 0;
			}
			if (!GKeys[VK_SHIFT]) {
				// Lower Case
				switch (wp) {
					case VK_ADD:
					case VK_SUBTRACT:
						// Need to do this to capture single key press
						// 30th bit tells if key was held down last event
						// cycle or if this event is a keypress for the first
						// time.
						GKeys[wp] = !(lp & (1 << 30));
						return 0;
					default:
						// Records key being held down
						GKeys[wp] = true;
						return 0;
				}
			} else {
				// Upper Case
				// switch (wp) {
					// case '':
						// Need to do this to capture single key press
						// GKeys[wp] = !(lp & (1 << 30));
						// return 0;
					// default:
						// Records key being held down
						GKeys[wp] = true;
						return 0;
				// }
			}
		}
		// Key was released
		case WM_KEYUP: {
			GKeys[wp] = false;
			return 0;
		}
	}
	// For events not handeled manually, let windows handel them
	return DefWindowProc(w, m, wp, lp);
}

struct Color {float r, g, b, a;};

float clip(float n, float lower, float upper) {
	#ifdef max
	#undef max
	#endif
	#ifdef min
	#undef min
	#endif
	return std::max(lower, std::min(n, upper));
};

int main(int argc, char** argv) {
	LogInit();
	Window window;
	Camera camera;
	Color 
		blue {(1.0f / 255) * 211, (1.0f / 255) * 214, (1.0f / 255) * 237, 1.0},
		green{(1.0f / 255) * 112, (1.0f / 255) * 149, (1.0f / 255) * 024, 1.0};
	Maze maze;
	maze.InitMaze(7,7);
	maze.SetCellSize(2.0f);

	// Window , Create win32 window
	//=================================
	GWinW=800;
	GWinH=600;
	// if (!window.MakeWindow(GetSystemMetrics(SM_CXSCREEN)/2-(int)GWinW/2,
	// 	GetSystemMetrics(SM_CYSCREEN)/2-(int)GWinH/2,
	// 	(int)GWinW,(int)GWinH,2,1,EventHandler)){
	// 	LogError("Could not Make Window\n");
	// 	return -1;
	// }
	if (!window.MakeWindow(0, 0, (int)GWinW, (int)GWinH, 2, 1, EventHandler)) {
		LogError("Could not Make Window\n");
		return -1;
	}
	window.DisplayWindow();
	LogOpenGLParams();

	// OpengGL Settings and Textures
	//=================================
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_MULTISAMPLE);
	// glClearColor(blue.r,blue.g,blue.b,blue.a);

	RGBpixmap ground,sky;
	ground.readBMPFile("grass128.bmp");
	ground.setTexture(3);
	maze.SetWallTex("brick128.bmp",1);
	sky.readBMPFile("sky128.bmp");
	sky.setTexture(2);

	// Camera Position
	//=================================
	// camera.SetPos(3.0f * (maze.GetCellSize() * 2.0f), 0.0f,
	// 			  3.0f * (maze.GetCellSize() * 2.0f));
	camera.SetPos(2.0f,0.0f,2.0f);
	maze.PlayerStart(camera.pos.x,camera.pos.z);
	// camera.SetPos( 0.0f, 0.0f, 0.0f);
	camera.SetUp ( 0.0f, 1.0f, 0.0f);
	camera.speed = 1.0f;
	camera.fov   = 45.0f;
	camera.yaw 	 = 0.0f;
	// camera.yaw   = 90.0f;
	camera.SetProjection(camera.fov,GWinW/GWinH,0.001f,1000.0f);

	// Setup the Maze
	//=================================
	// Initializing each cell of the maze
	// True  = opening
	// False = wall (Default Value)
	// 		ROW 1
	//=================
	// Cell 1
	maze[0][0].faces[EAST]  = true;
	// Cell 2
	maze[0][1].faces[NORTH] = true; 
	maze[0][1].faces[WEST]  = true;
	// Cell 3
	maze[0][2].faces[NORTH] = true; 
	maze[0][2].faces[EAST]  = true;
	// Cell 4
	maze[0][3].faces[SOUTH] = true; 
	maze[0][3].faces[WEST]  = true;
	// Cell 5
	maze[0][4].faces[NORTH] = true; 
	maze[0][4].faces[EAST]  = true;
	// Cell 6
	maze[0][5].faces[NORTH] = true; 
	maze[0][5].faces[WEST]  = true;
	// Cell 7
	maze[0][6].faces[NORTH] = true; 

	// 		ROW 2
	//=================
	// Cell 1
	maze[1][0].faces[NORTH] = true; 
	maze[1][0].faces[EAST]  = true;
	// Cell 2
	maze[1][1].faces[SOUTH] = true; 
	maze[1][1].faces[WEST]  = true;
	// Cell 3
	maze[1][2].faces[NORTH] = true; 
	maze[1][2].faces[SOUTH] = true; 
	// Cell 4
	maze[1][3].faces[NORTH] = true; 
	maze[1][3].faces[EAST]  = true;
	// Cell 5
	maze[1][4].faces[SOUTH] = true; 
	maze[1][4].faces[WEST]  = true;
	// Cell 6
	maze[1][5].faces[EAST]  = true;
	maze[1][5].faces[SOUTH] = true; 
	// Cell 7
	maze[1][6].faces[NORTH] = true; 
	maze[1][6].faces[SOUTH] = true; 
	maze[1][6].faces[WEST]  = true;

	// 		ROW 3
	//=================
	// Cell 1
	maze[2][0].faces[NORTH] = true; 
	maze[2][0].faces[SOUTH] = true; 
	// Cell 2
	maze[2][1].faces[NORTH] = true; 
	maze[2][1].faces[EAST]  = true;
	// Cell 3
	maze[2][2].faces[EAST]  = true;
	maze[2][2].faces[SOUTH] = true; 
	maze[2][2].faces[WEST]  = true;
	// Cell 4
	maze[2][3].faces[SOUTH] = true; 
	// Cell 5
	maze[2][4].faces[NORTH] = true; 
	maze[2][4].faces[EAST]  = true;
	// Cell 6
	maze[2][5].faces[EAST]  = true;
	maze[2][5].faces[WEST]  = true;
	// Cell 7
	maze[2][6].faces[SOUTH] = true; 
	maze[2][6].faces[WEST]  = true;

	// 		ROW 4
	//=================
	// Cell 1
	maze[3][0].faces[NORTH] = true; 
	maze[3][0].faces[EAST]  = true;
	maze[3][0].faces[SOUTH] = true; 
	// Cell 2
	maze[3][1].faces[EAST]  = true;
	maze[3][1].faces[SOUTH] = true; 
	maze[3][1].faces[WEST]  = true;
	// Cell 3
	maze[3][2].faces[EAST]  = true;
	maze[3][2].faces[WEST]  = true;
	// Cell 4
	maze[3][3].faces[EAST]  = true;
	maze[3][3].faces[WEST]  = true;
	// Cell 5
	maze[3][4].faces[EAST]  = true;
	maze[3][4].faces[SOUTH] = true; 
	maze[3][4].faces[WEST]  = true;
	// Cell 6
	maze[3][5].faces[EAST]  = true;
	maze[3][5].faces[WEST]  = true;
	// Cell 7
	maze[3][6].faces[NORTH] = true; 
	maze[3][6].faces[WEST]  = true;

	// 		ROW 5
	//=================
	// Cell 1
	maze[4][0].faces[NORTH] = true; 
	maze[4][0].faces[SOUTH] = true; 
	// Cell 2
	maze[4][1].faces[EAST]  = true;
	// Cell 3
	maze[4][2].faces[NORTH] = true; 
	// Cell 4
	maze[4][3].faces[NORTH] = true; 
	maze[4][3].faces[EAST]  = true;
	// Cell 5
	maze[4][4].faces[NORTH] = true; 
	maze[4][4].faces[EAST]  = true;
	maze[4][4].faces[WEST]  = true;
	// Cell 6
	maze[4][5].faces[NORTH] = true; 
	maze[4][5].faces[WEST]  = true;
	// Cell 7
	maze[4][6].faces[NORTH] = true; 
	maze[4][6].faces[SOUTH] = true;

	// 		ROW 6
	//=================
	// Cell 1
	maze[5][0].faces[EAST]  = true;
	maze[5][0].faces[SOUTH] = true; 
	// Cell 2
	maze[5][1].faces[EAST]  = true;
	maze[5][1].faces[WEST]  = true;
	// Cell 3
	maze[5][2].faces[NORTH] = true; 
	maze[5][2].faces[EAST]  = true;
	maze[5][2].faces[SOUTH] = true; 
	maze[5][2].faces[WEST]  = true;
	// Cell 4
	maze[5][3].faces[SOUTH] = true; 
	maze[5][3].faces[WEST]  = true;
	// Cell 5
	maze[5][4].faces[NORTH] = true; 
	maze[5][4].faces[SOUTH] = true; 
	// Cell 6
	maze[5][5].faces[NORTH] = true; 
	maze[5][5].faces[SOUTH] = true; 
	// Cell 7
	maze[5][6].faces[NORTH] = true; 
	maze[5][6].faces[SOUTH] = true; 

	// 		ROW 7
	//=================
	// Cell 1
	maze[6][0].faces[EAST]  = true;
	// Cell 2
	maze[6][1].faces[EAST]  = true;
	maze[6][1].faces[WEST]  = true;
	// Cell 3
	maze[6][2].faces[SOUTH] = true; 
	maze[6][2].faces[WEST]  = true;
	// Cell 4
	maze[6][3].faces[NORTH] = true; 
	maze[6][3].faces[EAST]  = true;
	// Cell 5
	maze[6][4].faces[SOUTH] = true; 
	maze[6][4].faces[WEST]  = true;
	// Cell 6
	maze[6][5].faces[EAST]  = true;
	maze[6][5].faces[SOUTH] = true; 
	// Cell 7
	maze[6][6].faces[SOUTH] = true; 
	maze[6][6].faces[WEST]  = true;
	camera.speed 	= 5.0f;
	auto 	start 	= std::chrono::high_resolution_clock::now();
	auto 	end 	= std::chrono::high_resolution_clock::now();
	float 	delta 	=
		(float)std::chrono::duration_cast<std::chrono::milliseconds>(end-start)
			.count();
	// Main Loop
	//=================================
	while (true) {
		start = std::chrono::high_resolution_clock::now();
		delta = ((-1) *
				(int)std::chrono::duration_cast<std::chrono::milliseconds>
				(end - start).count())/1000.0f;
		if (!window.ProccessEvents())
			break;
		glm::vec3 _pos = camera.pos;
		// Handle Input then update camera
		//===================================
		if (!GKeys[VK_SHIFT]) { 
		// Lower Case
			if (GKeys[VK_UP]) {
				// std::printf("\t\t\t\tINPUT: %-10s\r","UP");
				camera.pos+=camera.front*camera.speed*delta;
			}
			if (GKeys[VK_RIGHT]) {
				// std::printf("\t\t\t\tINPUT: %-10s\r","Right");
				camera.yaw+=125.0f*delta;
			}
			if (GKeys[VK_DOWN]) {
				// std::printf("\t\t\t\tINPUT: %-10s\r","Down");
				camera.pos-=camera.front*camera.speed*delta;
			}
			if (GKeys[VK_LEFT]) {
				// std::printf("\t\t\t\tINPUT: %-10s\r","Left");
				camera.yaw-=125.0f*delta;
			}
			if (GKeys[VK_ADD]){
				maze.SetCellSize((maze.GetCellSize()+0.1f));
				Log("Cell Size: %f\n",maze.GetCellSize());
				GKeys[VK_ADD]=false;
			}
			if (GKeys[VK_SUBTRACT]){
				maze.SetCellSize((maze.GetCellSize()-0.1f));
				Log("Cell Size: %f\n",maze.GetCellSize());
				GKeys[VK_SUBTRACT]=false;
			}
		}
		// else {
		// // Upper Case
		// }
		maze.MovePlayer(_pos,camera.pos);
		camera.Update();
		if (GResized){
			camera.SetProjection(camera.fov,GWinW/GWinH,0.01f,1000.0f);
			GResized=false;
		}
		// Rendering
		//===================================
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
		// Projection
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMultMatrixf(glm::value_ptr(camera.proj));
		// View
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glMultMatrixf(glm::value_ptr(camera.view));
		// Draw Floor
		glPushMatrix();
			glTranslatef((3.0f*(maze.GetCellSize()*2.0f)+maze.GetCellSize()),
						 0.0f,
						 (3.0f*(maze.GetCellSize()*2.0f))+maze.GetCellSize());
			glScalef(1.0f*maze.GetCellSize()*7,
					 1.0f,
					 1.0f*maze.GetCellSize()*7);
			ground.setTexture(3);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
			glBegin(GL_QUADS);
				glTexCoord2f(-128.0f/11.0f, 128.0f/11.0f);
				glVertex3f( -1.00f, -1.00f,  1.00f);
				glTexCoord2f( 128.0f/11.0f, 128.0f/11.0f);
				glVertex3f(  1.00f, -1.00f,  1.00f);
				glTexCoord2f( 128.0f/11.0f,-128.0f/11.0f);
				glVertex3f(  1.00f, -1.00f, -1.00f);
				glTexCoord2f(-128.0f/11.0f,-128.0f/11.0f);
				glVertex3f( -1.00f, -1.00f, -1.00f);
			glEnd();
		glPopMatrix();
		// Draw Sky
		glPushMatrix();
			glTranslatef((3.0f*(maze.GetCellSize()*2.0f)+maze.GetCellSize()),
						 0.0f,
						 (3.0f*(maze.GetCellSize()*2.0f))+maze.GetCellSize());
			glScalef(1.0f*maze.GetCellSize()*7,
					 1.0f,
					 1.0f*maze.GetCellSize()*7);
			sky.setTexture(2);
			// glBindTexture(GL_TEXTURE_2D, 0);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
			glBegin(GL_QUADS);
				glTexCoord2f(-128.0f/24.0f, 128.0f/24.0f);
				glVertex3f( -1.00f,  1.00f,  1.00f);
				glTexCoord2f( 128.0f/24.0f, 128.0f/24.0f);
				glVertex3f(  1.00f,  1.00f,  1.00f);
				glTexCoord2f( 128.0f/24.0f,-128.0f/24.0f);
				glVertex3f(  1.00f,  1.00f, -1.00f);
				glTexCoord2f(-128.0f/24.0f,-128.0f/24.0f);
				glVertex3f( -1.00f,  1.00f, -1.00f);
			glEnd();
		glPopMatrix();
		// glColor3f(1.0f,1.0f,1.0f);
		// Draw Maze
		maze.Draw();
		glFlush();
		end = std::chrono::high_resolution_clock::now();
		window.SwapBuffers();
	}
	return 0;
}
