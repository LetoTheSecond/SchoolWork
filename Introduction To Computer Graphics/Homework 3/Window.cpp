// Ahsan Khan
// 12/01/2016
// Homework 3
#include "Window.h"
#include "Logger.h"
using namespace Logger;

bool Window::classRegistered = false;
bool Window::glewInitialized = false;

Window::Window() {
	this->hWin		= nullptr;
	this->wDC		= nullptr;
	this->hRC		= nullptr;
	this->hInstance = nullptr;
}

Window::~Window() {
	if (this->hRC == wglGetCurrentContext())
		wglMakeCurrent(NULL, NULL);
	ReleaseDC(this->hWin, this->wDC);
	wglDeleteContext(this->hRC);
	DestroyWindow(this->hWin);
}

bool Window::MakeWindow(int x, int y, int w, int h,int verMaj,int verMin,
						LRESULT(CALLBACK* fun)(HWND, UINT, WPARAM, LPARAM)) {

	this->hInstance = GetModuleHandle(0);
	if (!Window::classRegistered) {
		WNDCLASSEX winClass	   = {0};
		winClass.cbSize		   = sizeof(WNDCLASSEX);
		winClass.style		   = CS_OWNDC;
		winClass.lpfnWndProc   = (WNDPROC)fun;
		winClass.hInstance	   = this->hInstance;
		winClass.hIcon		   = LoadIcon(NULL, IDI_APPLICATION);
		winClass.hCursor	   = LoadCursor(NULL, IDC_ARROW);
		winClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		winClass.lpszMenuName  = NULL;
		winClass.lpszClassName = "Win32Class";
		winClass.hIconSm	   = LoadIcon(NULL, IDI_APPLICATION);
		if (!RegisterClassEx(&winClass)) {
			MessageBox(nullptr,"Could not register window class","Error",0);
			LogError("Failed to register window class\n");
			return false;
		}
		Window::classRegistered = true;
	}

	// Init glew
	{
		HWND tempWin=CreateWindowExA(WS_EX_CLIENTEDGE, "Win32Class", "",
									  WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW,
									  x, y, w, h, nullptr, nullptr, this->hInstance, nullptr);
		HDC tempDC=GetDC(tempWin);
		PIXELFORMATDESCRIPTOR tempPFD;
		memset(&tempPFD, 0, sizeof(PIXELFORMATDESCRIPTOR));
		tempPFD.nSize		= sizeof(PIXELFORMATDESCRIPTOR);
		tempPFD.nVersion    = 1;
		tempPFD.dwFlags     = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
		tempPFD.iPixelType  = PFD_TYPE_RGBA;
		tempPFD.cColorBits  = 32;
		tempPFD.cDepthBits  = 32;
		tempPFD.iLayerType  = PFD_MAIN_PLANE;
		int tempPF			= ChoosePixelFormat(tempDC, &tempPFD);
		if (!tempPF) {
			LogError("Cannot find a suitable pixel format\n");
			MessageBox(NULL, "ChoosePixelFormat() failed"
					   "Cannot find a suitable pixel format.",
					   "Error", MB_OK);
			return false;
		}
		if (!SetPixelFormat(tempDC, tempPF, &tempPFD)) {
			LogError("Cannot set pixel format specified\n");
			MessageBox(NULL, "SetPixelFormat() failed"
					   "Cannot set pixel format specified.",
					   "Error", MB_OK);
			return false;
		}
		HGLRC tempRC=wglCreateContext(tempDC);
		wglMakeCurrent(tempDC,tempRC);
		glewExperimental = true;
		auto err = glewInit();
		if (err != GLEW_OK) {
			LogError("Could not initialize GLEW\n");
			MessageBox(NULL, "glewInit() failed"
					   "Cannot initalize GLEW.",
					   "Error", MB_OK);
			return false;
		}
		wglMakeCurrent(nullptr,nullptr);
		wglDeleteContext(tempRC);
		DestroyWindow(tempWin);
	}

	// Create Window
	this->hWin =
		CreateWindowExA(WS_EX_CLIENTEDGE, "Win32Class", "",
						WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW,
						x, y, w, h, nullptr, nullptr, this->hInstance, nullptr);
	if (!this->hWin) {
		LogError("Failed to create window\n");
		MessageBox(nullptr, "Could not create window", "Error", 0);
		return false;
	}
	this->wDC = GetDC(this->hWin);
	// Create Context
	{
		PIXELFORMATDESCRIPTOR pfd={0};
		if (verMaj<=2){
			pfd.nSize		= sizeof(PIXELFORMATDESCRIPTOR);
			pfd.nVersion   = 1;
			pfd.dwFlags    = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
			pfd.iPixelType = PFD_TYPE_RGBA;
			pfd.cColorBits = 32;
			pfd.cDepthBits = 32;
			pfd.iLayerType = PFD_MAIN_PLANE;
			int pf=ChoosePixelFormat(wDC,&pfd);
			if (!pf) {
				LogError("Cannot find a suitable pixel format\n");
				MessageBox(NULL, "ChoosePixelFormat() failed"
						   "Cannot find a suitable pixel format.",
						   "Error", MB_OK);
				return false;
			}
			if (!SetPixelFormat(wDC, pf, &pfd)) {
				LogError("Cannot set pixel format specified\n");
				MessageBox(NULL, "SetPixelFormat() failed"
						   "Cannot set pixel format specified.",
						   "Error", MB_OK);
				return false;
			}
			this->hRC=wglCreateContext(wDC);
			if (this->hRC){
				wglMakeCurrent(wDC,this->hRC);
			} else{
				LogError("Could not create context\n");
				MessageBox(NULL, "wglCreateContext() failed"
						   "Could not create context.",
						   "Error", MB_OK);
				return false;
			}
		} else if (WGLEW_ARB_create_context && WGLEW_ARB_pixel_format){
			int pfAttribList[]={
				WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
				WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
				WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
				WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
				WGL_COLOR_BITS_ARB, 32,
				WGL_DEPTH_BITS_ARB, 24,
				WGL_STENCIL_BITS_ARB, 8,
				0
			};
			int contextAttribs[]={
				WGL_CONTEXT_MAJOR_VERSION_ARB, verMaj,
				WGL_CONTEXT_MINOR_VERSION_ARB, verMin,
				WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
				0			
			};
			int pf,nf;
			wglChoosePixelFormatARB(wDC,pfAttribList,nullptr,1,&pf,(UINT*)&nf);
			if (!pf) {
				LogError("Cannot find a suitable pixel format\n");
				MessageBox(NULL, "ChoosePixelFormat() failed"
						   "Cannot find a suitable pixel format.",
						   "Error", MB_OK);
				return false;
			}
			if (!SetPixelFormat(wDC, pf, &pfd)) {
				LogError("Cannot set pixel format specified\n");
				MessageBox(NULL, "SetPixelFormat() failed"
						   "Cannot set pixel format specified.",
						   "Error", MB_OK);
				return false;
			}
			this->hRC=wglCreateContextAttribsARB(wDC,0,contextAttribs);
			if (this->hRC){
				wglMakeCurrent(wDC,this->hRC);
			} else{
				LogError("Could not create context\n");
				MessageBox(NULL, "wglCreateContext() failed"
						   "Could not create context.",
						   "Error", MB_OK);
				return false;
			}
		}
	}
	return true;
}

bool Window::ProccessEvents() {
	if (PeekMessage(&this->msg, nullptr, 0, 0, PM_REMOVE)) {
		if (this->msg.message == WM_QUIT) {
			return false;
		} else {
			TranslateMessage(&this->msg);
			DispatchMessage(&this->msg);
		}
	}
	return true;
}

void Window::SwapBuffers() { ::SwapBuffers(this->wDC); }

void Window::DisplayWindow() {
	ShowWindow(this->hWin, SW_SHOWNORMAL);
	UpdateWindow(this->hWin);
}
