// Ahsan Khan
// 12/01/2016
// Homework 3
#ifndef MAZE_H
#define MAZE_H

#include "glm/glm.hpp"
#include "RGBpixmap.h"

enum Direction { NORTH, EAST, SOUTH, WEST };

struct Cell {
	bool faces[4] = {false};
};

class Maze {
	Cell* cells;
	int xSize, ySize;
	float cellSize, posOffset, cellRadius, wallThickness;
	int texID,playerXCell,playerYCell;
	RGBpixmap wallTexture;
	void DrawCell(int, int, Cell&);
public:
	Maze();
	~Maze();
	void  InitMaze(int, int);
	void  PlayerStart(float,float);
	void  MovePlayer(glm::vec3&,glm::vec3&);
	void  SetCellSize(float);
	float GetCellSize();
	void  SetWallThickness(float);
	float GetWallThickness();
	float GetCellRadius();
	void  Draw();
	void  SetWallTex(const std::string&,int);
	// Col
	class Column {
		int _row;
		Maze* _super;
	public:
		Column(int row, Maze* super) : _row(row), _super(super) {}
		Cell& operator[](int col) {
			return (_super->cells[_row * _super->ySize + col]);
		}
	};
	// Row
	Column operator[](int row) { return (Column(row, this)); }
};

#endif 
