// Ahsan Khan
// 12/01/2016
// Homework 3
#ifndef _RGBPIXMAP
#define _RGBPIXMAP
#include "Window.h"
#include <fstream>
#include <iostream>
#include <string>
#include <strstream>

typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned long  ulong;

class IntPoint {
	public:
	int x, y;
	IntPoint() { x = y = 0; }
	IntPoint(int xx, int yy) {
		x = xx;
		y = yy;
	}
	void set(int xx, int yy) {
		x = xx;
		y = yy;
	}
	void set(IntPoint p) {
		x = p.x;
		y = p.y;
	}
};
class IntRect {
	public:
	int  left, top, right, bott;
	void set(int ll, int tt, int rr, int bb) {
		left  = ll;
		top   = tt;
		right = rr;
		bott  = bb;
	}
	void set(IntRect r) {
		left  = r.left;
		top   = r.top;
		right = r.right;
		bott  = r.bott;
	}
	void print() {
		std::cout << "IntRect: " << left << " " << top << " " << right << " "
				  << bott << '\n';
		int nCols		= right - left;
		int nBytes		= 3 * nCols;
		int nBytesInRow = ((nBytes + 3) / 4) * 4;
		int numPadBytes = nBytesInRow - nBytes;
		std::cout << " nCols, # pad bytes = " << nCols << "," << numPadBytes
				  << '\n';
	}
	void fix() {
		if (left > right) {
			int tmp = left;
			left	= right;
			right   = tmp;
		} // swap
		if (bott > top) {
			int temp = top;
			top		 = bott;
			bott	 = temp;
		}
	}
	void draw() {
		glBegin(GL_LINE_LOOP);
		glVertex2i(left, top);
		glVertex2i(right, top);
		glVertex2i(right, bott);
		glVertex2i(left, bott);
		glEnd();
		glFlush();
	}
	void drawDiag() {
		glBegin(GL_LINES);
		glVertex2i(left, top);
		glVertex2i(right, bott);
		glEnd();
	}
};

class mRGB {
	public:
	uchar r, g, b;
	mRGB() { r = g = b = 0; }
	mRGB(mRGB& p) {
		r = p.r;
		g = p.g;
		b = p.b;
	}
	mRGB(uchar rr, uchar gg, uchar bb) {
		r = rr;
		g = gg;
		b = bb;
	}
	void set(uchar rr, uchar gg, uchar bb) {
		r = rr;
		g = gg;
		b = bb;
	}
};

class RGBpixmap {
	private:
	mRGB* pixel;

	public:
	int nRows, nCols;
	RGBpixmap() {
		nRows = nCols = 0;
		pixel		  = 0;
	}
	RGBpixmap(int rows, int cols) {
		nRows = rows;
		nCols = cols;
		pixel = new mRGB[rows * cols];
	}
	int readBMPFile(std::string fname);

	void setTexture(GLuint textureName);
	void makeCheckerboard();

	int writeBMPFile(std::string fname);
	void freeIt() {
		delete[] pixel;
		nRows = nCols = 0;
	}
	void copy(IntPoint from, IntPoint to, int x, int y, int width, int height) {
		if (nRows == 0 || nCols == 0)
			return;
		glCopyPixels(x, y, width, height, GL_COLOR);
	}
	void draw() {
		if (nRows == 0 || nCols == 0)
			return;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glDrawPixels(nCols, nRows, GL_RGB, GL_UNSIGNED_BYTE, pixel);
	}
	int read(int x, int y, int wid, int ht) {
		nRows = ht;
		nCols = wid;
		pixel = new mRGB[nRows * nCols];
		if (!pixel)
			return -1;
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glReadPixels(x, y, nCols, nRows, GL_RGB, GL_UNSIGNED_BYTE, pixel);
		return 0;
	}
	int read(IntRect r) {
		nRows = r.top - r.bott;
		nCols = r.right - r.left;
		pixel = new mRGB[nRows * nCols];
		if (!pixel)
			return -1;
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glReadPixels(r.left, r.bott, nCols, nRows, GL_RGB, GL_UNSIGNED_BYTE,
					 pixel);
		return 0;
	}
	void setPixel(int x, int y, mRGB color) {
		if (x >= 0 && x < nCols && y >= 0 && y < nRows)
			pixel[nCols * y + x] = color;
	}
	mRGB getPixel(int x, int y) {
		mRGB bad(255, 255, 255);
		if (x < 0 || x >= nCols || y < 0 || y >= nRows) {
			std::cout << "\nx,y = " << x << "," << y << " bad in getPixel()";
			return bad;
		}
		return pixel[nCols * y + x];
	}
};

#endif