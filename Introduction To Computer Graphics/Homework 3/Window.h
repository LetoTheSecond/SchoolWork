// Ahsan Khan
// 12/01/2016
// Homework 3
#ifndef WINDOW_H
#define WINDOW_H
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
// Must include windows.h before gl.h
#include "GL\glew.h"
#include "GL\wglew.h"
// Must include glew.h before gl.h
#include <GL\GL.h>

class Window {

	MSG msg;
	HWND hWin;
	HDC wDC;
	HGLRC hRC;
	HINSTANCE hInstance;
	static bool classRegistered;
	static bool glewInitialized;
public:
	Window();
	~Window();
	bool MakeWindow(int, int, int, int,int,int,
					LRESULT(CALLBACK* fun)(HWND, UINT, WPARAM, LPARAM));
	void DisplayWindow();
	bool ProccessEvents();
	void SwapBuffers();
	HWND GetWindowHandel() { return this->hWin; }

};
#endif