// Ahsan Khan
// 12/01/2016
// Homework 3
#ifndef CAMERA_H
#define CAMERA_H
#include "glm/glm.hpp"
#include "glm\gtc\type_ptr.hpp"
#ifndef PI
#define PI 3.14159265359
#endif

struct Camera{
	glm::vec3 pos,front,up,right;
	glm::mat4 view,proj;
	float yaw,pitch,roll,sensitivity,speed,fov;

	Camera();
	void SetPos(float,float,float);
	void SetDir(float,float,float);
	void SetUp(float,float,float);
	void SetFront(float,float,float);
	void SetPos(glm::vec3&);
	void SetDir(glm::vec3&);
	void SetUp(glm::vec3&);
	void SetFront(glm::vec3&);
	void Update();
	void UpdateView();
	glm::mat4 GetView();
	void SetView(glm::vec3&,glm::vec3&,glm::vec3&);
	glm::mat4 GetPxV();
	void SetProjection(float,float,float,float);

};
#endif
