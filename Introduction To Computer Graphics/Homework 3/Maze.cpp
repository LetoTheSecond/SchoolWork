// Ahsan Khan
// 12/01/2016
// Homework 3
#include "Maze.h" 
#include "GL\glew.h"
#include "GL\wglew.h"
// Must include glew.h before gl.h
#include <GL\GL.h>
#include <cmath>
#include <cstdio>
// Private
//=====================================

void Maze::DrawCell(int x, int y,Cell& c){

	// static const auto drawWall = [&]() {
	// 	glScalef(this->cellSize, 1.0f, this->cellSize);
	// 	glBegin(GL_POLYGON);
	// 	glVertex3f(-1.00f, -1.00f, 0.00f);
	// 	glVertex3f( 1.00f, -1.00f, 0.00f);
	// 	glVertex3f( 1.00f,  1.00f, 0.00f);
	// 	glVertex3f(-1.00f,  1.00f, 0.00f);
	// 	glEnd();
	// 	return;
	// };
	static const auto drawWall = [&](){
		const float v = 0.001f;
		this->wallTexture.setTexture(this->texID);
		// Fore Side
		glBegin(GL_TRIANGLES);
			// Bottom Left
			glTexCoord2f(-4.00f,-4.00f);
			glVertex3f(-1.00f+v,-1.00f, this->wallThickness);
			// Bottom Right
			glTexCoord2f( 4.00f,-4.00f);
			glVertex3f( 1.00f-v,-1.00f, this->wallThickness);
			// Top Right
			glTexCoord2f( 4.00f, 4.00f);
			glVertex3f( 1.00f-v, 1.00f, this->wallThickness);
			// Top Right
			glTexCoord2f( 4.00f, 4.00f);
			glVertex3f( 1.00f-v, 1.00f, this->wallThickness);
			// Top Left
			glTexCoord2f(-4.00f, 4.00f);
			glVertex3f(-1.00f+v, 1.00f, this->wallThickness);
			// Bottom Left
			glTexCoord2f(-4.00f,-4.00f);
			glVertex3f(-1.00f+v,-1.00f, this->wallThickness);
		glEnd();
		// Left Side
		glBegin(GL_TRIANGLES);
			// Bottom Left
			glVertex3f(-1.00f,-1.00f, 0.00f);
			// Bottom Right
			glVertex3f(-1.00f+v,-1.00f, this->wallThickness);
			// Top Right
			glVertex3f(-1.00f+v, 1.00f, this->wallThickness);
			// Top Right
			glVertex3f(-1.00f+v, 1.00f, this->wallThickness);
			// Top Left
			glVertex3f(-1.00f, 1.00f, 0.00f);
			// Bottom Left
			glVertex3f(-1.00f,-1.00f, 0.00f);
		glEnd();
		// Far Side
		glBegin(GL_TRIANGLES);
			// Bottom Left
			glTexCoord2f(-4.00f,-4.00f);
			glVertex3f(-1.00f,-1.00f, 0.00f);
			// Bottom Right
			glTexCoord2f( 4.00f,-4.00f);
			glVertex3f( 1.00f,-1.00f, 0.00f);
			// Top Right
			glTexCoord2f( 4.00f, 4.00f);
			glVertex3f( 1.00f, 1.00f, 0.00f);
			// Top Right
			glTexCoord2f( 4.00f, 4.00f);
			glVertex3f( 1.00f, 1.00f, 0.00f);
			// Top Left
			glTexCoord2f(-4.00f, 4.00f);
			glVertex3f(-1.00f, 1.00f, 0.00f);
			// Bottom Left
			glTexCoord2f(-4.00f,-4.00f);
			glVertex3f(-1.00f,-1.00f, 0.00f);
		glEnd();
		// Right Side
		glBegin(GL_TRIANGLES);
			// Bottom Left
			glVertex3f( 1.00f,-1.00f, 0.00f);
			// Bottom Right
			glVertex3f( 1.00f-v,-1.00f, this->wallThickness);
			// Top Right
			glVertex3f( 1.00f-v, 1.00f, this->wallThickness);
			// Top Right
			glVertex3f( 1.00f-v, 1.00f, this->wallThickness);
			// Top Left
			glVertex3f( 1.00f, 1.00f, 0.00f);
			// Bottom Left
			glVertex3f( 1.00f,-1.00f, 0.00f);
		glEnd();
	};
	float _x=(float)x*this->posOffset;
	float _y=(float)y*this->posOffset;

	// Center Reference
	// glColor3f(0.0f,0.0f,0.0f);
	// glPushMatrix();
	// glTranslatef(_x,0.0f,_y);
	// glBegin(GL_POLYGON);
	// glVertex3f(-0.05f, -0.99f, 0.05f);
	// glVertex3f( 0.05f, -0.99f, 0.05f);
	// glVertex3f( 0.05f, -0.99f,-0.05f);
	// glVertex3f(-0.05f, -0.99f,-0.05f);
	// glEnd();
	// glPopMatrix();
	glColor3f(1.0f,1.0f,1.0f);

	if (!c.faces[NORTH]){
		glPushMatrix();
		glTranslatef(_x+this->cellSize,0.0f,_y);
		glRotatef(-90.0f,0.0f,1.0f,0.0f);
		glScalef(this->cellSize, 1.0f, this->cellSize);
		drawWall();
		glPopMatrix();
	}

	if (!c.faces[SOUTH]){
		// South Wall (-x)
		glPushMatrix();
		glTranslatef(_x-this->cellSize,0.0f,_y);
		glRotatef(90.0f,0.0f,1.0f,0.0f);
		glScalef(this->cellSize, 1.0f, this->cellSize);
		drawWall();
		glPopMatrix();
	}

	if (!c.faces[EAST]){
		// East Wall (+z)
		glPushMatrix();
		glTranslatef(_x,0.0f,_y+this->cellSize);
		glRotatef(180.0f,0.0f,1.0f,0.0f);
		glScalef(this->cellSize, 1.0f, this->cellSize);
		drawWall();
		glPopMatrix();
	}

	if (!c.faces[WEST]){
		// West Wall (-z)
		glPushMatrix();
		glTranslatef(_x,0.0f,_y-this->cellSize);
		glScalef(this->cellSize, 1.0f, this->cellSize);
		drawWall();
		glPopMatrix();
	}
}

// Public
//=====================================

Maze::Maze(){
	this->xSize 		= 0;
	this->ySize 		= 0;
	this->cellSize 		= 1.0f;
	this->posOffset 	= cellSize*2.0f;
	this->cellRadius 	=((this->cellSize/2.0f) - this->wallThickness)-0.2f;
	// this->cellRadius 	=((this->cellSize) - this->wallThickness)/2.0f;
	this->wallThickness	= 0.05f;
	this->playerXCell = 0;
	this->playerYCell = 0;
}

Maze::~Maze(){ delete [] this->cells; }

void Maze::InitMaze(int y, int x){
	this->xSize = x;
	this->ySize = y;
	this->cells = new Cell[x*y];
}

void Maze::PlayerStart(float x, float y){
	this->playerXCell = (int)(x/this->posOffset);
	this->playerYCell = (int)(y/this->posOffset);
}

void Maze::MovePlayer(glm::vec3& prev, glm::vec3& next) {
	if (prev == next) {
		// If the player is still
		return;
	}
	float x,y;
	int _x,_y;
	_x = (int)floor(prev.x/pow(this->cellSize,2));
	_y = (int)floor(prev.z/pow(this->cellSize,2));
	x = (_x*this->posOffset)+this->cellSize;
	y = (_y*this->posOffset)+this->cellSize;
	float distX = (next.x - x) - this->wallThickness,
		  distY = (next.z - y) - this->wallThickness;
	// printf("(%+2.2f,%+2.2f),(%+2.2f,%+2.2f),(%+2d,%+2d),(%2.2f,%2.2f)\r",prev.x,prev.z,x,y,_x,_y,distX,distY);

	if (abs(distX) > this->cellRadius){
		if (distX > 0 && !((*this)[_x][_y].faces[NORTH])){
			next.x = prev.x;
		} else if (distX < 0 && !((*this)[_x][_y].faces[SOUTH])){
			next.x = prev.x;
		}
	}
	if (abs(distY) > this->cellRadius){
		if (distY > 0 && !((*this)[_x][_y].faces[EAST])){
			next.z = prev.z;
		}else if (distY < 0 && !((*this)[_x][_y].faces[WEST])){
			next.z = prev.z;
		}
	}
}

void Maze::SetCellSize(float s){
	if (s<=0.0f){
		return;
	} else {
		this->cellSize 		= s;
		this->posOffset		= this->cellSize*2.0f;
		this->cellRadius 	=((this->cellSize) - this->wallThickness)-0.2f;
		// this->cellRadius 	= ((this->cellSize) - this->wallThickness)/4.0f;
	}
}

float Maze::GetCellSize(){ return this->cellSize; }

void Maze::SetWallThickness(float th){ this->wallThickness = th; }

float Maze::GetWallThickness(){ return this->wallThickness; }

float Maze::GetCellRadius(){ return this->cellRadius; }

void Maze::Draw(){
	glPushMatrix();
	glTranslatef(this->cellSize,0.0f,this->cellSize);
	for (int x=0;x < this->xSize;++x){
		for(int y=0;y < this->ySize;++y){
			DrawCell(x,y,(*this)[x][y]);
		}
	}
	glPopMatrix();
}

void Maze::SetWallTex(const std::string& fn, int i){
	this->wallTexture.readBMPFile(fn.c_str());
	this->wallTexture.setTexture(i);
	this->texID = i;
}
