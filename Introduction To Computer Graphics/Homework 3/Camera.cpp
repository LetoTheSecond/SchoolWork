// Ahsan Khan
// 12/01/2016
// Homework 3
#include "Camera.h"
#include "glm/gtc/matrix_transform.hpp"

Camera::Camera() {
	this->yaw		  = 000.00f;
	this->pitch		  = 000.00f;
	this->roll		  = 000.00f;
	this->sensitivity = 000.25f;
	this->speed		  = 001.00f;
	this->fov		  = 060.00f;
}

void Camera::SetPos(float x, float y, float z) {
	this->pos.x = x;
	this->pos.y = y;
	this->pos.z = z;
}

void Camera::SetDir(float x, float y, float z) {
	this->front.x = x;
	this->front.y = y;
	this->front.z = z;
}

void Camera::SetUp(float x, float y, float z) {
	this->up.x = x;
	this->up.y = y;
	this->up.z = z;
}

void Camera::SetFront(float x,float y,float z){
	this->front.x = x;
	this->front.y = y;
	this->front.z = z;
}

void Camera::SetPos(glm::vec3& v) {
	this->pos.x = v.x;
	this->pos.y = v.y;
	this->pos.z = v.z;
}

void Camera::SetDir(glm::vec3& v) {
	this->front.x = v.x;
	this->front.y = v.y;
	this->front.z = v.z;
}

void Camera::SetUp(glm::vec3& v) {
	this->up.x = v.x;
	this->up.y = v.y;
	this->up.z = v.z;
}

void Camera::SetFront(glm::vec3& v){
	this->front.x = v.x;
	this->front.y = v.y;
	this->front.z = v.z;
}

glm::mat4 Camera::GetView() {
	//this->view = glm::lookAt(this->pos, this->pos + this->front, this->up);
	return this->view;
}

void Camera::SetView(glm::vec3& _pos, glm::vec3& _target, glm::vec3& _up){
	this->view=glm::lookAt(_pos,_target,_up);
}

void Camera::UpdateView() {
	this->view = glm::lookAt(this->pos, this->pos + this->front, this->up);
	//return this->view;
}

glm::mat4 Camera::GetPxV() {
	//this->view = glm::lookAt(this->pos, this->pos + this->front, this->up);
	//glm::mat4 t = this->proj * this->view;
	return this->proj * this->view;
}

void Camera::SetProjection(float fov, float dim, float n, float f) {
	this->fov = fov;
	this->proj = glm::perspective(fov, dim, n, f);
	return;
}

void Camera::Update() {
	glm::vec3 _front;
	glm::vec3 _WorldUp(0.0f, 1.0f, 0.0f);
	_front.x =
		float(cos(this->yaw * PI / 180) * cos(this->pitch * PI / 180));
	_front.y = sin(this->pitch);
	_front.z =
		float(sin(this->yaw * PI / 180) * cos(this->pitch * PI / 180));
	glm::normalize(_front);
	this->front = _front;
	this->right = glm::cross(this->front,_WorldUp);
	glm::normalize(this->right);
	this->up=glm::cross(this->right,this->front);
	glm::normalize(this->up);
	this->view = glm::lookAt(this->pos, this->pos + this->front, this->up);
}
