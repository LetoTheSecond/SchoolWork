/*
 * Ahsan Khan
 * Feb 14, 2017
 * Operation Systems
 * Project 2
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <fcntl.h>
#include <getopt.h>

#include "logger.h"

// Time Keeping
struct timespec start,end;

// Shared Memeory Data
key_t key = 44;
int id;
int* sNum = NULL;

// Fork & Sync Data
pid_t* pids = NULL;
int forkNum=0;

void ChildLog(){
	clock_gettime(CLOCK_MONOTONIC, &end);
	int sec = (end.tv_sec - start.tv_sec);
	int nano = (end.tv_nsec - start.tv_nsec);
	Log("File modified by process number %d at time %02d:%02d "
		"with sharedNum = %d\n",forkNum,sec,nano,(*sNum));
	FILE* file = NULL;
	file = fopen("test.out","a");
	fprintf(file,"File modified by process number %d at time %02d:%02d "
		"with sharedNum = %d\n",forkNum,sec,nano,(*sNum));
	fclose(file);
}

void KillHandel(int signum){
	Log("Kill signal recived\n");
	shmdt(sNum);
	shmctl(id,IPC_RMID,0);
	free(pids);
	exit(0);
}

int main(int argc, char** argv){
	signal(SIGKILL,KillHandel);
	signal(SIGINT,KillHandel);

	FILE* file = NULL;
	file = fopen("test.out","w");
	fclose(file);

	// Program Settings
	char* filename="log.txt";
	int procNum=5,
		incNum=2,
		lifeT=20,
		o;
	while ((o = getopt(argc,argv,"s:l:i:t:h")) != -1){
		switch (o){
			case 's':
				printf("s -> %s\n",optarg);
				procNum = atoi(optarg);
				break;
			case 'l':
				printf("l -> %s\n",optarg);
				filename = optarg;
				break;
			case 'i':
				printf("i -> %s\n",optarg);
				incNum = atoi(optarg);
				break;
			case 't':
				Log("t -> %s\n",optarg);
				lifeT = atoi(optarg);
				break;
			case 'h':
				printf("Usage:\n"
						"\t-h     > Display this helpful help message\n"
						"\t-s [X] > Set the max number of slaves spawned\n"
						"\t-l [F] > Set the file name of the log file\n"
						"\t-i [Y] > Set the number of time a slave increments\n"
						"\t-t [Z] > Live time in seconds of the master\n");
			return 0;
			default:
				break;
		}
	}
	InitLog(filename);

	// Fork & Sync Data
	sem_t* sem=NULL;
	pids = (pid_t*)calloc(procNum,sizeof(pid_t));

	// Init Shared Memory
	id = shmget(key,sizeof(int),0777 | IPC_CREAT);
	if (id < 0){
		ErrorLog("shmget returned < 0\n");
		perror("~");
		exit(1);
	}
	sNum = (int*)shmat(id,NULL,0);
	*sNum = 0;
	Log("Initialized shared memeory\n");

	// Init Semaphore
	sem = sem_open("sem",O_CREAT|O_EXCL,0777,1);
	sem_unlink("sem");
	Log("Semaphore initialized\n");

	clock_gettime(CLOCK_MONOTONIC,&start);
	// Fork
	for (int i=0;i<procNum;++i,++forkNum){
		pids[i] = fork();
		if (pids[i] < 0){
			ErrorLog("Fork failed for %d fork\n",i);
		} else if (pids[i]==0){
			// If child, break
			break;
		}
	}

	// Child
	if (pids[forkNum]==0){
		sem_wait(sem);
		Log("Child %2d in critical section...\n",forkNum);
		for (int i=0;i<incNum;++i){
			sleep(rand()%2);
			(*sNum)+=1;
			ChildLog();
			sleep(rand()%2);
		}
		Log("sNum is now %2d\n",*sNum);
		sem_post(sem);
		exit(0);
	} 
	// Parent
	else {
		sleep(lifeT);
		// while (waitpid (-1, NULL, 0)){
		// 	if (errno == ECHILD)
		// 		break;
		// }
		for (int i=0;i<forkNum;++i){
			kill(pids[i],SIGKILL);
		}
		// Log("All children finished\n");
		shmdt(sNum);
		shmctl(id,IPC_RMID,0);
		sem_destroy(sem);
		free(pids);
		exit(0);
	}

	return 0;
}
