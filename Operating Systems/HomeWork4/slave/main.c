// Ahsan Khan
// April 16, 2017
// CS4760 Project 4

// C Headers
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

// System Headers
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

// Local Headers
#include "Logger.h"

// Data Types
typedef struct SIMTIME_T{
	unsigned long sec,	// 10^01
				  mili, // 10^-3
				  micro,// 10^-6
				  nano; // 10^-9
} SimTime;

typedef struct PCB_T{
	int id;
	SimTime WallTime, // Time Active
			CPUTime;  // Time Alive
} PCB;

typedef struct MSG_T{
	long type;
	int id;
	SimTime time;
	char msg[256];
} MSG;

typedef struct SHM_T {
	SimTime clock;
	PCB pcbs[20];
}SharedMem;

// Defines

// Global Variables
int shm_id = -1,
	mq1_id = -1,
	mq2_id = -1,
	mq3_id = -1;

SharedMem* 	shm = NULL;

void KillHandel(int sig){
	Log("Kill signal receved, terminating...\n");
	Log("Cleaning up\n");
	int ret;
	if (shm){
		ret = shmdt(shm);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		Log("shmdt\n");
	}
	exit(0);
}

int main(int argc, char* argv[]){
	//==========================================================================
	signal(SIGTERM,KillHandel);
	signal(SIGINT,KillHandel);
	//==========================================================================
	// Variables
	int id=0;
	id = atoi(argv[0]);
	char buff[256];
	sprintf(buff,"proc_%03d_log.txt",id);
	InitLog(buff);
	//==========================================================================
	// Variables
	key_t 	shm_k = -1,
			mq1_k = -1,
			mq2_k = -1,
			mq3_k = -1;
	int 	msgSize = sizeof(MSG)-sizeof(long);
	//==========================================================================
	//Initialization
	// Get Keys
	shm_k = ftok("./oss",0);
	if ( shm_k < 0){
		int e = errno;
		ErrorLog("ftok for shm_k failed: %s\n",strerror(e));
		return -1;
	}
	mq1_k = ftok("./oss",1);
	if ( mq1_k < 0){
		int e = errno;
		ErrorLog("ftok for msg1_k failed: %s\n",strerror(e));
		return -1;
	}
	mq2_k = ftok("./oss",2);
	if ( mq2_k < 0){
		int e = errno;
		ErrorLog("ftok for mq2_k failed: %s\n",strerror(e));
		return -1;
	}
	mq3_k = ftok("./oss",3);
	if ( mq3_k < 0){
		int e = errno;
		ErrorLog("ftok for mq3_k failed: %s\n",strerror(e));
		return -1;
	}
	shm_id = shmget(shm_k,sizeof(SharedMem), 0777 | IPC_CREAT);
	if (shm_id < 0){
		int e = errno;
		ErrorLog("shmget failed: %s\n",strerror(e));
		return -1;
	}
	// Get Shared Mem
	shm = (SharedMem*) shmat(shm_id,NULL,0);
	if (shm == (SharedMem*)-1 ){
		int e = errno;
		ErrorLog("shmat filed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	// Get Ids
	mq1_id = msgget(mq1_k, 0777 | IPC_CREAT);
	if (mq1_id < 0){
		int e = errno;
		ErrorLog("msgget 1 failed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	mq2_id = msgget(mq2_k, 0777 | IPC_CREAT);
	if (mq2_id < 0){
		int e = errno;
		ErrorLog("msgget 2 failed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	mq3_id = msgget(mq3_k, 0777 | IPC_CREAT);
	if (mq3_id < 0){
		int e = errno;
		ErrorLog("msgget 3 failed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	//==========================================================================
	// Main Loop
	unsigned long life = rand()%500000000;
	unsigned long startT = _clock[0]*1000000000 + _clock[1];
	unsigned long endSS,endNN;
	bool 	run 				= true,
			request 			= true,
			request_wait 		= false,
			critical_section 	= false,
			give_up				= false,
			done 				= false,
			die 				= false,
			die_wait 			= false;
	while (run){
		// Ask for access
		if (request){
			MSG req;
			req.type=1;
			req.procNum=id;
			sprintf(req.data,"Ping[%03d]\0",id-3);
			int ret = msgsnd(msg1_id,&req,msgSize,0);
			if (ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(request) Looks like master terminated: %s\n"
						,strerror(e));
					return -1;
				}
				ErrorLog("(request) msgsnd failed: %s\n",strerror(e));
				run=false;
			} else {
				Log("Message sent: %s\n",req.data);
				request=false;
				request_wait=true;
			}
		}
		// Wait for access
		if (request_wait){
			MSG wait;
			int ret = msgrcv(msg2_id,&wait,msgSize,id,IPC_NOWAIT);
			if ( errno != ENOMSG && ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(request_wait) Looks like master terminated: %s\n",
						strerror(e));
					return -1;
				}
				ErrorLog("(request) msgrcv failed: %s\n",strerror(e));
				run=false;
			} else if (ret > 0){
				Log("Message recived: %s\n",wait.data);
				// Have access to critical section
				critical_section=true;
				request_wait=false;
			}
		}
		// Do critical section
		if (critical_section){
			Log("In Critical Section\n");
			// 1s = 1000000000ns
			unsigned long t = _clock[0]*1000000000 + _clock[1];
			unsigned long diff = t - startT;
			if (diff >= life){
				done = true;
				endSS = _clock[0];
				endNN = _clock[1];
			}
			critical_section = false;
			give_up = true;
		}
		// Give up access to critical section
		if (give_up){
			MSG fin;
			fin.type=id;
			fin.procNum=id;
			sprintf(fin.data,"%03d access given up\0",id-3);
			int ret = msgsnd(msg1_id,&fin,msgSize,0);
			if (ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(give_up) Looks like master terminated: %s\n",
						strerror(e));
					return -1;
				}
				ErrorLog("msgsnd for fin failed: %s\n",strerror(e));
				run=false;
			} else {
				Log("Message sent: %s\n",fin.data);
				give_up=false;
				if (done){
					die = true;
				} else {
					request = true;
				}
			}
		}
		// Notify of impending death
		if (die){
			MSG req;
			req.type=2;
			req.procNum=id;
			req.mm=endSS;
			req.nn=endNN;
			sprintf(req.data,"Ready to die[%03d]\0",id-3);
			int ret = msgsnd(msg1_id,&req,msgSize,0);
			if (ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(die) Looks like master terminated: %s\n"
						,strerror(e));
					return -1;
				}
				ErrorLog("(die) msgsnd failed: %s\n",strerror(e));
			} else {
				Log("Message sent: %s\n",req.data);
				die=false;
				die_wait=true;
			}
		}
		// Wait for death acknowledgment
		if (die_wait){
			MSG wait;
			int ret = msgrcv(msg2_id,&wait,msgSize,id,0);
			if ( ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(die_wait) Looks like master terminated: %s\n",
						strerror(e));
					return -1;
				}
				ErrorLog("(die_wait) msgrcv failed: %s\n",strerror(e));
			} else {
				Log("Message recived: %s\n",wait.data);
				die_wait=false;
				run=false;
			}
		}
	}

	//==========================================================================
	// Cleanup
	raise(SIGTERM);}

