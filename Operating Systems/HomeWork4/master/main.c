// Ahsan Khan
// April 16, 2017
// CS4760 Project 4

// C Headers
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

// System Headers
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

// Local Headers
#include "Logger.h"

// Data Types
typedef struct SIMTIME_T{
/*
	unsigned long sec,	// 10^01
				  mili, // 10^-3
				  micro,// 10^-6
				  nano; // 10^-9
*/
	unsigned long nano;
} SimTime;

typedef struct PCB_T{
	int id,
		msgQ;
	SimTime WallTime, // Time Active
			CPUTime;  // Time Alive
} PCB;

typedef struct MSG_T{
	long type;
	int id;
	SimTime time;
	char msg[256];
} MSG;

typedef struct SHM_T {
	SimTime clock;
	PCB pcbs[20];
}SharedMem;

// Defines

// Global Variables
int shm_id = -1,
	mq1_id = -1,
	mq2_id = -1,
	mq3_id = -1,
	slaves = 20;
pid_t* 		pids = NULL;
SharedMem* 	shm = NULL;

void KillHandel(int sig){
	Log("Kill signal receved, terminating...\n");
	Log("Cleaning up\n");
	int ret;
	if (shm){
		ret = shmdt(shm);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		Log("shmdt\n");
	}
	if (shm_id >= 0){
		ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		Log("shmctl\n");
	}
	if (mq1_id >= 0){
		ret = msgctl(mq1_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("msgctl RMID for mq1_id failed: %s\n",strerror(e));
		}
		Log("msgctl 1\n");
	}
	if (mq2_id >= 0){
		ret = msgctl(mq2_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("msgctl RMID for mq2_id failed: %s\n",strerror(e));
		}
		Log("msgctl 2\n");
	}
	if (mq3_id >= 0){
		ret = msgctl(mq3_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("msgctl RMID for mq3_id failed: %s\n",strerror(e));
		}
		Log("msgctl 3\n");
	}
	if (pids){
		for (int i=0;i<slaves;++i){
			if (pids[i] > 0){
				int ret = kill(pids[i],SIGTERM);
				if (ret < 0 ){
					ErrorLog("Killing %d failed\n",pids[i]);
				}
			}
		}
	}
	exit(0);
}


int main(int argc, char* argv[]){
	//==========================================================================
	signal(SIGTERM,KillHandel);
	signal(SIGINT,KillHandel);
	// Program Settings
	char* filename="log.txt";
	int o;
	while ((o = getopt(argc,argv,"s:l:t:h")) != -1){
		switch (o){
			case 'h':
				printf("Usage:\n");
				printf("\toss [arguments]\n");
				printf("Arguments:\n");
				printf("\t%-14s -> %s\n","-s [N]","Sets number of processes, "
						"default is 5");
				printf("\t%-14s -> %s\n","-l <filename>","Sets log filename, "
						"default is log.txt");
				return 0;
			case 's':
				slaves=atoi(optarg);
				break;
			case 'l':
				filename=optarg;
				break;
			default:
				break;
		}
	}
	InitLog(filename);
	//==========================================================================
	// Variables
	key_t 	shm_k = -1,
			mq1_k = -1,
			mq2_k = -1,
			mq3_k = -1;
	int 	msgSize = sizeof(MSG)-sizeof(long);
	//==========================================================================
	//Initialization
	pids = (pid_t*)calloc(slaves,sizeof(pid_t));
	// Get Keys
	shm_k = ftok("./oss",0);
	if ( shm_k < 0){
		int e = errno;
		ErrorLog("ftok for shm_k failed: %s\n",strerror(e));
		return -1;
	}
	mq1_k = ftok("./oss",1);
	if ( mq1_k < 0){
		int e = errno;
		ErrorLog("ftok for msg1_k failed: %s\n",strerror(e));
		return -1;
	}
	mq2_k = ftok("./oss",2);
	if ( mq2_k < 0){
		int e = errno;
		ErrorLog("ftok for mq2_k failed: %s\n",strerror(e));
		return -1;
	}
	mq3_k = ftok("./oss",3);
	if ( mq3_k < 0){
		int e = errno;
		ErrorLog("ftok for mq3_k failed: %s\n",strerror(e));
		return -1;
	}
	shm_id = shmget(shm_k,sizeof(SharedMem), 0777 | IPC_CREAT | IPC_EXCL);
	if (shm_id < 0){
		int e = errno;
		ErrorLog("shmget failed: %s\n",strerror(e));
		return -1;
	}
	// Get Shared Mem
	shm = (SharedMem*) shmat(shm_id,NULL,0);
	if (shm == (SharedMem*)-1 ){
		int e = errno;
		ErrorLog("shmat filed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	// Get Ids
	mq1_id = msgget(mq1_k, 0777 | IPC_CREAT | IPC_EXCL);
	if (mq1_id < 0){
		int e = errno;
		ErrorLog("msgget 1 failed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	mq2_id = msgget(mq2_k, 0777 | IPC_CREAT | IPC_EXCL);
	if (mq2_id < 0){
		int e = errno;
		ErrorLog("msgget 2 failed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	mq3_id = msgget(mq3_k, 0777 | IPC_CREAT | IPC_EXCL);
	if (mq3_id < 0){
		int e = errno;
		ErrorLog("msgget 3 failed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	//==========================================================================

/*
	typedef struct PCB_T{
		int id;
		int msgQ;
		SimTime WallTime, // Time Active
				CPUTime;  // Time Alive
	} PCB;

	typedef struct MSG_T{
		long type;
		int id;
		SimTime time;
		char msg[256];
	} MSG;

	typedef struct SHM_T {
		SimTime clock;
		PCB pcbs[20];
	}SharedMem;
*/

	// Main Loop
	//shm->clock.sec = shm->clock.mili = shm->clock.micro = shm->clock.nano = 0;
	shm->clock.nano=0;
	unsigned long SECOND = 1000000000; // 1 billion nanoseconds = 1 second
	int spawns = 0;
	bool run = true;
	unsigned long nextSpawn = 0;
	while (run){
		
		// Spawn User Process
		if (spawns < 20 && shm->clock.nano >= nextSpawn){
			pid_t pid = fork();
			if (pid == 0){
				char buff[256];
				sprintf(buff,"%d\0",spawns);
				int ret = execl("./user",buff,(char*)0);
				if (ret <-1){
					int e = errno;
					ErrorLog("execl for %d failed: %s\n",
						spawns,strerror(e));
					return -1;
				}
			} else if (pid < 0){
				int e = errno;
				ErrorLog("fork for %d failed: %s\n",spawns,strerror(e));
				pids[spawns]=-1;
			} else {
				pids[spawns]=pid;
				Log("Process %d spawned into Message Q 1\n",spawns);
				nextSpawn=shm->clock.nano+SECOND;
				shm->pcbs[spawns].id=spawns;
				shm->pcbs[spawns].magQ=1;
				++spawns;
			}
		}

		//Advance Clock
		shm->clock.nano+=rand()%1000;
		// Increment Clock
		_clock[1]+=rand()%50;
		if (_clock[1] >= 1000000000){
			++_clock[0];
			_clock[1]=0;
		}
		// Check for a request
		if (request){
			MSG req;
			int ret = msgrcv(msg1_id,&req,msgSize,1,IPC_NOWAIT);
			if ( errno != ENOMSG && ret < 0){
				int e = errno;
				ErrorLog("(request) msgrcv failed: %s\n",strerror(e));
			} else if (ret > 0){
				request=false;
				request_ack=true;
				curProc=req.procNum;
				// Log("Message recived(request): %s\n",req.data);
			}
		}
		// Send message giving access to critical section
		if (request_ack){
			MSG snd;
			snd.type = curProc;
			snd.procNum = -1;
			sprintf(snd.data,"Pong[%03d]\0",curProc-3);
			int ret = msgsnd(msg2_id,&snd,msgSize,0);
			if (ret < 0){
				int e = errno;
				ErrorLog("(request_ack) msgsnd failed: %s type: %d, data: %s,"
					" size: %d\n",strerror(e),curProc,snd.data,msgSize);
				run=false;
			} else {
				waiting = true;
				request_ack=false;
				// Log("Message sent(request_ack) %s\n",snd.data);
			}
		}
		// Check if access if given up
		if (waiting){
			MSG fin;
			int ret = msgrcv(msg1_id,&fin,msgSize,curProc,IPC_NOWAIT);
			if ( errno != ENOMSG && ret == -1){
				int e = errno;
				ErrorLog("(waiting) msgrcv failed: %s\n",strerror(e));
			} else if (ret > 0){
				waiting=false;
				// waiting_ack=true;
				request=true;
				// Log("Message recived(waiting) %s\n",fin.data);
				curProc=1;
			}
		}
		// Check if anyone is ready to die
		MSG death;
		int ret = msgrcv(msg1_id,&death,msgSize,2,IPC_NOWAIT);
		if ( errno != ENOMSG && ret == -1){
			int e = errno;
			ErrorLog("(die) msgrcv failed: %s\n",strerror(e));
		} else if (ret > 0) {
			// Log("Message recived(death): %s\n",death.data);
			MSG ack_death;
			ack_death.type=death.procNum;
			ack_death.procNum=-1;
			sprintf(ack_death.data,"Rip in peace[%03d]\0",death.procNum-3);
			ret = msgsnd(msg2_id,&ack_death,msgSize,0);
			if (ret < 0){
				int e = errno;
				ErrorLog("(ack_death) msgsnd failed: %s\n",strerror(e));
			} else {
				// Log("Message sent(death) %s\n",ack_death.data);
				Log("Master: Child %d is terminating at my time %lu:%lu because"
					" it reached %lu:%lu in slave\n",pids[death.procNum-3],
					_clock[0],_clock[1],death.mm,death.nn);
				++done;
				if (spawns<100){
					pid_t pid = fork();
					if (pid == 0){
						char buff[256];
						sprintf(buff,"%d\0",spawns);
						int ret = execl("./user",buff,(char*)0);
						if (ret <-1){
							int e = errno;
							ErrorLog("execl for %d failed: %s\n",
								spawns,strerror(e));
							return -1;
						}
					} else if (pid < 0){
						int e = errno;
						ErrorLog("fork for %d failed: %s\n",spawns,strerror(e));
						pids[spawns]=-1;
					} else {
						pids[spawns]=pid;
						// Log("Process %d spawned\n",spawns);
						++spawns;
					}
				}
			}
		}
		Log("SS:[%2d] NN:[%9d] Spawns:[%d/%d] Life:[%d]\n",_clock[0],_clock[1], done,spawns,(time(NULL) - start));
		// Check if its time to die
		if (_clock[0] == 2 || (time(NULL) - start) > life  || done == 100){
			run = false;
		}
	//==========================================================================
	// Cleanup
	raise(SIGTERM);
}

