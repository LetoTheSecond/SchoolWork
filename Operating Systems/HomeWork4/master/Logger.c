// Ahsan Khan
// April 08, 2017
// CS4760 Project 3
#if defined _WIN32 && !defined _CRT_SECURE_NO_WARNINGS
	#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Logger.h"

// #ifndef LOG_FILENAME
// #define LOG_FILENAME "LOG.txt"
// #endif
static char* LOG_FILENAME=NULL;

bool InitLog(const char* logName){
	LOG_FILENAME = (char*)malloc(strlen(logName))+1;
	if (strcmp(strcpy(LOG_FILENAME,logName),logName) != 0){
		if (LOG_FILENAME) free(LOG_FILENAME);
		char* temp = "LOG.txt";
		LOG_FILENAME = (char*)malloc(strlen(temp))+1;
		strcpy(LOG_FILENAME,temp);
	}
	#ifndef NO_LOG_FILE
		FILE* log=NULL;
		log=fopen(LOG_FILENAME,"a");
		if (!log){
			fprintf(stderr,"Could not open log file : %s : for writing\n",
					LOG_FILENAME);
			perror("Perror ");
			return false;
		}
	#endif

	time_t now=time(NULL);
	//std::string timestamp="[";
	//timestamp+=ctime(&now);
	//timestamp.pop_back();
	//timestamp+="] Log > ";
	char* date = ctime(&now);
	date[strlen(date)-1]='\0';
	char timestamp[strlen(date)+9];
	sprintf(timestamp,"[%s] Log > ",date);
	#ifndef NO_LOG_FILE
		//fprintf(log,"%s",timestamp.data());
		fprintf(log,"%s",timestamp);
		fprintf(log,"Log Init\n");
		fclose(log);
	#endif
	#ifdef STDOUT_LOG
		//fprintf(stdout,"%s",timestamp.data());
		fprintf(stdout,"%s",timestamp);
		fprintf(stdout,"Log Init\n");
	#endif
	return true;
}

bool RestartLog(){
	#ifndef NO_LOG_FILE
		FILE* log=NULL;
		log=fopen(LOG_FILENAME,"w");
		if (!log){
			fprintf(stderr,"Could not open log file : %s : for writing\n",
					LOG_FILENAME);
			perror("Perror ");
			return false;
		}
		fclose(log);
	#endif
	if (LOG_FILENAME)
		return InitLog(LOG_FILENAME);
	else
		return InitLog("LOG.txt");
}

bool Log(const char* inMsg,...){
	#ifndef NO_LOG_FILE
		FILE* log=NULL;
		log=fopen(LOG_FILENAME,"a");
		if (!log){
			fprintf(stderr,"Could not open log file : %s : for appending\n",
					LOG_FILENAME);
			perror("Perror ");
			return false;
		}
	#endif
	va_list args;
	time_t now=time(NULL);
	//std::string timestamp="[";
	//timestamp+=ctime(&now);
	//timestamp.pop_back();
	//timestamp+="] Log > ";
	char* date = ctime(&now);
	date[strlen(date)-1]='\0';
	char timestamp[strlen(date)+9];
	sprintf(timestamp,"[%s] Log > ",date);
	#ifndef NO_LOG_FILE
		va_start(args,inMsg);
		//fprintf(log,"%s",timestamp.data());
		fprintf(log,"%s",timestamp);
		vfprintf(log,inMsg,args);
		va_end(args);
		fclose(log);
	#endif
	#ifdef STDOUT_LOG
		va_start(args,inMsg);
		//fprintf(stdout,"%s",timestamp.data());
		fprintf(stdout,"%s",timestamp);
		vfprintf(stdout,inMsg,args);
		va_end(args);
	#endif
	return true;
}

bool ErrorLog(const char* inMsg,...){
	#ifndef NO_LOG_FILE
		FILE* log=NULL;
		log=fopen(LOG_FILENAME,"a");
		if (!log){
			fprintf(stderr,"Could not open log file : %s : for appending\n",
					LOG_FILENAME);
			perror("Perror ");
			return false;
		}
	#endif
	va_list args;
	time_t now=time(NULL);
	//std::string timestamp="[";
	//timestamp+=ctime(&now);
	//timestamp.pop_back();
	//timestamp+="] Log > ";
	char* date = ctime(&now);
	date[strlen(date)-1]='\0';
	char timestamp[strlen(date)+11];
	sprintf(timestamp,"[%s] ERROR > ",date);
	#ifndef NO_LOG_FILE
		va_start(args,inMsg);
		//fprintf(log,"%s",timestamp.data());
		fprintf(log,"%s",timestamp);
		vfprintf(log,inMsg,args);
		va_end(args);
		fclose(log);
	#endif
	va_start(args,inMsg);
	//fprintf(stderr,"%s",timestamp.data());
	fprintf(stderr,"%s",timestamp);
	vfprintf(stderr,inMsg,args);
	//perror("Perror ");
	va_end(args);

	#ifdef STDOUT_LOG
		va_start(args,inMsg);
		//fprintf(stdout,"%s",timestamp.data());
		fprintf(stdout,"%s",timestamp);
		vfprintf(stdout,inMsg,args);
		//perror("Perror ");
		va_end(args);
	#endif

	return true;
}

