#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Logger.h"

#define PROCESSES 	10
#define PAGES 		8
int main(){
	InitLog("log.txt");
	srand(time(NULL));
	int OPTIMAL=0,
		FIFO=1,
		Memory[5],
		PageFaults[2][PROCESSES],
		Requests[PROCESSES][15];
	bool found;
	for (int i=0;i<PROCESSES;++i){
		for (int j=0;j<15;++j){
			Requests[i][j]=rand()%PAGES;
		}
	}
	for (int proc=0;proc<PROCESSES;++proc){
		Log("Process %d\n",proc);
		PageFaults[OPTIMAL][proc]=0;
		PageFaults[FIFO][proc]=0;
		int idx=0;
		
		Log("Requests: [%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d]\n",
				Requests[proc][ 0],Requests[proc][ 1],Requests[proc][ 2],
				Requests[proc][ 3],Requests[proc][ 4],Requests[proc][ 5],
				Requests[proc][ 6],Requests[proc][ 7],Requests[proc][ 8],
				Requests[proc][ 9],Requests[proc][10],Requests[proc][11],
				Requests[proc][12],Requests[proc][13],Requests[proc][14]);
		

//		Log("\t=Optimal=\n");	
		for (;idx<5;++idx){
			Memory[idx]=Requests[proc][idx];
		}
//		Log("Memory: \t[%d,%d,%d,%d,%d]\n",
//		Memory[0],Memory[1],Memory[2],Memory[3],Memory[4]);
		++idx;
		for (;idx<15;++idx){
//			Log("Request[%d]: \t[%d]\n",idx,Requests[proc][idx]);
			found=false;
			for (int j=0;j<5;++j){
				if (Memory[j]==Requests[proc][idx]){
					found=true;
				}
			}
			if (!found){
				++PageFaults[OPTIMAL][proc];
//				Log("Page Fault, \tTotal: [%d]\n",PageFaults[OPTIMAL][proc]);
				int place=0,time=0;
				for(int i=0;i<5;++i){
					int t=0;
					for (int j=15-idx;j<15;++j){
						if (Memory[i]!=Requests[proc][j]){
							++t;
						} else {
							break;
						}
					}
					if (t>time){
						place=i;
					}
				}
				Memory[place] = Requests[proc][idx];
			}
		}
		
		
//		Log("\t=FIFO=\n");
		idx=0;
		for (;idx<5;++idx){
			Memory[idx]=Requests[proc][idx];
		}
//		Log("Memory: \t[%d,%d,%d,%d,%d]\n",
//				Memory[0],Memory[1],Memory[2],Memory[3],Memory[4]);

		int place=0;
		++idx;
		for (;idx<15;++idx){
//			Log("Request[%d]: \t[%d]\n",idx,Requests[proc][idx]);
			// Check if next request is in memory
			found=false;
			for (int j=0;j<5;++j){
				if (Memory[j]==Requests[proc][idx]){
					found=true;
//					Log("Found! Memory: [%d,%d,%d,%d,%d] at %d\n",
//						Memory[0],Memory[1],Memory[2],Memory[3],Memory[4],j);
					break;
				}
			}
			if (!found){
				++PageFaults[FIFO][proc];
				Memory[place]=Requests[proc][idx];
//				Log("Page Fault, \tTotal: [%d]\n",PageFaults[FIFO][proc]);
				++place;
				place%=5;
			}
		}

//		Log("\n");
		Log("Optimal Page Faults: %d\n",PageFaults[OPTIMAL][proc]);
		Log("FIFO    Page Faults: %d\n",PageFaults[FIFO][proc]);
		Log("========================================\n");
	}

	int avgOptimal=0,avgFIFO=0;
	
	for (int i=0;i<PROCESSES;++i){
		avgOptimal+=PageFaults[OPTIMAL][i];
		avgFIFO+=PageFaults[FIFO][i];
	}
	avgOptimal/=PROCESSES;
	avgFIFO/=PROCESSES;
	Log("Average Optimal Page Faults: %d\n",avgOptimal);
	Log("Average FIFO    Page Faults: %d\n",avgFIFO);
	Log("-----------------------\n");

	return 0;
}

