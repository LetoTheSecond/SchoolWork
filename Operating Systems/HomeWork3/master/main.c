// Ahsan Khan
// April 08, 2017
// CS4760 Project 3
// C Headers
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

// System Headers
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

// Local Headers
#include "Logger.h"

// Global Variables
int shm_id  = -1,
	msg1_id = -1,
	msg2_id = -1,
	slaves  =  5;
unsigned long* _clock = NULL;
pid_t* pids = NULL;
typedef struct MSG_t {
	long type;
	int procNum;
	unsigned long mm,nn;
	char data[256];
}MSG;

void KillHandel(int sig){
	Log("Kill signal receved, terminating...\n");
	Log("Cleaning up\n");
	int ret;
	ret = shmdt(_clock);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmdt failed: %s\n",strerror(e));
	}
	Log("shmdt\n");
	ret = shmctl(shm_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmctl failed: %s\n",strerror(e));
	}
	Log("shmctl\n");
	ret = msgctl(msg1_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("msgctl1 failed: %s\n",strerror(e));
	}
	Log("msgctl1\n");
	ret = msgctl(msg2_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("msgctl2 failed: %s\n",strerror(e));
	}
	Log("msgctl2\n");
	if (pids){
		for (int i=0;i<slaves;++i){
			if (pids[i] > 0){
				int ret = kill(pids[i],SIGTERM);
				if (ret < 0 ){
					ErrorLog("Killing %d failed\n",pids[i]);
				}
			}
		}
		// if (pids) free(pids);
	}
	exit(0);
}

int main(int argc, char* argv[]){
	//==========================================================================
	signal(SIGTERM,KillHandel);
	signal(SIGINT,KillHandel);
	// Program Settings
	char* filename="log.txt";
	int life=20, o;
	while ((o = getopt(argc,argv,"s:l:t:h")) != -1){
		switch (o){
			case 'h':
				printf("Usage:\n");
				printf("\toss [arguments]\n");
				printf("Arguments:\n");
				printf("\t%-14s -> %s\n","-s [N]","Sets number of processes, "
						"default is 5");
				printf("\t%-14s -> %s\n","-l <filename>","Sets log filename, "
						"default is log.txt");
				printf("\t%-14s -> %s\n","-t [N]","Sets lifetime of OS in "
						"seconds, default is 20");
				return 0;
			case 's':
				slaves=atoi(optarg);
				break;
			case 'l':
				filename=optarg;
				break;
			case 't':
				life=atoi(optarg);
				break;
			default:
				break;
		}
	}
	InitLog(filename);
	//==========================================================================
	// Variables
	key_t 	shm_k,
			msg1_k,
			msg2_k;
	int 	spawns=0,done=0;
	int 	msgSize = sizeof(MSG)-sizeof(long);
	//==========================================================================
	//Initialization
	pids = (pid_t*)calloc(slaves,sizeof(pid_t));
	shm_k = ftok("./oss",1);
	if ( shm_k < 0){
		int e = errno;
		ErrorLog("ftok for shm_k failed: %s\n",strerror(e));
		return -1;
	}
	msg1_k = ftok("./oss",2);
	if ( msg1_k < 0){
		int e = errno;
		ErrorLog("ftok for msg1_k failed: %s\n",strerror(e));
		return -1;
	}
	msg2_k = ftok("./oss",3);
	if ( msg2_k < 0){
		int e = errno;
		ErrorLog("ftok for msg2_k failed: %s\n",strerror(e));
		return -1;
	}
	shm_id = shmget(shm_k,2*sizeof(unsigned long), 0777 | IPC_CREAT | IPC_EXCL);
	if (shm_id < 0){
		int e = errno;
		ErrorLog("shmget failed: %s\n",strerror(e));
		return -1;
	}
	_clock = (unsigned long*) shmat(shm_id,NULL,0);
	if (_clock == (unsigned long*)-1 ){
		int e = errno;
		ErrorLog("shmat filed: %s\n",strerror(e));
		int ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		return -1;
	}
	_clock[0]=0;
	_clock[1]=0;
	msg1_id = msgget(msg1_k, 0777 | IPC_CREAT | IPC_EXCL);
	if (msg1_id < 0){
		int e = errno;
		ErrorLog("msgget1 failed: %s\n",strerror(e));
		int ret = shmdt(_clock);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		return -1;
	}
	msg2_id = msgget(msg2_k, 0777 | IPC_CREAT | IPC_EXCL);
	if (msg2_id < 0){
		int e = errno;
		ErrorLog("msgget2 failed: %s\n",strerror(e));
		int ret = shmdt(_clock);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		ret = msgctl(msg1_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("msgctl1 failed: %s\n",strerror(e));
		}
		return -1;
	}
	//==========================================================================
	// Spawn processes
	for (;spawns<slaves;++spawns){
		pid_t pid = fork();
		if (pid == 0){
			char buff[256];
			sprintf(buff,"%d\0",spawns);
			int ret = execl("./user",buff,(char*)0);
			if (ret <-1){
				int e = errno;
				ErrorLog("execl for %d failed: %s\n",spawns,strerror(e));
			}
		} else if (pid < 0){
			int e = errno;
			ErrorLog("fork for %d failed: %s\n",spawns,strerror(e));
			pids[spawns]=-1;
		} else {
			pids[spawns]=pid;
			Log("Process %d spawned\n",spawns);
		}
	}
	//==========================================================================
	// Main Loop
	time_t start;
	time(&start);
	int 	curProc 	= 1; // Process w/ critical section access
	bool 	run 		= true,
			request 	= true,
			request_ack = false,
			waiting 	= false,
			waiting_ack = false;
	while (run){
		// Increment Clock
		_clock[1]+=rand()%50;
		if (_clock[1] >= 1000000000){
			++_clock[0];
			_clock[1]=0;
		}
		// Check for a request
		if (request){
			MSG req;
			int ret = msgrcv(msg1_id,&req,msgSize,1,IPC_NOWAIT);
			if ( errno != ENOMSG && ret < 0){
				int e = errno;
				ErrorLog("(request) msgrcv failed: %s\n",strerror(e));
			} else if (ret > 0){
				request=false;
				request_ack=true;
				curProc=req.procNum;
				// Log("Message recived(request): %s\n",req.data);
			}
		}
		// Send message giving access to critical section
		if (request_ack){
			MSG snd;
			snd.type = curProc;
			snd.procNum = -1;
			sprintf(snd.data,"Pong[%03d]\0",curProc-3);
			int ret = msgsnd(msg2_id,&snd,msgSize,0);
			if (ret < 0){
				int e = errno;
				ErrorLog("(request_ack) msgsnd failed: %s type: %d, data: %s,"
					" size: %d\n",strerror(e),curProc,snd.data,msgSize);
				run=false;
			} else {
				waiting = true;
				request_ack=false;
				// Log("Message sent(request_ack) %s\n",snd.data);
			}
		}
		// Check if access if given up
		if (waiting){
			MSG fin;
			int ret = msgrcv(msg1_id,&fin,msgSize,curProc,IPC_NOWAIT);
			if ( errno != ENOMSG && ret == -1){
				int e = errno;
				ErrorLog("(waiting) msgrcv failed: %s\n",strerror(e));
			} else if (ret > 0){
				waiting=false;
				// waiting_ack=true;
				request=true;
				// Log("Message recived(waiting) %s\n",fin.data);
				curProc=1;
			}
		}
		// Check if anyone is ready to die
		MSG death;
		int ret = msgrcv(msg1_id,&death,msgSize,2,IPC_NOWAIT);
		if ( errno != ENOMSG && ret == -1){
			int e = errno;
			ErrorLog("(die) msgrcv failed: %s\n",strerror(e));
		} else if (ret > 0) {
			// Log("Message recived(death): %s\n",death.data);
			MSG ack_death;
			ack_death.type=death.procNum;
			ack_death.procNum=-1;
			sprintf(ack_death.data,"Rip in peace[%03d]\0",death.procNum-3);
			ret = msgsnd(msg2_id,&ack_death,msgSize,0);
			if (ret < 0){
				int e = errno;
				ErrorLog("(ack_death) msgsnd failed: %s\n",strerror(e));
			} else {
				// Log("Message sent(death) %s\n",ack_death.data);
				Log("Master: Child %d is terminating at my time %lu:%lu because"
					" it reached %lu:%lu in slave\n",pids[death.procNum-3],
					_clock[0],_clock[1],death.mm,death.nn);
				++done;
				if (spawns<100){
					pid_t pid = fork();
					if (pid == 0){
						char buff[256];
						sprintf(buff,"%d\0",spawns);
						int ret = execl("./user",buff,(char*)0);
						if (ret <-1){
							int e = errno;
							ErrorLog("execl for %d failed: %s\n",
								spawns,strerror(e));
							return -1;
						}
					} else if (pid < 0){
						int e = errno;
						ErrorLog("fork for %d failed: %s\n",spawns,strerror(e));
						pids[spawns]=-1;
					} else {
						pids[spawns]=pid;
						// Log("Process %d spawned\n",spawns);
						++spawns;
					}
				}
			}
		}
		// Log("SS:[%2d] NN:[%9d] Spawns:[%d/%d] Life:[%d]\n",_clock[0],_clock[1],
			// done,spawns,(time(NULL) - start));
		// Check if its time to die
		if (_clock[0] == 2 || (time(NULL) - start) > life  || done == 100){
			run = false;
		}
	}

	//==========================================================================
	// Cleanup
	Log("Cleaning up\n");
	for (int i=0;i<slaves;++i){
		if (pids[i] > 0){
			int ret = kill(pids[i],SIGTERM);
			if (ret < 0 ){
				ErrorLog("Killing %d failed\n",pids[i]);
			}
		}
	}
	// if (pids) free(pids);
	int ret;
	ret = shmdt(_clock);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmdt failed: %s\n",strerror(e));
	}
	Log("shmdt\n");
	ret = shmctl(shm_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmctl failed: %s\n",strerror(e));
	}
	Log("shmctl\n");
	ret = msgctl(msg1_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("msgctl1 failed: %s\n",strerror(e));
	}
	Log("msgctl1\n");
	ret = msgctl(msg2_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("msgctl2 failed: %s\n",strerror(e));
	}
	Log("msgctl2\n");
	return 0;
}

