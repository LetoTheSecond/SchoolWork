// Ahsan Khan
// April 08, 2017
// CS4760 Project 3
// C Headers
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

// System Headers
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <signal.h>

// Local Headers
#include "Logger.h"

// Global Variables
int shm_id  = -1,
	msg1_id = -1,
	msg2_id = -1;
unsigned long* _clock = NULL;

typedef struct MSG_t {
	long type;
	int procNum;
	unsigned long mm,nn;
	char data[256];
}MSG;

void KillHandel(int sig){
	Log("Kill signal receved, terminating...\n");
	Log("Cleaning up\n");
	int ret;
	ret = shmdt(_clock);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmdt failed: %s\n",strerror(e));
	}
	Log("shmdt\n");
	exit(0);
}

int main(int argc, char* argv[]){
	//==========================================================================
	signal(SIGTERM,KillHandel);
	signal(SIGINT,KillHandel);
	//==========================================================================
	// Variables
	int id=0;
	id = atoi(argv[0])+3;
	char buff[256];
	sprintf(buff,"proc_%03d_log.txt",id-3);
	InitLog(buff);
	//==========================================================================
	// Variables
	key_t 	shm_k,
			msg1_k,
			msg2_k;
	int 	msgSize = sizeof(MSG)-sizeof(long);
	//==========================================================================
	//Initialization
	shm_k = ftok("./oss",1);
	if ( shm_k < 0){
		int e = errno;
		ErrorLog("ftok for shm_k failed: %s\n",strerror(e));
		return -1;
	}
	msg1_k = ftok("./oss",2);
	if ( msg1_k < 0){
		int e = errno;
		ErrorLog("ftok for msg1_k failed: %s\n",strerror(e));
		return -1;
	}
	msg2_k = ftok("./oss",3);
	if ( msg2_k < 0){
		int e = errno;
		ErrorLog("ftok for msg2_k failed: %s\n",strerror(e));
		return -1;
	}
	shm_id = shmget(shm_k,2*sizeof(int), 0777 | IPC_CREAT);
	if (shm_id < 0){
		int e = errno;
		ErrorLog("shmget failed: %s\n",strerror(e));
		return -1;
	}
	_clock = (unsigned long*) shmat(shm_id,NULL,0);
	if (_clock == (unsigned long*)-1 ){
		int e = errno;
		ErrorLog("shmat filed: %s\n",strerror(e));
		int ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		return -1;
	}
	msg1_id = msgget(msg1_k, 0777 | IPC_CREAT);
	if (msg1_id < 0){
		int e = errno;
		ErrorLog("msgget1 failed: %s\n",strerror(e));
		int ret = shmdt(_clock);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		return -1;
	}
	msg2_id = msgget(msg2_k, 0777 | IPC_CREAT);
	if (msg2_id < 0){
		int e = errno;
		ErrorLog("msgget2 failed: %s\n",strerror(e));
		int ret = shmdt(_clock);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		ret = shmctl(shm_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmctl failed: %s\n",strerror(e));
		}
		ret = msgctl(msg1_id,IPC_RMID,NULL);
		if (ret < 0){
			int e = errno;
			ErrorLog("msgctl1 failed: %s\n",strerror(e));
		}
		return -1;
	}
	//==========================================================================
	// Main Loop
	unsigned long life = rand()%500000000;
	unsigned long startT = _clock[0]*1000000000 + _clock[1];
	unsigned long endSS,endNN;
	bool 	run 				= true,
			request 			= true,
			request_wait 		= false,
			critical_section 	= false,
			give_up				= false,
			done 				= false,
			die 				= false,
			die_wait 			= false;
	while (run){
		// Ask for access
		if (request){
			MSG req;
			req.type=1;
			req.procNum=id;
			sprintf(req.data,"Ping[%03d]\0",id-3);
			int ret = msgsnd(msg1_id,&req,msgSize,0);
			if (ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(request) Looks like master terminated: %s\n"
						,strerror(e));
					return -1;
				}
				ErrorLog("(request) msgsnd failed: %s\n",strerror(e));
				run=false;
			} else {
				Log("Message sent: %s\n",req.data);
				request=false;
				request_wait=true;
			}
		}
		// Wait for access
		if (request_wait){
			MSG wait;
			int ret = msgrcv(msg2_id,&wait,msgSize,id,IPC_NOWAIT);
			if ( errno != ENOMSG && ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(request_wait) Looks like master terminated: %s\n",
						strerror(e));
					return -1;
				}
				ErrorLog("(request) msgrcv failed: %s\n",strerror(e));
				run=false;
			} else if (ret > 0){
				Log("Message recived: %s\n",wait.data);
				// Have access to critical section
				critical_section=true;
				request_wait=false;
			}
		}
		// Do critical section
		if (critical_section){
			Log("In Critical Section\n");
			// 1s = 1000000000ns
			unsigned long t = _clock[0]*1000000000 + _clock[1];
			unsigned long diff = t - startT;
			if (diff >= life){
				done = true;
				endSS = _clock[0];
				endNN = _clock[1];
			}
			critical_section = false;
			give_up = true;
		}
		// Give up access to critical section
		if (give_up){
			MSG fin;
			fin.type=id;
			fin.procNum=id;
			sprintf(fin.data,"%03d access given up\0",id-3);
			int ret = msgsnd(msg1_id,&fin,msgSize,0);
			if (ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(give_up) Looks like master terminated: %s\n",
						strerror(e));
					return -1;
				}
				ErrorLog("msgsnd for fin failed: %s\n",strerror(e));
				run=false;
			} else {
				Log("Message sent: %s\n",fin.data);
				give_up=false;
				if (done){
					die = true;
				} else {
					request = true;
				}
			}
		}
		// Notify of impending death
		if (die){
			MSG req;
			req.type=2;
			req.procNum=id;
			req.mm=endSS;
			req.nn=endNN;
			sprintf(req.data,"Ready to die[%03d]\0",id-3);
			int ret = msgsnd(msg1_id,&req,msgSize,0);
			if (ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(die) Looks like master terminated: %s\n"
						,strerror(e));
					return -1;
				}
				ErrorLog("(die) msgsnd failed: %s\n",strerror(e));
			} else {
				Log("Message sent: %s\n",req.data);
				die=false;
				die_wait=true;
			}
		}
		// Wait for death acknowledgment
		if (die_wait){
			MSG wait;
			int ret = msgrcv(msg2_id,&wait,msgSize,id,0);
			if ( ret < 0){
				int e = errno;
				if (errno == EIDRM ){
					ErrorLog(
						"(die_wait) Looks like master terminated: %s\n",
						strerror(e));
					return -1;
				}
				ErrorLog("(die_wait) msgrcv failed: %s\n",strerror(e));
			} else {
				Log("Message recived: %s\n",wait.data);
				die_wait=false;
				run=false;
			}
		}
	}
	//==========================================================================
	// Cleanup
	Log("Cleaning up\n");
	int ret;
	ret = shmdt(_clock);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmdt failed: %s\n",strerror(e));
	}
	return 0;
}

