//=====================================
//		C Headers
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

//=====================================
//		System Headers
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>


//=====================================
//		Local Headers
#include "Logger.h"

//=====================================
//		Stuctures
typedef struct RESDESC_T {
	int In,
		Out;
} ResDesc;

typedef struct USRSTAT_T {
	bool Requesting;
	int Total,
		Held[20];
	//int SemID;
} UsrStat;

typedef struct SHM_T{
	unsigned int Time[2];
	UsrStat Users[18];
	ResDesc Resources[20];
	int SharedRes;
	//int SemID;
} Shm;

//=====================================
//		Global Variables
Shm* shm=NULL;
sem_t* TimeSem=NULL;
sem_t* UsrStatSem;
int id;

void KillHandel(int sig){
	Log("Kill signal receved, terminating...\n");
	Log("Cleaning up\n");
	sem_wait(UsrStatSem);
	shm->Users[id].Requesting=false;
	int limit = shm->SharedRes;
	for (int i=0;i<limit;++i){
		shm->Resources[i].In+=shm->Users[id].Held[i];
		shm->Resources[i].Out-=shm->Users[id].Held[i];
		shm->Users[id].Held[i]=0;
		shm->Users[id].Requesting=false;
	}
	shm->Users[id].Total=0;
	sem_post(UsrStatSem);
	int ret;
	if (shm){
		ret = shmdt(shm);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		} else {
			Log("shmdt\n");
		}
	}
	if (TimeSem>0){
		ret = sem_close(TimeSem);
		if (ret < 0){
			int e = errno;
			ErrorLog("sem_close(TimeSem) failed: %s\n",strerror(e));
		} else {
			Log("sem_close(TimeSem)\n");
		}
	}
	if (UsrStatSem>0){
		ret = sem_close(UsrStatSem);
		if (ret<0){
			int e = errno;
			ErrorLog("sem_close(UsrStatSem failed: %s\n",strerror(e));
		} else {
			Log("sem_close(UsrStatSem)\n");
		}
	}
	exit(0);
}


int main(int argc, char* argv[]){
	signal(SIGTERM,KillHandel);
	signal(SIGINT,KillHandel);
	
	id = atoi(argv[0]);
	char buff[256];
	sprintf(buff,"proc_%03d_log.txt",id);
	InitLog(buff);

	//=================================
	//				Variables & Initialization
	key_t shm_k = ftok("./oss",0);
	if ( shm_k < 0){
		int e = errno;
		ErrorLog("ftok for shm_k failed: %s\n",strerror(e));
		return -1;
	}
	int shm_id = shmget(shm_k,sizeof(Shm), 0777 | IPC_CREAT);
	if (shm_id < 0){
		int e = errno;
		ErrorLog("shmget failed: %s\n",strerror(e));
		return -1;
	}
	// Get Shared Mem
	shm = (Shm*) shmat(shm_id,NULL,0);
	if (shm == (Shm*)-1 ){
		int e = errno;
		ErrorLog("shmat filed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	sprintf(buff,"/%02d\0",id);
	UsrStatSem = sem_open(buff,O_CREAT,0777,1);
	if (UsrStatSem == SEM_FAILED){
		int e = errno;
		ErrorLog("sem_open(/%02d) failed: %s\n",id,strerror(e));
		raise(SIGTERM);
	}
	//shm->Users[i].SemID=-1;
	TimeSem = sem_open("/TimeSem",O_CREAT,0777,1);
	if (TimeSem == SEM_FAILED){
		int e = errno;
		ErrorLog("sem_open(/TimeSem) failed: %s\n",strerror(e));
		raise(SIGTERM);
	}

	//=================================
	//				Main Loop
	Log("Process %d\n",id);
	long long startTime = 0,
			  MS 		= 1000000,
			  last		= 0,
			  deathWait ;
	int bound = 250,
		limit = 0;
	bool req = false;
	sem_wait(TimeSem);
	Log("%d:%010d\n",shm->Time[0],shm->Time[1]);
	startTime = shm->Time[0]*100000000 + shm->Time[1];
	deathWait= startTime + (rand()%250 * MS);
	limit = shm->SharedRes;
	sem_post(TimeSem);
	sem_wait(UsrStatSem);
	shm->Users[id].Requesting=true;
	sem_post(UsrStatSem);

	while (true){

		sem_wait(TimeSem);
		long long time = shm->Time[0]*100000000 + shm->Time[1];
		sem_post(TimeSem);

		// Check for suicide
		if ( (time - startTime) >= 100000000 && time >= deathWait){
			if (rand()&1){
				Log("Waiting for death\n");
				// 1
				deathWait = time + (rand()&250 * MS);
			} else {
				Log("Kiling Self\n");
				// 0
				raise(SIGTERM);
			}
		}

		sem_wait(UsrStatSem);
		if (!shm->Users[id].Requesting){
			if (rand()%1000 <= bound){
				Log("Triggered\n");
				if (rand()%1){
					Log("Releasing a resource\n");
					// 1 Relese resource
					for (int i=0;i<limit;i++){
						if (shm->Users[id].Held[i]>0){
							++shm->Resources[i].In;
							--shm->Resources[i].Out;
							--shm->Users[id].Held[i];
							break;
						}
					}
				} else {
					Log("Requesting a resource\n");
					// 0 Request resource
					shm->Users[id].Requesting=true;	
				}
			}
		}
		sem_post(UsrStatSem);
	}


	//=================================
	//				Cleanup
	raise(SIGTERM);
}

