//=====================================
//		C Headers
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

//=====================================
//		System Headers
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>


//=====================================
//		Local Headers
#include "Logger.h"

//=====================================
//		Stuctures
typedef struct RESDESC_T {
	int In,
		Out;
} ResDesc;

typedef struct USRSTAT_T {
	bool Requesting;
	int Total,
		Held[20];
	//int SemID;
} UsrStat;

typedef struct SHM_T{
	unsigned int Time[2];
	UsrStat Users[18];
	ResDesc Resources[20];
	int SharedRes;
	//int SemID;
} Shm;

//=====================================
//		Global Variables
int shm_id;
pid_t pids[18];
Shm* shm=NULL;
sem_t* TimeSem=NULL;
sem_t* UsrStatSem[18];

void KillHandel(int sig){
	Log("Kill signal receved, terminating...\n");
	Log("Cleaning up\n");
	int ret;
	for (int i=0;i<18;++i){
		if (pids[i] > 0){
			ret = kill(pids[i],SIGTERM);
			if (ret < 0 ){
				ErrorLog("Killing %d failed\n",pids[i]);
			}
		}
	}
	if (shm){
		ret = shmdt(shm);
		if (ret < 0){
			int e = errno;
			ErrorLog("shmdt failed: %s\n",strerror(e));
		}
		Log("shmdt\n");
	}
	ret = shmctl(shm_id,IPC_RMID,NULL);
	if (ret < 0){
		int e = errno;
		ErrorLog("shmctl failed: %s\n",strerror(e));
	}
	Log("shmctl\n");
	if (TimeSem > 0){
		ret = sem_unlink("/TimeSem");
		if (ret < 0){
			int e = errno;
			ErrorLog("sem_close(TimeSem) failed: %s\n",strerror(e));
		}
		Log("sem_close(TimeSem)\n");
	}
	for (int i=0;i<18;++i){
		if (UsrStatSem[i]>0){
			char buff[4];
			sprintf(buff,"/%02d\0",i);
			ret = sem_unlink(buff);
			if (ret<0){
				int e = errno;
				ErrorLog("sem_close(UsrStatSem[%d] failed: %s\n",i,strerror(e));
			} else {
				Log("sem_close(UsrStatSem[%d])\n",i);
			}
		}
	}
	exit(0);
}


int main(int argc, char* argv[]){
	signal(SIGTERM,KillHandel);
	signal(SIGINT,KillHandel);

	//=================================
	//				Arguments
	char* filename="log.txt";
	int o;
	while ((o = getopt(argc,argv,"l:")) != -1){
		switch (o){
			case 'l':
				filename=optarg;
				break;
			default:
				break;
		}
	}
	InitLog(filename);

	//=================================
	//				Variables & Initialization
	key_t shm_k = ftok("./oss",0);
	if ( shm_k < 0){
		int e = errno;
		ErrorLog("ftok for shm_k failed: %s\n",strerror(e));
		return -1;
	}
	shm_id = shmget(shm_k,sizeof(Shm), 0777 | IPC_CREAT);
	if (shm_id < 0){
		int e = errno;
		ErrorLog("shmget failed: %s\n",strerror(e));
		return -1;
	}
	// Get Shared Mem
	shm = (Shm*) shmat(shm_id,NULL,0);
	if (shm == (Shm*)-1 ){
		int e = errno;
		ErrorLog("shmat filed: %s\n",strerror(e));
		raise(SIGTERM);
	}
	shm->Time[0]=0;
	shm->Time[1]=0;
	for (int i=0;i<18;++i){
		char buff[4];
		sprintf(buff,"/%02d\0",i);
		UsrStatSem[i] = sem_open(buff,O_CREAT,0777,1);
		if (UsrStatSem[i] == SEM_FAILED){
			int e = errno;
			ErrorLog("sem_open(/%02d) failed: %s\n",i,strerror(e));
			raise(SIGTERM);
		}
		shm->Users[i].Requesting=false;
		shm->Users[i].Total=0;
		for (int j=0;j<20;++j) shm->Users[i].Held[j]=0;
		//shm->Users[i].SemID=-1;
	}
	shm->SharedRes = 5+(rand()%3); // [5-8] Shared Resources out of 20
	for (int i=0;i<shm->SharedRes;++i){
		shm->Resources[i].In=1+(rand()%10); // [1-10] Resources per descriptor
		shm->Resources[i].Out=0;
	}
	//shm->SemID=-1;
	for (int i=0;i<18;++i){
		pids[i]=0;
	}
	TimeSem = sem_open("/TimeSem",O_CREAT,0777,1);
	if (TimeSem == SEM_FAILED){
		int e = errno;
		ErrorLog("sem_open(/TimeSem) failed: %s\n",strerror(e));
		raise(SIGTERM);
	}

	//=================================
	//				Main Loop
	long long SEC 			= 1000000000,
		 	  MS 			= 1000000,
			  spawnCheck 	= 0;
	bool deadlock 		= false;
	int increment = 1,
		limit = shm->SharedRes,
		id = 0 ;
	while (true){
		
		sem_wait(TimeSem);
		// Increment Time
		if (shm->Time[1]>=SEC){
			shm->Time[1]-=SEC;
			++shm->Time[0];
			deadlock=true;
		} else {
			shm->Time[1]+=increment;
		}
		//Log("%d:%010d\n",shm->Time[0],shm->Time[1]);
		long long totalTime = shm->Time[0]*SEC+shm->Time[1];
		sem_post(TimeSem);
		
		// Handel request
		sem_wait(UsrStatSem[id]);
		if (shm->Users[id].Requesting){
			for (int i=0;i<limit;++i){
				if (shm->Resources[i].In){
					Log("Giving User[%d] one Resource [%d]\n",id,i);
					--shm->Resources[i].In;
					++shm->Resources[i].Out;
					++shm->Users[id].Total;
					++shm->Users[id].Held[i];
					++id;
					id%=18;
					break;
				}
			}
		}
		sem_post(UsrStatSem[id]);

		// Spawn User Process
		if ( (totalTime - spawnCheck) >= 1+rand()%(500*MS)){ // [1-500]ms
			for (int i=0;i<18;++i){
				if (pids[i]<=0){
					pid_t pid=fork();
					if (pid == 0){
						char buff[256];
						sprintf(buff,"%d\0",i);
						int ret = execl("./user",buff,(char*)0);
						if (ret <-1){
							int e = errno;
							ErrorLog("execl for %d failed: %s\n",i,strerror(e));
						}
					} else if (pid < 0){
						int e = errno;
						ErrorLog("fork for %d failed: %s\n",i,strerror(e));
						pids[i]=-1;
					} else {
						pids[i]=pid;
						Log("Process %d spawned\n",i);
					}
					break;
				}
			}
			spawnCheck = totalTime;
		}

		if (deadlock){
			int max = 0;
			int j =0;
			for (int i=0;i<18;++i){
				sem_wait(UsrStatSem[i]);
				if (!shm->Users[i].Requesting){
					deadlock=false;
				} else {
					if(shm->Users[i].Total > max){
						max=shm->Users[i].Total;
						j=i;
					}
				}
				if(!kill(pids[j],SIGTERM)){
					Log("Killing %d for deadlock\n",j);
					pids[j]=0;
				}
				sem_post(UsrStatSem[i]);
			}

			deadlock=false;
		}
	}

	//=================================
	//				Cleanup
	raise(SIGTERM);
}


