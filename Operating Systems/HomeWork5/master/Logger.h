// Ahsan Khan
// April 08, 2017
// CS4760 Project 3
//#pragma once
#ifndef LOGGER_H
#define LOGGER_H
#include <stdbool.h>
//	Function forwards
bool InitLog(const char*);
bool RestartLog();
bool Log( const char* , ... );
bool ErrorLog( const char* , ... );
#endif
