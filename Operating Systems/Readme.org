* Homework 1
+ Unix System Calls and Library Functions
+ Implementation of a loggin utility from a template provided in Exercise 2.13(Page 56) in the textbook.
** Template
*** log.h
#+BEGIN_SRC c++
    #include <time.h>
    typedef struct data_struct {
        time_t time;
        char* string;
    } data_t;
    int addmsg(data_t data);
    void clearlog(void);
    char* getlog(void);
    int savelog(char* filename);
#+END_SRC
*** loglib.c
#+BEGIN_SRC c++
    #include <stdlib.h>
    #include <string.h>
    #include "log.h"
    typedef struct list_struct {
        data_t item;
        struct list_struct* next;
    } log_t;
    static log_t* headptr = NULL;
    static log_t tailptr = NULL;
    int addmsg(data_t data){
        return 0;
    }
    void clearlog(void){
    }
    char* getlog(void){
        return NULL;
    }
    int savelog(char* filename){
        return 0;
    }
#+END_SRC
** Spec
+ Format for log: ~<argv[0]>: Time:"" Error:"" nValue="" - <Detailed error message>~
+ Command line arguments, implemented using getopt:
	- ~-h~: Display help menu
	- ~-n x~: Sets a variable in the program to x, the value of which in included in every log entry. The default value is: ~37.
	- ~-l filename~: Sets the log filename. Default is: ~logfile.txt~
* Homework 2
+ Concurrent UNIX Processes and Shared Memory
** Notes 
*** Master Process

# A program that uses multiple processes to increment a number in shared memory and write to a file. They each compete to gain access to the file and memory.
* Homework 3
A master process allocates shared memory with a timer only it writes to. It then forks a number of child processes and waits for messages from them. The child process reads this simulated system clock and generates a random duration representing its life time. Inside a critical section the child checks the parents time agains its own life span and when its time is up it send a message to the parent and dies.
* Homework 4
The master process allocates shared memory for system data structures, which include a process control block for each slave process. The master creates new slaves at random intervals while managing a clock stored in shared memory.
* Homework 5

* Homework 6

