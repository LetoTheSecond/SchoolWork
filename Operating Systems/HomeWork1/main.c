#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include "main.h"

typedef struct List_Struct {
	Data_T item;
	struct List_Struct *next;
} Log_T;

static Log_T *headptr = NULL;
static Log_T *tailptr = NULL;
static char* ProgName = NULL;

int AddMsg(Data_T data){
	Log_T* newnode;
	int nodesize;
	nodesize = sizeof(Log_T)+strlen(data.string)+1;
	if ((newnode = (Log_T *)(malloc(nodesize))) == NULL){
		perror("Malloc failed for AddMsg()\n");
		return -1;
	}
	newnode->item.time=data.time;
	newnode->item.string = malloc(strlen(data.string)+1);
	newnode->item.error = data.error;
	strcpy(newnode->item.string,data.string);
	free(data.string);
	newnode->next = NULL;
	if (headptr == NULL){
		headptr = newnode;
	} else {
		tailptr->next = newnode;
	}
	tailptr = newnode;
	return 0;
}

void ClearLog(void){
	Log_T* curr = NULL;
	Log_T* next = NULL;
	curr = headptr;
	headptr = NULL;
	do {
		next = curr->next;
		free(curr->item.string);
		free (curr);
		curr = next;
	} while (curr);
}

char* GetLog(void){
	char* ret = NULL;
	int len = 1;
	for(Log_T* i =headptr;i;i=i->next){ len+=strlen(i->item.string); }
	ret = malloc(len);
	if (!ret){
		perror("Malloc failed for GetLog()\n");
		return NULL;
	}
	ret[0]='\0';
	for(Log_T* i =headptr;i;i=i->next){ strcat(ret,i->item.string); }
	return ret;
}

int SaveLog(const char* filename){
	FILE* file = NULL;
	file = fopen(filename,"w");
	if (!file){
		perror("Could not open logfile to SaveLog()\n");
		return -1;
	}
	char* log = NULL;
	log = GetLog();
	if (!log){
		perror("In SaveLog GetLog returned NULL\n");
		return -1;
	}
	fprintf(file, "%s",log);
	if (ferror(file)){
		perror("In SaveLog ferror return an error\n");
		fclose(file);
		return -1;
	}
	fclose(file);
	free(log);
	return 0;
}

int Log(int eV, const char* msg){
	Data_T data;
	data.time = time(NULL);
	data.error = eV;
	data.string = NULL;

	// char Tbuff[20] = {' '};
	// strftime(Tbuff,sizeof Tbuff,"%Y-%m-%d|%H:%M:%S",gmtime(&data.time));
	static const char* format = "%s [%d]: Error [%d]: %s\n";
	// int len = snprintf(NULL, 0,format, ProgName, Tbuff,eV, msg);
	int len = snprintf(NULL, 0,format, ProgName, data.time, eV, msg);
	data.string = malloc(len+1);
	if (data.string == NULL){
		perror("Malloc failed for Log()\n");
		return -1;
	}
	// sprintf(data.string,format,ProgName,Tbuff,eV,msg);
	sprintf(data.string,format,ProgName,data.time,eV,msg);

	return AddMsg(data);
}

int main(int argc, char ** argv){
	int x = 37;
	char* filename = "logfile.txt\0";
	
	ProgName = malloc(strlen(argv[0])+1);
	strcpy(ProgName,argv[0]);
	
	int o;
	while ((o = getopt(argc,argv,"hn:l:")) != -1){
		switch (o){
			case 'h':
				printf("Usage:\n"
					"\t-h : Shows this message\n"
					"\t-n x : Sets the logging constant to x\n"
					"\t-l filename : Sets the name for log saved to disk\n");
				free(ProgName);
				return 0;
			break;
			case 'n':
				x = atoi(optarg);
			break;
			case 'l':
				filename = optarg;
			break;
			default:
			break;
		}
	}

	Log(x,"Detailed Msg 1");
	Log(x,"Detailed Msg 2");

	char* log = NULL;
	log = GetLog();
	printf("%s\n",log);

	SaveLog(filename);

	free(log);
	ClearLog();
	free(ProgName);
	return 0;
}
