:: @echo off
:: if /i [%1] == [clean] (
:: 	del /Q  *.pdb *.ilk *.idb *.obj *.exe
:: ) else (
:: 	cl /Zi /Gm /nologo /Debug /EHsc /I include /D CURL_STATICLIB *.cpp /Fe:main.exe lib/libcurl_a.lib
:: )

@echo off

if /i [%1]==[clean]		(goto :clean)
if /i [%1]==[run]		(goto :run)
if /i [%1]==[brun]		(goto :brun)
						(goto :default)

:clean
::rmdir /s /Q
del main.exe
goto :eof

:run
main.exe
goto :eof

:brun
mingw32-gcc -g -std=c99 main.c -o main.exe
goto :run

:default
mingw32-gcc -g -std=c99 main.c -o main.exe
goto :eof

:eof
::