#ifndef LOG_H
#define LOG_H
#include <time.h>

typedef struct Data_Struct {
	time_t time;
	int error;
	char* string;
} Data_T;

int AddMsg(Data_T data);
void ClearLog(void);
char* GetLog(void);
int SaveLog(const char* filename);

#endif
