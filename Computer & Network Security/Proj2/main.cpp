﻿#include <iostream>
#include <iomanip>
#include <array>
#include <vector>
#include <string>
#include <algorithm>
#include <bitset>
#include <cstring>
#include <cstdlib>

std::string Example[]={
	"74617245","74756120","68206d65","206f6d6f",
	"70207865","69726168","69656173","4e202c73",
	"646f6369","646f6369","6d6f6e20","2c656e69",
	"69727020","800a636e","000001b8","00000000"
};

//std::string input = "Erat autem homo ex pharisaeis, Nicodemus nomine, princ";
// std::string input = "Erat autem viro Elimelech consanguineus, home potens, et "
	// "magnarum opum, nomine Booz. Dixitque Ruth Moabitis ad socrum suam: "
	// "Si iubes vadam in agrum";

// std::string in = "Erat autem homo ex pharisaeis, Nicodemus nomine, princ";
std::string in = "Rosetta Code";
int main(){
	// std::string input;
	// std::getline(std::cin,input);
	// std::cout << "Input: " << input << '\n';

	/*
	 * Block 			= 64 bytes : 512 bits
	 * Word 			=  4 bytes :  32 bits
	 * Hash 			= 16 bytes : 128 bits
	 * Char 			=  1 byte
	 * Int 				=  4 bytes
	 * Long Long 		=  8 bytes
	 * String.size() 	= # of chars (excludes \0)
	*/
	// std::cout << input << '\n';
	// std::cout << "31d6cfe0 d16ae931 b73c59d7 e0c089c0\n";
	char* input = nullptr;
	input = (char*)malloc((in.size()+2)*sizeof(char));
	std::strcpy(input,in.c_str());
	std::cout << std::bitset<8>(input[in.size()-2]) << '\n' << std::bitset<8>(input[in.size()-1])  << '\n' << std::bitset<8>(input[in.size()]) << '\n' << std::bitset<8>(input[in.size()+1]) << "\n\n";
	int bitidx = in.size();
	input[in.size()]=0x80;
	// input[in.size()+1]='\0';
	std::cout << std::bitset<8>(input[in.size()-2]) << '\n' << std::bitset<8>(input[in.size()-1])  << '\n' << std::bitset<8>(input[in.size()]) << '\n' << std::bitset<8>(input[in.size()+1]) << "\n\n";
	unsigned int* msg=nullptr;
	int inputlen=in.size();
	// unsigned char bit=0x80;
	// input += (unsigned char)0x80;
	int msglen=(56-std::strlen(input))%64;
	if (msglen<0) msglen+=64;
	msglen=(std::strlen(input)+msglen)/4+2;
	msg=(unsigned int*)malloc(sizeof(unsigned int)*msglen);
	std::memset(msg,0x00,sizeof(unsigned int)*msglen);
	for (int i=0;i<=inputlen;i+=4){
		unsigned int temp=0;
		// if (i >= inputlen-4){
			std::cout << i   << ": " << std::bitset<8>(input[i]) << ' '
					  << i+1 << ": " << std::bitset<8>(input[i+1]) << ' '
					  << i+2 << ": " << std::bitset<8>(input[i+2]) << ' '
					  << i+3 << ": " << std::bitset<8>(input[i+3]) << "\n\n";
			std::cout << i+3 << ": " << std::bitset<8>(input[i+3]) << ' '
					  << i+2 << ": " << std::bitset<8>(input[i+2]) << ' '
					  << i+1 << ": " << std::bitset<8>(input[i+1]) << ' '
					  << i   << ": " << std::bitset<8>(input[i])   << "\n\n";
			std::cout << std::bitset<32>(temp) << '\n';
			      temp|=(unsigned char)input[i+3];
			std::cout << std::bitset<32>(temp) << '\n';
			(temp<<=8)|=(unsigned char)input[i+2];
			std::cout << std::bitset<32>(temp) << '\n';
			(temp<<=8)|=(unsigned char)input[i+1];
			std::cout << std::bitset<32>(temp) << '\n';
			(temp<<=8)|=(unsigned char)input[i  ];
			std::cout << std::bitset<32>(temp) << "\n\n";
			// if ( in.size()!=0 && bitidx%4 == 0){
			//  	temp^=0xff000000;
			// 	std::cout << std::bitset<32>(temp) << "\n\n";
			// }
		// } else {
		// 	      temp|=input[i+3];
		// 	(temp<<=8)|=input[i+2];
		// 	(temp<<=8)|=input[i+1];
		// 	(temp<<=8)|=input[i  ];
		// }
		//for (int j=3;j>=0;--j){
		//	temp|=(input[i+j])<<(8*(j));
		//}
		// std::cout<<i/4<<" : "<< input[i]<< input[i+1]<< input[i+2]<< input[i+3] << "=>"
		// 		 << input[i+3]<< input[i+2]<< input[i+1]<< input[i]<<'\n';
		// std::cout<<std::hex<<temp<<" : "<<Example[i/4]<<'\n';
		msg[i/4]=temp;
	}
	std::cout << std::bitset<32>(msg[msglen-4]) << '\n' << std::bitset<32>(msg[msglen-3]) << '\n' << std::bitset<32>(msg[msglen-2]) << '\n' << std::bitset<32>(msg[msglen-1]) << "\n\n";
	msg[msglen-2]=(inputlen*8);
	std::cout << std::bitset<32>(msg[msglen-4]) << '\n' << std::bitset<32>(msg[msglen-3]) << '\n' << std::bitset<32>(msg[msglen-2]) << '\n' << std::bitset<32>(msg[msglen-1]) << "\n\n";
	for (int i=0;i<msglen;++i){
		std::cout << std::hex << std::setw(8) << std::setfill('0') << msg[i] << '\n';
		// std::cout <<std::setw(8)<<std::setfill('0')<< std::hex << msg[i] << '\t' << Example[i] << '\n';
		// // std::cout<<std::bitset<32>(msg[i])<<'\t'<<std::bitset<32>(std::strtoul(Example[i].c_str(),NULL,16))<<"\n\n";
		// //std::printf("%08x \t %08x\n",msg[i],std::strtoul(Example[i].c_str(),NULL,16));
		// if ( std::bitset<32>(msg[i]) != std::bitset<32>(std::strtoul(Example[i].c_str(),NULL,16))){
		// 	// std::cout<<i<<' '<<std::setfill('0')<<std::hex<<msg[i]<<'\t'<<Example[i]<<'\n';
		// 	std::cout<</*std::dec<<i<<' '<<*/std::bitset<32>(msg[i])<<'\t'<<std::bitset<32>(std::strtoul(Example[i].c_str(),NULL,16))<<"\n";
		// }
	}
	unsigned int A=0x67452301,AA,
				 B=0xefcdab89,BB,
				 C=0x98badcfe,CC,
				 D=0x10325476,DD;
	auto F=[](unsigned int X,unsigned int Y,unsigned int Z){
		return ((X&Y)|((~X)&Z));
	};
	auto G=[](unsigned int X,unsigned int Y,unsigned int Z){
		return ((X&Y)|(X&Z)|(Y&Z));
	};
	auto H=[](unsigned int X,unsigned int Y,unsigned int Z){
		return (X^Y^Z);
	};
	auto R1=[&](unsigned int& a,unsigned int& b,unsigned int& c,unsigned int& d,unsigned int& k,int s){
		a=(a+F(b,c,d)+k);
		a=(a<<s)|(a>>(32-(s)));
	};
	auto R2=[&](unsigned int& a,unsigned int& b,unsigned int& c,unsigned int& d,unsigned int& k,int s){
		a=(a+G(b,c,d)+k+0x5A827999);
		a=(a<<s)|(a>>(32-(s)));
	};
	auto R3=[&](unsigned int& a,unsigned int& b,unsigned int& c,unsigned int& d,unsigned int& k,int s){
		a=(a+H(b,c,d)+k+0x6ED9EBA1);
		a=(a<<s)|(a>>(32-(s)));
	};
	unsigned int X[16];
	for (int i=0;i<(msglen/16);i++){
		for (int j=0;j<16;j++){
			X[j]=msg[i*16+j];
		}
		AA=A;BB=B;CC=C;DD=D;
		
		R1(A,B,C,D,X[ 0],3);
		// std::printf("FF%2d %08x %08x %08x %08x\n",1,A,B,C,D);
		R1(D,A,B,C,X[ 1],7);
		// std::printf("FF%2d %08x %08x %08x %08x\n",2,A,B,C,D);
		R1(C,D,A,B,X[ 2],11);
		// std::printf("FF%2d %08x %08x %08x %08x\n",3,A,B,C,D);
		R1(B,C,D,A,X[ 3],19);
		// std::printf("FF%2d %08x %08x %08x %08x\n",4,A,B,C,D);
		R1(A,B,C,D,X[ 4],3);
		// std::printf("FF%2d %08x %08x %08x %08x\n",5,A,B,C,D);
		R1(D,A,B,C,X[ 5],7);
		// std::printf("FF%2d %08x %08x %08x %08x\n",6,A,B,C,D);
		R1(C,D,A,B,X[ 6],11);
		// std::printf("FF%2d %08x %08x %08x %08x\n",7,A,B,C,D);
		R1(B,C,D,A,X[ 7],19);
		// std::printf("FF%2d %08x %08x %08x %08x\n",8,A,B,C,D);
		R1(A,B,C,D,X[ 8],3);
		// std::printf("FF%2d %08x %08x %08x %08x\n",9,A,B,C,D);
		R1(D,A,B,C,X[ 9],7);
		// std::printf("FF%2d %08x %08x %08x %08x\n",10,A,B,C,D);
		R1(C,D,A,B,X[10],11);
		// std::printf("FF%2d %08x %08x %08x %08x\n",11,A,B,C,D);
		R1(B,C,D,A,X[11],19);
		// std::printf("FF%2d %08x %08x %08x %08x\n",12,A,B,C,D);
		R1(A,B,C,D,X[12],3);
		// std::printf("FF%2d %08x %08x %08x %08x\n",13,A,B,C,D);
		R1(D,A,B,C,X[13],7);
		// std::printf("FF%2d %08x %08x %08x %08x\n",14,A,B,C,D);
		R1(C,D,A,B,X[14],11);
		// std::printf("FF%2d %08x %08x %08x %08x\n",15,A,B,C,D);
		R1(B,C,D,A,X[15],19);
		// std::printf("FF%2d %08x %08x %08x %08x\n",16,A,B,C,D);


		R2(A,B,C,D,X[ 0],3);R2(D,A,B,C,X[ 4],5);R2(C,D,A,B,X[ 8], 9);R2(B,C,D,A,X[12],13);
		R2(A,B,C,D,X[ 1],3);R2(D,A,B,C,X[ 5],5);R2(C,D,A,B,X[ 9], 9);R2(B,C,D,A,X[13],13);
		R2(A,B,C,D,X[ 2],3);R2(D,A,B,C,X[ 6],5);R2(C,D,A,B,X[10], 9);R2(B,C,D,A,X[14],13);
		R2(A,B,C,D,X[ 3],3);R2(D,A,B,C,X[ 7],5);R2(C,D,A,B,X[11], 9);R2(B,C,D,A,X[15],13);
		
		R3(A,B,C,D,X[ 0],3);R3(D,A,B,C,X[ 8],9);R3(C,D,A,B,X[ 4],11);R3(B,C,D,A,X[12],15);
		R3(A,B,C,D,X[ 2],3);R3(D,A,B,C,X[10],9);R3(C,D,A,B,X[ 6],11);R3(B,C,D,A,X[14],15);
		R3(A,B,C,D,X[ 1],3);R3(D,A,B,C,X[ 9],9);R3(C,D,A,B,X[ 5],11);R3(B,C,D,A,X[13],15);
		R3(A,B,C,D,X[ 3],3);R3(D,A,B,C,X[11],9);R3(C,D,A,B,X[ 7],11);R3(B,C,D,A,X[15],15);

		A+=AA;
		B+=BB;
		C+=CC;
		D+=DD;
	}
	A = ((A & 0xFF) << 24) | ((A & 0xFF00) << 8) | ((A & 0xFF0000) >> 8) | ((A & 0xFF000000) >> 24);
	B = ((B & 0xFF) << 24) | ((B & 0xFF00) << 8) | ((B & 0xFF0000) >> 8) | ((B & 0xFF000000) >> 24);
	C = ((C & 0xFF) << 24) | ((C & 0xFF00) << 8) | ((C & 0xFF0000) >> 8) | ((C & 0xFF000000) >> 24);
	D = ((D & 0xFF) << 24) | ((D & 0xFF00) << 8) | ((D & 0xFF0000) >> 8) | ((D & 0xFF000000) >> 24);
	std::printf("%08x %08x %08x %08x\n",A,B,C,D);
	return 0;
}

// : vim: set nowrap : 

