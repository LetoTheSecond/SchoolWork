#include <iostream>
#include <iomanip>
#include <array>
#include <vector>
#include <bitset>
#include <string>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <cstdlib>
// LITTLE ENDIAN
// Ran Like: mmb `cat Key` < Msg > Crypt
// Turning dir: /accounts/classes/schultemw/cs4780/<student #>

const unsigned int 	C   = 0x2AAAAAAA, 
					c[] = { 0x025F1CDB,0x04BE39B6,0x12F8E6D8,0x2F8E6D81 },
					mod = pow(2,32)-1;

std::array<unsigned int,4> keyblock {0};

std::array<unsigned int,4> f(std::array<unsigned int,4> x){
	std::array<unsigned int,4> ret{0};
	for ( int i=0;i<4;++i ){
		ret[i] = (c[i]*x[i])%mod;
	}
	if (ret[0]%2!=0){
		ret[0] = ret[0]^C;
	}
	if (ret[3]%2!=0){
		ret[3] = ret[3]^C;
	}
	for (int i=0;i<4;++i){
		ret[i] = ret[(4+(i-1))%4]^ret[i]^ret[(i+1)%4];
	}
	return ret;
}
std::array<unsigned int,4> Encrypt(std::array<unsigned int,4> P){
	std::array<unsigned int,4> ret = P;
	for (int l=0;l<2;++l){
		for (int i=0;i<4;++i){
			ret[i] = ret[i] ^ keyblock[(i+0)%4];
		}
		//std::printf("XOR: %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		ret = f(ret);
		//std::printf("F:   %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		for (int i=0;i<4;++i){
			ret[i] = ret[i]^keyblock[(i+1)%4];
		}
		//std::printf("XOR: %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		ret = f(ret);
		//std::printf("F:   %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		for (int i=0;i<4;++i){
			ret[i] =ret[i]^keyblock[(i+2)%4];
		}
		//std::printf("XOR: %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		ret = f(ret);
		//std::printf("F:   %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		//for (int i=0;i<4;++i){
		//	ret[i] = ret[i]^keyblock[(i+3)%4];
		//}
		//std::printf("XOR: %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
		//ret = f(ret);
		//std::printf("F:   %08x %08x %08x %08x\n",ret[0],ret[1],ret[2],ret[3]);
	}
	return ret;
}

int main(int argc, char* argv[]){
	std::string key = "13579BDF02468ACE124578ABDEFECB98";
	std::array<std::string,4> temp;
	temp[0]=key.substr( 0,8);
	temp[1]=key.substr( 8,8);
	temp[2]=key.substr(16,8);
	temp[3]=key.substr(24,8);
	std::memcpy(&keyblock[0], temp[0].data(), 7 );
	std::memcpy(&keyblock[1], temp[1].data(), 7 );
	std::memcpy(&keyblock[2], temp[2].data(), 7 );
	std::memcpy(&keyblock[3], temp[3].data(), 7 );
	//for (int i=0;i<4;++i){
	//	keyblock[0]=std::strtoul(temp[i].c_str(),NULL,16);
	//}
	std::memcpy(keyblock.data(),key.c_str(),16*4);
	//std::printf("Key: %08x %08x %08x %08x\n",keyblock[0],keyblock[1],keyblock[2],keyblock[3]);
	for (int i=0;i<4;++i){
		std::printf("temp[%d] %s key[%d] %8x\n",
				i,temp[i].c_str(),i,keyblock[i]);
		for (auto j:temp[i]){
			std::cout << std::bitset<CHAR_BIT>(j) << " ";
		}
		std::cout << "  :  " << std::bitset<16>(keyblock[i])<<"\n";
	}
	std::printf("\n");
	char inputblock[17];
	std::vector<std::string> input;
	std::vector<std::array<unsigned int,4>> blocks;
	std::vector<std::array<unsigned int,4>> crypt;
	while (std::fgets(inputblock,sizeof inputblock,stdin)!=NULL){
		int l = strlen(inputblock);
		if (l < 16){
			for (int i=l;i<16;++i){
				inputblock[i] = 0;
			}
		}
		input.push_back(inputblock);
		std::array<unsigned int,4> buff;
		std::memset(buff.data(),0,16);
		std::memcpy(buff.data(),inputblock,strlen(inputblock));
		blocks.push_back(buff);
		std::memset(inputblock,'\0',17);
	}
	for (int i=0;i<blocks.size();++i){
		// printf("Block %d M: %s\n",i,input[i].c_str());
		crypt.push_back(Encrypt(blocks[i]));
		std::printf("Block %d P: %08x %08x %08x %08x\n",i,blocks[i][0],blocks[i][1],blocks[i][2],blocks[i][3]);
		std::printf("Block %d C: %08x %08x %08x %08x\n\n",i,crypt[i][0],crypt[i][1],crypt[i][2],crypt[i][3]);
	}
	return 0;
}

/*: vim: set nowrap  :*/

